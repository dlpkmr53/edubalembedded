%% 3-Axis accelerometer calibration using least squares method.

nmeas = 1000; % Number of measurements for each axis configuration
pause_time = 15;
W1=zeros(nmeas,4); % Initialize measurement vector 1
GX1 = 0; GY1 = 0; GZ1 = 0; % Initialize gyro acculmulator 1
W2=zeros(nmeas,4); % Initialize measurement vector 2
GX2 = 0; GY2 = 0; GZ2 = 0; % Initialize gyro acculmulator 2
W3=zeros(nmeas,4); % Initialize measurement vector 3
GX3 = 0; GY3 = 0; GZ3 = 0; % Initialize gyro acculmulator 3
W4=zeros(nmeas,4); % Initialize measurement vector 4
GX4 = 0; GY4 = 0; GZ4 = 0; % Initialize gyro acculmulator 4
W5=zeros(nmeas,4); % Initialize measurement vector 5
GX5 = 0; GY5 = 0; GZ5 = 0; % Initialize gyro acculmulator 5
W6=zeros(nmeas,4); % Initialize measurement vector 6
GX6 = 0; GY6 = 0; GZ6 = 0; % Initialize gyro acculmulator 6
count = 0; % initialize counter for timeout error 
s = serial(com_port_string); % Configure serial port
s.BaudRate = 115200; % Set baudrate
s.Terminator = ''; % Define terminator character

disp('Starting IMU calibration...')
calibMSID = input('ID of MiniSegway to calibrate: ');

%% Fill vectors for the given orientations:
disp('Z-Axis down');
pause(pause_time);
disp('Starting measurement...')
Y1 = repmat([0 0 -1],nmeas,1); % Output vector for Zdown
fopen(s); % open serial port
for k = 1:nmeas
    [ax, ay, az, gx, gy, gz] = readIMU(s); %Read serial port
    if rem(k,100)==0
        disp(['ax: ' num2str(ax) ' ay: ' num2str(ay) ' az: ' num2str(az) ' gx: ' num2str(gx) ' gy: ' num2str(gy) ' gz: ' num2str(gz)]);
    end
    W1(k,1:4) = [ax ay az 1]; % Fill measurement matrix
    GX1 = GX1 + gx;
    GY1 = GY1 + gy;
    GZ1 = GZ1 + gz;
end
fclose(s);
GX1 = GX1/nmeas; % Take the average
GY1 = GY1/nmeas;
GZ1 = GZ1/nmeas;

disp('Z-Axis up')
pause(pause_time);
disp('Starting measurement...')
Y2 = repmat([0 0 1],nmeas,1); % Output vector for Zup
fopen(s); % open serial port
for k = 1:nmeas
    [ax, ay, az, gx, gy, gz] = readIMU(s); %Read serial port
    if rem(k,100)==0
        disp(['ax: ' num2str(ax) ' ay: ' num2str(ay) ' az: ' num2str(az) ' gx: ' num2str(gx) ' gy: ' num2str(gy) ' gz: ' num2str(gz)]);
    end
    W2(k,1:4) = [ax ay az 1]; % Fill measurement matrix
    GX2 = GX2 + gx;
    GY2 = GY2 + gy;
    GZ2 = GZ2 + gz;
end
fclose(s);
GX2 = GX2/nmeas; % Take the average
GY2 = GY2/nmeas;
GZ2 = GZ2/nmeas;

disp('Y-Axis down');
pause(pause_time);
disp('Starting measurement...')
Y3 = repmat([0 -1 0],nmeas,1); % Output vector for Ydown
fopen(s); % open serial port
for k = 1:nmeas
    [ax, ay, az, gx, gy, gz] = readIMU(s); %Read serial port
    if rem(k,100)==0
        disp(['ax: ' num2str(ax) ' ay: ' num2str(ay) ' az: ' num2str(az) ' gx: ' num2str(gx) ' gy: ' num2str(gy) ' gz: ' num2str(gz)]);
    end
    W3(k,1:4) = [ax ay az 1]; % Fill measurement matrix
    GX3 = GX3 + gx;
    GY3 = GY3 + gy;
    GZ3 = GZ3 + gz;
end
fclose(s);
GX3 = GX3/nmeas; % Take the average
GY3 = GY3/nmeas;
GZ3 = GZ3/nmeas; 

disp('Y-Axis up');
pause(pause_time);
disp('Starting measurement...')
Y4 = repmat([0 1 0],nmeas,1); % Output vector for Yup
fopen(s); % open serial port
for k = 1:nmeas
    [ax, ay, az, gx, gy, gz] = readIMU(s); %Read serial port
    if rem(k,100)==0
        disp(['ax: ' num2str(ax) ' ay: ' num2str(ay) ' az: ' num2str(az) ' gx: ' num2str(gx) ' gy: ' num2str(gy) ' gz: ' num2str(gz)]);
    end
    W4(k,1:4) = [ax ay az 1]; % Fill measurement matrix
    GX4 = GX4 + gx;
    GY4 = GY4 + gy;
    GZ4 = GZ4 + gz;
end
fclose(s);
GX4 = GX4/nmeas; % Take the average
GY4 = GY4/nmeas;
GZ4 = GZ4/nmeas;

disp('X-Axis down');
pause(pause_time);
disp('Starting measurement...')
Y5 = repmat([-1 0 0],nmeas,1); % Output vector for Xdown
fopen(s); % open serial port
for k = 1:nmeas
    [ax, ay, az, gx, gy, gz] = readIMU(s); %Read serial port
    if rem(k,100)==0
        disp(['ax: ' num2str(ax) ' ay: ' num2str(ay) ' az: ' num2str(az) ' gx: ' num2str(gx) ' gy: ' num2str(gy) ' gz: ' num2str(gz)]);
    end
    W5(k,1:4) = [ax ay az 1]; % Fill measurement matrix
    GX5 = GX5 + gx;
    GY5 = GY5 + gy;
    GZ5 = GZ5 + gz;
end
fclose(s);
GX5 = GX5/nmeas; % Take the average
GY5 = GY5/nmeas;
GZ5 = GZ5/nmeas;

disp('X-Axis up');
pause(pause_time);
disp('Starting measurement...')
Y6 = repmat([1 0 0],nmeas,1); % Output vector for Xup
fopen(s); % open serial port
for k = 1:nmeas
    [ax, ay, az, gx, gy, gz] = readIMU(s); %Read serial port
    if rem(k,100)==0
        disp(['ax: ' num2str(ax) ' ay: ' num2str(ay) ' az: ' num2str(az) ' gx: ' num2str(gx) ' gy: ' num2str(gy) ' gz: ' num2str(gz)]);
    end
    W6(k,1:4) = [ax ay az 1]; % Fill measurement matrix
    GX6 = GX6 + gx;
    GY6 = GY6 + gy;
    GZ6 = GZ6 + gz;
end
fclose(s);
GX6 = GX6/nmeas; % Take the average
GY6 = GY6/nmeas;
GZ6 = GZ6/nmeas;

% Generate measurement and output matrices
Y = [Y1; Y2; Y3; Y4; Y5; Y6];
W = [W1; W2; W3; W4; W5; W6];

% Calculate the least square solution

X = (W.'*W)\(W.'*Y); % Calibration matrix

% Calculate mean value of gyro readings
Gxc = (GX1 + GX2 + GX3 + GX4 + GX5 + GX6)/6;
Gyc = (GY1 + GY2 + GY3 + GY4 + GY5 + GY6)/6;
Gzc = (GZ1 + GZ2 + GZ3 + GZ4 + GZ5 + GZ6)/6;

disp('Calibration values [X, Gxc, Gyc, Gzc] are saved to workspace.')