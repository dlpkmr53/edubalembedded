function [ax, ay, az, gx, gy, gz] = readIMU(s)
    found_package_header = false;
    while ~found_package_header
        rcv_char = fread(s,1,'char');
        if rcv_char == 'E'
            rcv_char = fread(s,1,'char');
            if rcv_char == 'S'
                found_package_header = true;
            end
        end
    end
    data = double(fread(s,6,'int32'))*2^-17;
    gx = data(1);
    gy = data(2);
    gz = data(3);
    ax = data(4);
    ay = data(5);
    az = data(6);

end