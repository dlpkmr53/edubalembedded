% DC Motor Identification
% Pololu PO-2823
voltages = [2,4,6,8,10,12]; % Anchor voltage [V]
speed = [4.5,10.26,16.28,22.28,28.29,34.72]*30; % Motor speed before gear [rad/s]
current_extech = [121,163,192,211,229,241]/1000; % Anchor current [A]
current_fluke75 = [100,143,177,205,228,245]/1000;
current_fluke179 = [118,160,195,220,235,258]/1000;
current = current_fluke179;
R_extech = 16; % Anchor resistance [Ohm]
R_fluke75 = 5;
R_fluke179 =5.6;
R=R_fluke179;
phi = (voltages - R*current)./speed; % Motor constant

figure
hold on
plot(voltages, speed);
yyaxis right
plot(voltages, current);
figure
plot(voltages, phi);

load('increasing_step_sequence.mat');
figure
hold on
plot(ts.Time, ts.Data(:,1));
yyaxis right
plot(ts.Time, ts.Data(:,2));