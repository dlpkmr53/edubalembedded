function out = rn_form(System, Typ)
% Mit sysRNF = rn_form(System, 'RNF') wird ein System in die
% Regelungsnormalform transformiert.
% Mit Tr = rn_form(System, 'Tr') wird die Transformationsmatrix berechnet.
% Das System kann eine �bertragungsfunktion oder ein Zustandsmodell sein.
%
% Quelle: Helmut Bode (2010): Systeme der Regelungstechnik mit MATLAB und
% Simulink - Analyse und Simulation

if strcmp(Typ,'RNF') == 1
    fall = 0;
elseif strcmp(Typ,'Tr') == 1
    fall = 1;
end

[A,B,C,D] = ssdata(System);
[n,m] = size(B);
r = size(C);
r = r(1);

if m > 1
    disp('Achtung:')
    disp(['   Von den Matrizen B und D wird jeweils nur die zur '])
    disp('   ersten Steuergr��e geh�rende Spalte verwendet.')
end
b = B(:,1); d = D(:,1); %Beschr�nkung auf eine Steuergr��e, Auswahl der ersten Zeilen
st = ctrb(A,b); %Steuerbarkeitsmatrix
nst = length(A)-rank(st); %Differenz von Systemordnung und rank(st)
if nst >= 1
    fall = 2; %nicht steuerbar
else
    % Vereinbarungen
    M = zeros(size(A));
    Tr = zeros(size(A));
    for k = 1:size(A), M(k,:) = (A^(k-1)*b)'; end
    tz(n,1) = 1; t1 = (M)^-1*tz;
    for k = 1:size(A), Tr(k,:) = t1'*A^(k-1); end
end

switch fall
    case 0 % R�ckgabe des Zustandsmodell in der RNF
        Ar = Tr*A*(Tr)^-1;
        br = Tr*b;
        Cr = C*(Tr)^-1;
        dr = d;
         for k = 1:n % Ersetzen von Werten < 1e-5 durch 0
             for j= 1:n
                 if abs(Ar(k,j)) < 1e-5, Ar(k,j) = 0; end
             end
             if abs(br(k,1)) < 1e-5, br(k,1) = 0; end
         end
         for k = 1:r % Ersetzen von Werten < 1e-5 durch 0
             for j = 1:n
                 if abs(Cr(k,j)) < 1e-5, Cr(k,j) = 0; end
             end
         end
        out = ss(Ar,br,Cr,dr);
    case 1 % R�ckgabe der Transformationsmatrix
        out = Tr;
    case 2
        disp('Achtung')
        disp(['   Mit der ersten Steuergr��e ist das System nicht steuerbar.'])
    otherwise
        error('Die Abk�rzung f�r "Typ" ist falsch. Bitte entweder "RNF" oder "Tr" �bergeben.')
end