close all 
clear all
clc

opts = bodeoptions('cstprefs');
opts.MagUnits = 'abs';
opts.MagScale = 'log';
opts.PhaseVisible = 'off';

%% Mini Segway parameters

m = 0.933;                  % body part mass [kg] 
R = 0.04;                   % radius of wheel [m]
L_Theta = 0.085749;         % position of COM [m]
I = 0.00686014;             % inertia of body part [kg*m^2]       
Bm = 0;                     % bearing damping ratio [N*m/(rad/s)]
g = 9.81;                   % gravity constant [m/s^2]
Tau = 0.0994;               % speed loop time constant (P=0.194775 I=3.387779)
Kv = 1.0071;                % speed loop gain
Ts = 0.01;                  % controller sampling time
  
%% State-space representation of the system (with actuator simplified as PT1)
% State variable: [Phi, Phidot, Theta, Thetadot]'  
% System matrix
A = [ 0                      1                                       0                          0           ;...
      0                   -1/Tau                                     0                          0           ;...
      0                      0                                       0                          1           ;...
      0     (Bm+m*R*L_Theta/Tau)/(I+m*L_Theta^2)         m*g*L_Theta/(I+m*L_Theta^2)   -Bm/(I+m*L_Theta^2)  ];

% Input matrix
B = [0;  Kv/Tau;  0;  -Kv*m*R*L_Theta/(Tau*(I+m*L_Theta^2))];

% Output matrix
C = [0 1 0 0; 0 0 1 0];

% Feedforward matrix
D = [0; 0];

%% System analysis

dim = size(A);          % dimension of the state transition matrix
n =  dim(1);            % system order
sys = ss(A,B,C,D);      % state space model of the system 
sysd = c2d(sys,Ts);     % discrete model
sys.StateName = {'wheel angle (rad)'; 'wheel vel (rad/s)';...
                 'body angle (rad)';  'body vel (rad/s)'};

% tzero(sys('Phidot','Phidot_ref'))

poles = eig(sys);     % poles of the system
G = tf(sys);          % transfer function of sys 
zpk(G)
% rlocus(G);            % place of poles and zeros

co = rank(ctrb(sys)); % check for rank of controllability matrix
if co == n
    disp('System is controllable');
else
    disp('System is not controllable');
end

%% LQR controller design 

Q = [1;1e-2;1;1e-2].*eye(4);        % weighting matrix Q 
R = 100;                            % weighting matrix R 
K = lqr(A,B,Q,R);                   % LQR gain matrix 
Kd = lqrd(A,B,Q,R,Ts);              % discrete LQR gain

Ac = (A-B*K);
Bc = B;
Cc = C;
Dc = D;

sys1_lqr = ss(Ac,Bc,Cc,Dc);         % close-loop system
% initial(sys1_lqr, [0; 0; 0.17; 0])  % free response 

CLtf_lqr = tf(sys1_lqr);

% Weighting based on the sensitivities

s = tf('s');

Wp1_lqr = tf(conv([1 7.575],conv([1 7.552],[1 0.1007])), conv([1 7.563],conv([1 1e-6],[1 1e-6])));
Wp2_lqr = tf(conv([1 7.575],[1 0.1007]), conv([1 1.587e-08],[1 1e-6]));                
  
%% Compute the Weighting Functions

% S/KS problem: objective is to minimize N = [WpS; WuKS]

%--------------------------------------------------------------------------
% S is the transfer function from r to -e = r-y
% Common choice for the performance weight Wp (Page 95 - Skogestad): 
%   * A<<1 ensures approximate integral action with S(0)~0
%   * M ~ 2 for all outputs and wB diverges for each output
%   * large wB yields a faster response for output i

wB1 = 0.1; wB2 = 0.1; M1 = 3; M2 = 3; A1 = 1e-6; A2 = 1.1;

Wp1 = tf([1/M1 wB1], [1 wB1*A1]);
Wp2 = tf([1/M2 wB2], [1 wB2*A2]);

% Wp1 = tf(conv([1/sqrt(M1) wB1],[1/sqrt(M1) wB1]), conv([1 wB1*sqrt(A1)],[1 wB2*sqrt(A1)]));  % Weights.
% Wp2 = tf(conv([1/sqrt(M2) wB2],[1/sqrt(M2) wB2]), conv([1 wB2*sqrt(A2)],[1 wB2*sqrt(A2)]));  % Weights.

%--------------------------------------------------------------------------
% KS is the transfer fucntion from references r to inputs u
%   * Reasonable choice for the input weight is Wu = I
%   * If a tight control at low frequencies is required (i.e. A is small),
%   then input usage is unavoidable at low frequencies, and it may be
%   better to use a weight of the form Wu = s/(s+w1), where w1 is
%   approximatelly the closed-loop bandwidth.

Wu = 0.1*tf([1 0],[1000 1]);
%Wu = 5e7*tf(1); 
% Wu = 0.01*tf([1 0],conv([100 1],[1000 1]));
% Wu = 0.1*tf(conv([1 0],[1 0]),conv([100 1],[100 1])); %

%--------------------------------------------------------------------------
% T is the transfer function from r to y (or from n to y)
%   * To reduce sensitivity to noise or uncertainty, we want T small at
%   high frequencies , and thus additional roll-off in L.

wBt1 = 1e4; Mt1 = 0.01; At1 = 2;  wBt2 = 14; Mt2 = 2; At2 = 1;

% Wt = [];
Wt1 = tf([1/Mt1 wBt1], [1 wBt1*At1]);
Wt2 = tf([1/Mt2 wBt2], [1 wBt1*At2]);

%--------------------------------------------------------------------------
% Weighting for the reference

Wr1 = tf(1);
Wr2 = tf(1);

%% Pole cancellation for theta

N = tf([1 -7.563],[1 1e-10]);
N1 = inv(N);

newG = [G(1); G(2)*N];

%% augmented plant & hinfsys

sys.InputName = {'Phidot_u'};
sys.OutputName = {'Phidot';'Theta'};        % measured data

% newG.InputName = {'Phidot_u'};
% newG.OutputName = {'Phidot';'Theta'};        % measured data

Phidot_error = sumblk('y1 = Phidot_ref - Phidot');
%Gd = sumblk('y = Theta_ref + d');
Theta_error = sumblk('y2 = Theta_ref - Theta');

% Wr1.InputName = {'Phidot_ref'}; 
% Wr1.OutputName = {'Phidot_ref_Wr1'}; 
% Wr2.InputName = {'Theta_ref'}; 
% Wr2.OutputName = {'Theta_ref_Wr2'}; 
% Phidot_error = sumblk('y1 = Phidot_ref_Wr1 - Phidot');
% Gd = sumblk('y = Theta_ref_Wr2 + d');
% Theta_error = sumblk('y2 = Theta_ref_Wr2 - Theta');

Wp1.InputName = {'y1'};
Wp1.OutputName = {'z1'};

% N1.InputName = {'y2'};
% N1.OutputName = {'y2new'};

Wp2.InputName = {'y2'};
Wp2.OutputName = {'z2'};

Wu.InputName = {'Phidot_u'};
Wu.OutputName = {'z3'};

Wt1.InputName = {'Phidot'};
Wt1.OutputName = {'z4'};

Wt2.InputName = {'Theta'};
Wt2.OutputName = {'z5'};

ICInputs = {'Phidot_ref';'Theta_ref';'Phidot_u'};
%ICInputs = {'Phidot_ref_Wr1';'Theta_ref_Wr2';'Phidot_u'};
ICOutputs ={'z1';'z2';'z3';'z4';'z5';'y1';'y2'};

P = connect(sys, Wp1, Wp2, Wu, Wt1, Wt2, Phidot_error, Theta_error, ICInputs, ICOutputs);
%P = connect(newG, N1, Wp1, Wp2, Wu, Phidot_error, Theta_error, ICInputs, ICOutputs);

P = minreal(P);

nmeas = 2;
ncont = 1;

% [r,c] = size(P);
% NU = 3;
% NY = 5;
% P.InputGroup  = struct('U1',1:c-NU,'U2',c-NU+1:c);
% P.OutputGroup = struct('Y1',1:r-NY,'Y2',r-NY+1:r);

%% Hinf-Synthese

hopts = hinfsynOptions;
hopts.AutoScale='on';
hopts.LimitGain='off';
hopts.Display = 'on';
hopts.Method = 'RIC'; % 'lmi';

[K_hinfa, CL, gam] = hinfsyn(P, nmeas, ncont, hopts);
K_hinf = minreal(K_hinfa);

K_hinfd = c2d(K_hinf, Ts);      % discrete controller

Ktf = tf(K_hinf);

Ktf(1).u = 'y1';
Ktf.y = 'Phidot_u';
Ktf(2).u = 'y2';

%% Open Loop Analysis

L = G*K_hinf;           % loop transfer function
Marg = allmargin(L);
Marg_phidot = Marg(1,1)
Marg_theta = Marg(2,1)

%% Mixed-Sensitivity Analysis

% Complementary Sensitivity Function (Transfer function from r to y)

T1 = connect(sys,Ktf,Phidot_error,Theta_error,{'Phidot_ref', 'Theta_ref'},{'Phidot','Theta','Phidot_u'});      % gives also T for control output     

T = feedback(L,eye(2));

figure
bode(T(1,1),1/Wt1,opts)
legend('T1','1/Wt1','location','southeast')
title('Sensitivity for Phi_{dot}')

figure
bode(T(2,2),1/Wt2,opts)
legend('T2','1/Wt2')
title('Sensitivity for Theta')

%--------------------------------------------------------------------------
% Sensitivity function (Transfer function from r to e)

S = feedback(eye(2),L);

figure
bode(S(1,1),1/Wp1,opts)
legend('S1','1/Wp1','location','southeast')
title('Sensitivity for Phi_{dot}')

figure
bode(S(2,2),1/Wp2,opts)
legend('S2','1/Wp2')
title('Sensitivity for Theta')

%--------------------------------------------------------------------------
% Transfer function from r to u

KS = K_hinf*S;

figure
bode(KS(1),KS(2),1/Wu,opts)
legend('KS1','KS2','1/Wu')
title('From r to u')

%--------------------------------------------------------------------------
% Phidot

[a,b] = ss2tf(K_hinf(1).A,K_hinf(1).B,K_hinf(1).C,K_hinf(1).D);
Ktf_Phidot = tf(a,b);
zpk(Ktf_Phidot)

Wp1Sphidot = CL(1,1);        % |Wp * S_hinf| should be smaller than 1 for all frequency
WuKSphidot = CL(3,1);        % |Wu * K_hinf * S_hinf| should be smaller than 1 for all frequency

figure

bode(S(1,1), opts)
hold on
bode(1/Wp1, opts)
hold on
bode(Wp1Sphidot,opts)
hold on
bode(WuKSphidot,opts)
legend('S','1/Wp','WpS','WuKS');

%--------------------------------------------------------------------------
% Theta

[a,b] = ss2tf(K_hinf(2).A,K_hinf(2).B,K_hinf(2).C,K_hinf(2).D);
Ktf_Theta = tf(a,b);
zpk(Ktf_Theta)

Wp2Stheta = CL(2,2);        % |Wp * S_hinf| should be smaller than 1 for all frequency
WuKStheta = CL(3,2);        % |Wu * K_hinf * S_hinf| should be smaller than 1 for all frequency

figure

bode(S(2,2), opts)
hold on
bode(1/Wp2, opts)
hold on
bode(Wp2Stheta,opts)
hold on
bode(WuKStheta,opts)
legend('S','1/Wp','WpS','WuKS');

%% Step response

figure
t = (0:0.01:50);
y = step(CLtf_lqr,t);
plot(t, y)
legend('Phidot','Theta');
title('LQR Step Response');
