% Load Simulink bus files
load('MSSimulinkBuses.mat');
load('StandardConfigurationParameters.mat');

try
    [com_port_string, ~] = getFTDICOMPort;
    setExternalModeCOMPort(cs, com_port_string);
catch e
    disp(['Automatic identification of external COM port not available. Error: ' e.message]);
end

MSModelConfig.BodyAngleOperatingPoint = 10.5;        % Body angle target for controller [deg]

% Variant Subsystem parameters

% Remote connection: 1=None, 2=Wifi, 3=SPI
MSRemoteConnection = 1;

% Body Controller: 1=LQR, 2=LQIpFF, 3=LQR_raffi, 4=SMC, 5=Hinf, 
MSBodyController = 1;

% Model execution rates (change only if you know what you're doing)
MSModelConfig.dt=0.01;        % Controller execution rate
MSModelConfig.dt_SCI = 0.01;  % Maximum SCI transmit block execution rate