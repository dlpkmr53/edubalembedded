function cs = EdubalModelConfig()
% MATLAB function for configuration set generated on 06-Oct-2020 18:07:28
% MATLAB version: 9.6.0.1472908 (R2019a) Update 9

cs = Simulink.ConfigSet;

% Original configuration set version: 19.0.0
if cs.versionCompare('19.0.0') < 0
    error('Simulink:MFileVersionViolation', 'The version of the target configuration set is older than the original configuration set.');
end

% Original environment character encoding: windows-1252
if ~strcmpi(get_param(0, 'CharacterEncoding'), 'windows-1252')
    warning('Simulink:EncodingUnMatched', 'The target character encoding (%s) is different from the original (%s).',  get_param(0, 'CharacterEncoding'), 'windows-1252');
end

% Do not change the order of the following commands. There are dependencies between the parameters.
cs.set_param('Name', 'Configuration'); % Name
cs.set_param('Description', ''); % Description

% Original configuration set target is grt.tlc
cs.switchTarget('ert.tlc','');

cs.set_param('HardwareBoard', 'TI Piccolo F28069M LaunchPad');   % Hardware board

% Solver
cs.set_param('StartTime', '0.0');   % Start time
cs.set_param('StopTime', 'Inf');   % Stop time
cs.set_param('SolverType', 'Fixed-step');   % Type
cs.set_param('Solver', 'FixedStepDiscrete');   % Solver
cs.set_param('FixedStep', 'auto');   % Fixed-step size
cs.set_param('SampleTimeConstraint', 'Unconstrained');   % Periodic sample time constraint
cs.set_param('EnableMultiTasking', 'on');   % Treat each discrete rate as a separate task
cs.set_param('ConcurrentTasks', 'off');   % Allow tasks to execute concurrently on target
cs.set_param('AutoInsertRateTranBlk', 'off');   % Automatically handle rate transition for data transfer
cs.set_param('PositivePriorityOrder', 'off');   % Higher priority value indicates higher task priority
cs.set_param('DecoupledContinuousIntegration', 'off');   % Enable decoupled continuous integration
cs.set_param('MinimalZcImpactIntegration', 'off');   % Enable minimal zero-crossing impact integration

% Possibly unused solver settings
cs.set_param('ZeroCrossControl', 'UseLocalSettings');   % Zero-crossing control
cs.set_param('ZeroCrossAlgorithm', 'Nonadaptive');   % Algorithm
cs.set_param('AutoInsertRateTranBlk', 'off');   % Automatically handle rate transition for data transfer

% Data Import/Export
cs.set_param('Decimation', '1');   % Decimation
cs.set_param('LoadExternalInput', 'off');   % Load external input
cs.set_param('SaveFinalState', 'off');   % Save final state
cs.set_param('LoadInitialState', 'off');   % Load initial state
cs.set_param('LimitDataPoints', 'off');   % Limit data points
cs.set_param('SaveFormat', 'Dataset');   % Format
cs.set_param('SaveOutput', 'on');   % Save output
cs.set_param('SaveState', 'off');   % Save states
cs.set_param('SignalLogging', 'on');   % Signal logging
cs.set_param('DSMLogging', 'on');   % Data stores
cs.set_param('InspectSignalLogs', 'off');   % Record logged workspace data in Simulation Data Inspector
cs.set_param('SaveTime', 'on');   % Save time
cs.set_param('ReturnWorkspaceOutputs', 'off');   % Single simulation output
cs.set_param('TimeSaveName', 'tout');   % Time variable
cs.set_param('OutputSaveName', 'yout');   % Output variable
cs.set_param('SignalLoggingName', 'logsout');   % Signal logging name
cs.set_param('DSMLoggingName', 'dsmout');   % Data stores logging name
cs.set_param('LoggingToFile', 'off');   % Log Dataset data to file
cs.set_param('DatasetSignalFormat', 'timeseries');   % Dataset signal format
cs.set_param('LoggingIntervals', '[-inf, inf]');   % Logging intervals

% Optimization
cs.set_param('BlockReduction', 'on');   % Block reduction
cs.set_param('DataBitsets', 'off');   % Use bitsets for storing Boolean data
cs.set_param('StateBitsets', 'off');   % Use bitsets for storing state configuration
cs.set_param('EnableMemcpy', 'on');   % Use memcpy for vector assignment
cs.set_param('BufferReuse', 'on');   % Reuse local block outputs
cs.set_param('ExpressionFolding', 'on');   % Eliminate superfluous local variables (expression folding)
cs.set_param('LocalBlockOutputs', 'on');   % Enable local block outputs
cs.set_param('OptimizeBlockIOStorage', 'on');   % Signal storage reuse
cs.set_param('ConditionallyExecuteInputs', 'on');   % Conditional input branch execution
cs.set_param('BooleanDataType', 'on');   % Implement logic signals as Boolean data (vs. double)
cs.set_param('LifeSpan', 'auto');   % Application lifespan (days)
cs.set_param('UseDivisionForNetSlopeComputation', 'off');   % Use division for fixed-point net slope computation
cs.set_param('GainParamInheritBuiltInType', 'off');   % Gain parameters inherit a built-in integer type that is lossless
cs.set_param('UseFloatMulNetSlope', 'off');   % Use floating-point multiplication to handle net slope corrections
cs.set_param('DefaultUnderspecifiedDataType', 'single');   % Default for underspecified data type
cs.set_param('InitFltsAndDblsToZero', 'on');   % Use memset to initialize floats and doubles to 0.0
cs.set_param('EfficientFloat2IntCast', 'off');   % Remove code from floating-point to integer conversions that wraps out-of-range values
cs.set_param('EfficientMapNaN2IntZero', 'on');   % Remove code from floating-point to integer conversions with saturation that maps NaN to zero
cs.set_param('SimCompilerOptimization', 'off');   % Compiler optimization level
cs.set_param('AccelVerboseBuild', 'off');   % Verbose accelerator builds
cs.set_param('DefaultParameterBehavior', 'Tunable');   % Default parameter behavior
cs.set_param('MemcpyThreshold', 64);   % Memcpy threshold (bytes)
cs.set_param('RollThreshold', 5);   % Loop unrolling threshold
cs.set_param('MaxStackSize', '512');   % Maximum stack size (bytes)
cs.set_param('ActiveStateOutputEnumStorageType', 'Native Integer');   % Base storage type for automatically created enumerations
cs.set_param('BufferReusableBoundary', 'on');   % Buffer for reusable subsystems
cs.set_param('UseRowMajorAlgorithm', 'off');   % Use algorithms optimized for row-major array layout
cs.set_param('DenormalBehavior', 'GradualUnderflow');   % In accelerated simulation modes, denormal numbers can be flushed to zero using the 'flush-to-zero' option.

% Diagnostics
cs.set_param('AlgebraicLoopMsg', 'warning');   % Algebraic loop
cs.set_param('ArtificialAlgebraicLoopMsg', 'warning');   % Minimize algebraic loop
cs.set_param('BlockPriorityViolationMsg', 'warning');   % Block priority violation
cs.set_param('MinStepSizeMsg', 'warning');   % Min step size violation
cs.set_param('TimeAdjustmentMsg', 'none');   % Sample hit time adjusting
cs.set_param('MaxConsecutiveZCsMsg', 'error');   % Consecutive zero crossings violation
cs.set_param('UnknownTsInhSupMsg', 'warning');   % Unspecified inheritability of sample time
cs.set_param('ConsistencyChecking', 'none');   % Solver data inconsistency
cs.set_param('SolverPrmCheckMsg', 'none');   % Automatic solver parameter selection
cs.set_param('ModelReferenceExtraNoncontSigs', 'error');   % Extraneous discrete derivative signals
cs.set_param('StateNameClashWarn', 'none');   % State name clash
cs.set_param('OperatingPointInterfaceChecksumMismatchMsg', 'warning');   % Operating point restore interface checksum mismatch
cs.set_param('NonCurrentReleaseOperatingPointMsg', 'error');   % Operating point object from a different release
cs.set_param('InheritedTsInSrcMsg', 'warning');   % Source block specifies -1 sample time
cs.set_param('MultiTaskRateTransMsg', 'error');   % Multitask rate transition
cs.set_param('SingleTaskRateTransMsg', 'none');   % Single task rate transition
cs.set_param('MultiTaskCondExecSysMsg', 'error');   % Multitask conditionally executed subsystem
cs.set_param('TasksWithSamePriorityMsg', 'warning');   % Tasks with equal priority
cs.set_param('SigSpecEnsureSampleTimeMsg', 'warning');   % Enforce sample times specified by Signal Specification blocks
cs.set_param('SignalResolutionControl', 'UseLocalSettings');   % Signal resolution
cs.set_param('CheckMatrixSingularityMsg', 'none');   % Division by singular matrix
cs.set_param('IntegerSaturationMsg', 'warning');   % Saturate on overflow
cs.set_param('UnderSpecifiedDataTypeMsg', 'none');   % Underspecified data types
cs.set_param('UnderSpecifiedDimensionMsg', 'none');   % Underspecified dimensions
cs.set_param('SignalRangeChecking', 'none');   % Simulation range checking
cs.set_param('IntegerOverflowMsg', 'warning');   % Wrap on overflow
cs.set_param('SignalInfNanChecking', 'none');   % Inf or NaN block output
cs.set_param('StringTruncationChecking', 'error');   % String truncation checking
cs.set_param('RTPrefix', 'error');   % "rt" prefix for identifiers
cs.set_param('ParameterDowncastMsg', 'error');   % Detect downcast
cs.set_param('ParameterOverflowMsg', 'error');   % Detect overflow
cs.set_param('ParameterUnderflowMsg', 'none');   % Detect underflow
cs.set_param('ParameterPrecisionLossMsg', 'warning');   % Detect precision loss
cs.set_param('ParameterTunabilityLossMsg', 'error');   % Detect loss of tunability
cs.set_param('ReadBeforeWriteMsg', 'UseLocalSettings');   % Detect read before write
cs.set_param('WriteAfterReadMsg', 'UseLocalSettings');   % Detect write after read
cs.set_param('WriteAfterWriteMsg', 'UseLocalSettings');   % Detect write after write
cs.set_param('MultiTaskDSMMsg', 'none');   % Multitask data store
cs.set_param('UniqueDataStoreMsg', 'none');   % Duplicate data store names
cs.set_param('UnderspecifiedInitializationDetection', 'Classic');   % Underspecified initialization detection
cs.set_param('MergeDetectMultiDrivingBlocksExec', 'none');   % Detect multiple driving blocks executing at the same time step
cs.set_param('ArrayBoundsChecking', 'none');   % Array bounds exceeded
cs.set_param('AssertControl', 'UseLocalSettings');   % Model Verification block enabling
cs.set_param('AllowSymbolicDim', 'on');   % Allow symbolic dimension specification
cs.set_param('ArithmeticOperatorsInVariantConditions', 'warning');   % Arithmetic operations in variant conditions
cs.set_param('UnnecessaryDatatypeConvMsg', 'none');   % Unnecessary type conversions
cs.set_param('VectorMatrixConversionMsg', 'none');   % Vector/matrix block input conversion
cs.set_param('Int32ToFloatConvMsg', 'warning');   % 32-bit integer to single precision float conversion
cs.set_param('FixptConstUnderflowMsg', 'none');   % Detect underflow
cs.set_param('FixptConstOverflowMsg', 'none');   % Detect overflow
cs.set_param('FixptConstPrecisionLossMsg', 'none');   % Detect precision loss
cs.set_param('SignalLabelMismatchMsg', 'none');   % Signal label mismatch
cs.set_param('UnconnectedInputMsg', 'warning');   % Unconnected block input ports
cs.set_param('UnconnectedOutputMsg', 'warning');   % Unconnected block output ports
cs.set_param('UnconnectedLineMsg', 'warning');   % Unconnected line
cs.set_param('RootOutportRequireBusObject', 'warning');   % Unspecified bus object at root Outport block
cs.set_param('BusObjectLabelMismatch', 'warning');   % Element name mismatch
cs.set_param('StrictBusMsg', 'ErrorLevel1');   % Bus signal treated as vector
cs.set_param('NonBusSignalsTreatedAsBus', 'none');   % Non-bus signals treated as bus signals
cs.set_param('BusNameAdapt', 'WarnAndRepair');   % Repair bus selections
cs.set_param('FcnCallInpInsideContextMsg', 'error');   % Context-dependent inputs
cs.set_param('SFcnCompatibilityMsg', 'none');   % S-function upgrades needed
cs.set_param('FrameProcessingCompatibilityMsg', 'error');   % Block behavior depends on frame status of signal
cs.set_param('ModelReferenceVersionMismatchMessage', 'none');   % Model block version mismatch
cs.set_param('ModelReferenceIOMismatchMessage', 'none');   % Port and parameter mismatch
cs.set_param('ModelReferenceIOMsg', 'none');   % Invalid root Inport/Outport block connection
cs.set_param('ModelReferenceDataLoggingMessage', 'warning');   % Unsupported data logging
cs.set_param('SaveWithDisabledLinksMsg', 'warning');   % Block diagram contains disabled library links
cs.set_param('SaveWithParameterizedLinksMsg', 'warning');   % Block diagram contains parameterized library links
cs.set_param('SFUnusedDataAndEventsDiag', 'warning');   % Unused data, events, messages and functions
cs.set_param('SFUnexpectedBacktrackingDiag', 'error');   % Unexpected backtracking
cs.set_param('SFInvalidInputDataAccessInChartInitDiag', 'warning');   % Invalid input data access in chart initialization
cs.set_param('SFNoUnconditionalDefaultTransitionDiag', 'error');   % No unconditional default transitions
cs.set_param('SFTransitionOutsideNaturalParentDiag', 'warning');   % Transition outside natural parent
cs.set_param('SFUnreachableExecutionPathDiag', 'warning');   % Unreachable execution path
cs.set_param('SFUndirectedBroadcastEventsDiag', 'warning');   % Undirected event broadcasts
cs.set_param('SFTransitionActionBeforeConditionDiag', 'warning');   % Transition action specified before condition action
cs.set_param('SFOutputUsedAsStateInMooreChartDiag', 'error');   % Read-before-write to output in Moore chart
cs.set_param('SFTemporalDelaySmallerThanSampleTimeDiag', 'warning');   % Absolute time temporal value shorter than sampling period
cs.set_param('SFSelfTransitionDiag', 'warning');   % Self-transition on leaf state
cs.set_param('SFExecutionAtInitializationDiag', 'warning');   % 'Execute-at-initialization' disabled in presence of input events
cs.set_param('SFMachineParentedDataDiag', 'warning');   % Use of machine-parented data instead of Data Store Memory
cs.set_param('IgnoredZcDiagnostic', 'warning');   % Ignored zero crossings
cs.set_param('InitInArrayFormatMsg', 'warning');   % Initial state is array
cs.set_param('MaskedZcDiagnostic', 'warning');   % Masked zero crossings
cs.set_param('ModelReferenceSymbolNameMessage', 'none');   % Insufficient maximum identifier length
cs.set_param('AllowedUnitSystems', 'all');   % Allowed unit systems
cs.set_param('UnitsInconsistencyMsg', 'warning');   % Units inconsistency messages
cs.set_param('AllowAutomaticUnitConversions', 'on');   % Allow automatic unit conversions
cs.set_param('ForceCombineOutputUpdateInSim', 'off');   % Combine output and update methods for code generation and simulation
cs.set_param('DebugExecutionForFMUViaOutOfProcess', 'off');   % FMU Import blocks

% Hardware Implementation
cs.set_param('ProdHWDeviceType', 'Texas Instruments');   % Production device vendor and type
cs.set_param('ProdLongLongMode', 'on');   % Support long long
cs.set_param('ProdEqTarget', 'on');   % Test hardware is the same as production hardware
cs.set_param('TargetPreprocMaxBitsSint', 32);   % Maximum bits for signed integer in C preprocessor
cs.set_param('TargetPreprocMaxBitsUint', 32);   % Maximum bits for unsigned integer in C preprocessor
cs.set_param('HardwareBoardFeatureSet', 'EmbeddedCoderHSP');   % Feature set for selected hardware board

% TI board-specific settings
codertargetdata = get_param(cs, 'CoderTargetData');

codertargetdata.UseCoderTarget = 1;
codertargetdata.TargetHardware = 'TI Piccolo F28069M LaunchPad';

codertargetdata.ConnectionInfo.serial.IPAddress = 'codertarget.registry.getLoopbackIP;';
codertargetdata.ConnectionInfo.serial.Port = '17725';
codertargetdata.ConnectionInfo.serial.Verbose = 0;
codertargetdata.ConnectionInfo.CAN.MEXArgs = ' ';

codertargetdata.ExtMode.Configuration = 'serial';

codertargetdata.RTOS = 'Baremetal';

codertargetdata.Scheduler_interrupt_source = 0;

codertargetdata.Runtime.BuildAction = 'Build, load and run';
codertargetdata.Runtime.DeviceID = 'F28069M';
codertargetdata.Runtime.FlashLoad = 1;
codertargetdata.Runtime.BootloaderProgrammingSupport = 0;
codertargetdata.Runtime.BootloaderChoice = 'Serial (SCI_A)';
codertargetdata.Runtime.BootloaderCOMPort = 'COM1';
codertargetdata.Runtime.LoadCommandArg = '$(TARGET_ROOT)/CCS_Config/f28069.ccxml';
codertargetdata.Runtime.DMAAccess = 0;
codertargetdata.Runtime.BootloaderCompatibleOS = 1;

codertargetdata.TargetLinkObj.UseCustomLinker = 1;
codertargetdata.TargetLinkObj.Name = '$(TARGET_ROOT)\src\c28069M.cmd';

codertargetdata.Clocking.cpuClockRateMHz = '90';
codertargetdata.Clocking.UseInternalOsc = 1;
codertargetdata.Clocking.OSCCLK = '10';
codertargetdata.Clocking.AutoSetPllSettings = 1;
codertargetdata.Clocking.PLLCR = '9';
codertargetdata.Clocking.DIVSEL = '(OSCCLK * PLLCR)/1';
codertargetdata.Clocking.ClosestCpuClock = '90';
codertargetdata.Clocking.LspclkDiv = 'SYSCLKOUT/1';
codertargetdata.Clocking.LSPCLK = '90';

codertargetdata.ADC.ClockDiv = 'SYSCLKOUT/2';
codertargetdata.ADC.ClockFrequency = 45;
codertargetdata.ADC.Nonoverlap = 'Allowed';
codertargetdata.ADC.OffsetCorrectionValue = 'AdcRegs.ADCOFFTRIM.bit.OFFTRIM';
codertargetdata.ADC.ExternalReferenceSelector = 0;
codertargetdata.ADC.ExternalReferenceVREFHI = '3.3';
codertargetdata.ADC.ExternalReferenceVREFLO = '0';
codertargetdata.ADC.INTPulseGeneration = 'Late interrupt pulse';
codertargetdata.ADC.SOCpriority = 'All in round robin mode';
codertargetdata.ADC.XINT2GPIO = 'GPIO0';

codertargetdata.COMP.PinAssignment_COMP1 = 'GPIO1';
codertargetdata.COMP.PinAssignment_COMP2 = 'GPIO3';
codertargetdata.COMP.PinAssignment_COMP3 = 'GPIO34';

codertargetdata.eCAN_A.ModuleClockFrequency = '45';
codertargetdata.eCAN_A.BaudRatePrescaler = 5;
codertargetdata.eCAN_A.TSEG1 = '5';
codertargetdata.eCAN_A.TSEG2 = '3';
codertargetdata.eCAN_A.BaudRate = '1000000';
codertargetdata.eCAN_A.SBG = 'Only_falling_edges';
codertargetdata.eCAN_A.SJW = '2';
codertargetdata.eCAN_A.SAM = 'Sample_one_time';
codertargetdata.eCAN_A.EnhancedCANMode = 1;
codertargetdata.eCAN_A.SelfTestMode = 0;

codertargetdata.eCAP.PinAssignment_ECAP1 = 'GPIO19';
codertargetdata.eCAP.PinAssignment_ECAP2 = 'GPIO7';
codertargetdata.eCAP.PinAssignment_ECAP3 = 'GPIO9';

codertargetdata.ePWM.PinAssignment_TZ1 = 'None';
codertargetdata.ePWM.PinAssignment_TZ2 = 'None';
codertargetdata.ePWM.PinAssignment_TZ3 = 'None';
codertargetdata.ePWM.PinAssignment_SYNCI = 'None';
codertargetdata.ePWM.PinAssignment_SYNCO = 'None';
codertargetdata.ePWM.PinAssignment_PWM7A = 'GPIO40';
codertargetdata.ePWM.PinAssignment_PWM7B = 'GPIO41';
codertargetdata.ePWM.PinAssignment_PWM8A = 'GPIO42';

codertargetdata.I2C.Mode = 'Master';
codertargetdata.I2C.AddrDataFormat = '7-Bit Addressing';
codertargetdata.I2C.OwnAddress = '1';
codertargetdata.I2C.BitCount = '8';
codertargetdata.I2C.ModuleClockPrescaler = '8';
codertargetdata.I2C.ModuleClockFrequency = 10000000;
codertargetdata.I2C.MasterClkLowTime = '10';
codertargetdata.I2C.MasterClkHighTime = '5';
codertargetdata.I2C.MasterClockFrequency = 400000;
codertargetdata.I2C.MasterClockFrequency_1 = 400000;
codertargetdata.I2C.MasterClockFrequency_2 = 400000;
codertargetdata.I2C.EnableLoopback = 0;
codertargetdata.I2C.PinAssignment_SDAA = 'GPIO32';
codertargetdata.I2C.PinAssignment_SCLA = 'GPIO33';
codertargetdata.I2C.EnableTxInt = 0;
codertargetdata.I2C.TxFifoLevel = '0';
codertargetdata.I2C.EnableRxInt = 0;
codertargetdata.I2C.RxFifoLevel = '0';
codertargetdata.I2C.EnableSysInt = 0;
codertargetdata.I2C.AAS = 0;
codertargetdata.I2C.SCD = 0;
codertargetdata.I2C.ARDY = 0;
codertargetdata.I2C.NACK = 0;
codertargetdata.I2C.AL = 0;

codertargetdata.SCI_A.EnableLoopBack = 0;
codertargetdata.SCI_A.SuspensionMode = 'Free_run';
codertargetdata.SCI_A.NumberOfStopBits = '1';
codertargetdata.SCI_A.ParityMode = 'None';
codertargetdata.SCI_A.CharacterLengthBits = '8';
codertargetdata.SCI_A.UserBaudRate = '115200';
codertargetdata.SCI_A.BaudRatePrescaler = 97;
codertargetdata.SCI_A.BaudRate = 114796;
codertargetdata.SCI_A.CommunicationMode = 'Raw_data';
codertargetdata.SCI_A.BlockingMode = 0;
codertargetdata.SCI_A.DataByteOrder = 'Little_Endian';
codertargetdata.SCI_A.DataSwapWidth = '8_bits';
codertargetdata.SCI_A.PinAssignment_Tx = 'GPIO29';
codertargetdata.SCI_A.PinAssignment_Rx = 'GPIO28';

codertargetdata.SCI_B.EnableLoopBack = 0;
codertargetdata.SCI_B.SuspensionMode = 'Free_run';
codertargetdata.SCI_B.NumberOfStopBits = '1';
codertargetdata.SCI_B.ParityMode = 'None';
codertargetdata.SCI_B.CharacterLengthBits = '8';
codertargetdata.SCI_B.UserBaudRate = '921600';
codertargetdata.SCI_B.BaudRatePrescaler = 11;
codertargetdata.SCI_B.BaudRate = 937500;
codertargetdata.SCI_B.CommunicationMode = 'Raw_data';
codertargetdata.SCI_B.BlockingMode = 0;
codertargetdata.SCI_B.DataByteOrder = 'Little_Endian';
codertargetdata.SCI_B.DataSwapWidth = '8_bits';
codertargetdata.SCI_B.PinAssignment_Tx = 'GPIO58';
codertargetdata.SCI_B.PinAssignment_Rx = 'GPIO15';

codertargetdata.SPI_A.Mode = 'Slave';
codertargetdata.SPI_A.UserBaudRate = '921600';
codertargetdata.SPI_A.BaudRateFactor = 97;
codertargetdata.SPI_A.BaudRate = 918367;
codertargetdata.SPI_A.DataBits = '16';
codertargetdata.SPI_A.ClockPolarity = 'Rising_edge';
codertargetdata.SPI_A.ClockPhase = 'No_delay';
codertargetdata.SPI_A.SuspensionMode = 'Free_run';
codertargetdata.SPI_A.EnableLoopback = 0;
codertargetdata.SPI_A.EnableThreeWire = 0;
codertargetdata.SPI_A.TXINTERRUPTENABLE = 0;
codertargetdata.SPI_A.FIFOInterruptLevel_Tx = '0';
codertargetdata.SPI_A.RXINTERRUPTENABLE = 1;
codertargetdata.SPI_A.FIFOInterruptLevel_Rx = '4';
codertargetdata.SPI_A.FIFOEnable = 1;
codertargetdata.SPI_A.FIFOTransmitDelay = '0';
codertargetdata.SPI_A.PinAssignment_SIMO = 'GPIO16';
codertargetdata.SPI_A.PinAssignment_SOMI = 'GPIO17';
codertargetdata.SPI_A.PinAssignment_CLK = 'GPIO18';
codertargetdata.SPI_A.PinAssignment_STE = 'None';
codertargetdata.SPI_A.PinValue_SIMO = '16';
codertargetdata.SPI_A.PinValue_SOMI = '17';
codertargetdata.SPI_A.PinValue_CLK = '18';
codertargetdata.SPI_A.PinValue_STE = -1;
codertargetdata.SPI_A.PinMux_SIMO = '1';
codertargetdata.SPI_A.PinMux_SOMI = '1';
codertargetdata.SPI_A.PinMux_CLK = '1';
codertargetdata.SPI_A.PinMux_STE = '0';
codertargetdata.SPI_A.FIFO_Level = 4;

codertargetdata.SPI_B.Mode = 'Master';
codertargetdata.SPI_B.UserBaudRate = '921600';
codertargetdata.SPI_B.BaudRateFactor = 97;
codertargetdata.SPI_B.BaudRate = 918367;
codertargetdata.SPI_B.DataBits = '16';
codertargetdata.SPI_B.ClockPolarity = 'Rising_edge';
codertargetdata.SPI_B.ClockPhase = 'No_delay';
codertargetdata.SPI_B.SuspensionMode = 'Free_run';
codertargetdata.SPI_B.EnableLoopback = 0;
codertargetdata.SPI_B.EnableThreeWire = 0;
codertargetdata.SPI_B.TXINTERRUPTENABLE = 0;
codertargetdata.SPI_B.FIFOInterruptLevel_Tx = '0';
codertargetdata.SPI_B.RXINTERRUPTENABLE = 0;
codertargetdata.SPI_B.FIFOInterruptLevel_Rx = '4';
codertargetdata.SPI_B.FIFOEnable = 1;
codertargetdata.SPI_B.FIFOTransmitDelay = '0';
codertargetdata.SPI_B.PinAssignment_SIMO = 'None';
codertargetdata.SPI_B.PinAssignment_SOMI = 'None';
codertargetdata.SPI_B.PinAssignment_CLK = 'None';
codertargetdata.SPI_B.PinAssignment_STE = 'None';
codertargetdata.SPI_B.PinValue_SIMO = -1;
codertargetdata.SPI_B.PinValue_SOMI = -1;
codertargetdata.SPI_B.PinValue_CLK = -1;
codertargetdata.SPI_B.PinValue_STE = -1;
codertargetdata.SPI_B.PinMux_SIMO = '0';
codertargetdata.SPI_B.PinMux_SOMI = '0';
codertargetdata.SPI_B.PinMux_CLK = '0';
codertargetdata.SPI_B.PinMux_STE = '0';
codertargetdata.SPI_B.FIFO_Level = 4;

codertargetdata.eQUEP.PinAssignment_eQEP1A = 'GPIO20';
codertargetdata.eQUEP.PinAssignment_eQEP1B = 'GPIO21';
codertargetdata.eQUEP.PinAssignment_eQEP1S = 'None';
codertargetdata.eQUEP.PinAssignment_eQEP1I = 'None';
codertargetdata.eQUEP.PinAssignment_eQEP2A = 'GPIO54';
codertargetdata.eQUEP.PinAssignment_eQEP2B = 'GPIO55';
codertargetdata.eQUEP.PinAssignment_eQEP2S = 'None';
codertargetdata.eQUEP.PinAssignment_eQEP2I = 'None';

codertargetdata.Watchdog.Enable_watchdog = 0;
codertargetdata.Watchdog.Watchdogclock = 'OSCCLK/512/1';
codertargetdata.Watchdog.Time_period = 0.0131;
codertargetdata.Watchdog.Watchdogevent = 'Chip reset';

codertargetdata.GPIO0_7.GPIOQualSel0 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO0_7.GPIOQualSel1 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO0_7.GPIOQualSel2 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO0_7.GPIOQualSel3 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO0_7.GPIOQualSel4 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO0_7.GPIOQualSel5 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO0_7.GPIOQualSel6 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO0_7.GPIOQualSel7 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO0_7.QualPRD = '0';

codertargetdata.GPIO8_15.GPIOQualSel8 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO8_15.GPIOQualSel9 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO8_15.GPIOQualSel10 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO8_15.GPIOQualSel11 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO8_15.GPIOQualSel12 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO8_15.GPIOQualSel13 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO8_15.GPIOQualSel14 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO8_15.GPIOQualSel15 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO8_15.QualPRD = '0';

codertargetdata.GPIO16_23.GPIOQualSel16 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO16_23.GPIOQualSel17 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO16_23.GPIOQualSel18 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO16_23.GPIOQualSel19 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO16_23.GPIOQualSel20 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO16_23.GPIOQualSel21 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO16_23.GPIOQualSel22 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO16_23.GPIOQualSel23 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO16_23.QualPRD = '0';

codertargetdata.GPIO24_31.GPIOQualSel24 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO24_31.GPIOQualSel25 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO24_31.GPIOQualSel26 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO24_31.GPIOQualSel27 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO24_31.GPIOQualSel28 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO24_31.GPIOQualSel29 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO24_31.GPIOQualSel30 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO24_31.GPIOQualSel31 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO24_31.QualPRD = '0';

codertargetdata.GPIO32_39.GPIOQualSel32 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO32_39.GPIOQualSel33 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO32_39.GPIOQualSel34 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO32_39.GPIOQualSel39 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO32_39.QualPRD = '0';

codertargetdata.GPIO40_44.GPIOQualSel40 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO40_44.GPIOQualSel41 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO40_44.GPIOQualSel42 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO40_44.GPIOQualSel43 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO40_44.GPIOQualSel44 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO40_44.QualPRD = '0';

codertargetdata.GPIO50_55.GPIOQualSel50 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO50_55.GPIOQualSel51 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO50_55.GPIOQualSel52 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO50_55.GPIOQualSel53 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO50_55.GPIOQualSel54 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO50_55.GPIOQualSel55 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO50_55.QualPRD = '0';

codertargetdata.GPIO56_58.GPIOQualSel56 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO56_58.GPIOQualSel57 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO56_58.GPIOQualSel58 = 'Synchronize to SYSCLKOUT only';
codertargetdata.GPIO56_58.QualPRD = '0';

codertargetdata.DMA_ch1.EnableDMAChannel = 0;
codertargetdata.DMA_ch1.DataSize = '16 bit';
codertargetdata.DMA_ch1.InterruptSrc = 'None';
codertargetdata.DMA_ch1.ExternalPin = '0';
codertargetdata.DMA_ch1.BurstSize = '1';
codertargetdata.DMA_ch1.TransferSize = '1';
codertargetdata.DMA_ch1.SrcBeginAdd = '0xC000';
codertargetdata.DMA_ch1.DstBeginAdd = '0xD000';
codertargetdata.DMA_ch1.SrcBurstStep = '0';
codertargetdata.DMA_ch1.DstBurstStep = '0';
codertargetdata.DMA_ch1.SrcTransStep = '0';
codertargetdata.DMA_ch1.DstTransStep = '0';
codertargetdata.DMA_ch1.WrapSrcSize = '65536';
codertargetdata.DMA_ch1.WrapDstSize = '65536';
codertargetdata.DMA_ch1.SrcWrapStep = '0';
codertargetdata.DMA_ch1.DstWrapStep = '0';
codertargetdata.DMA_ch1.SetCh1ToHigh = 0;
codertargetdata.DMA_ch1.EnableOneShot = 0;
codertargetdata.DMA_ch1.EnableContinuous = 1;
codertargetdata.DMA_ch1.SyncEnable = 0;
codertargetdata.DMA_ch1.EnableDSTSync = 0;
codertargetdata.DMA_ch1.GenInterrupt = 'Never';
codertargetdata.DMA_ch1.EnableOverFlow = 0;

codertargetdata.DMA_ch2.EnableDMAChannel = 0;
codertargetdata.DMA_ch2.DataSize = '16 bit';
codertargetdata.DMA_ch2.InterruptSrc = 'None';
codertargetdata.DMA_ch2.ExternalPin = '0';
codertargetdata.DMA_ch2.BurstSize = '1';
codertargetdata.DMA_ch2.TransferSize = '1';
codertargetdata.DMA_ch2.SrcBeginAdd = '0xC000';
codertargetdata.DMA_ch2.DstBeginAdd = '0xD000';
codertargetdata.DMA_ch2.SrcBurstStep = '0';
codertargetdata.DMA_ch2.DstBurstStep = '0';
codertargetdata.DMA_ch2.SrcTransStep = '0';
codertargetdata.DMA_ch2.DstTransStep = '0';
codertargetdata.DMA_ch2.WrapSrcSize = '65536';
codertargetdata.DMA_ch2.WrapDstSize = '65536';
codertargetdata.DMA_ch2.SrcWrapStep = '0';
codertargetdata.DMA_ch2.DstWrapStep = '0';
codertargetdata.DMA_ch2.EnableOneShot = 0;
codertargetdata.DMA_ch2.EnableContinuous = 1;
codertargetdata.DMA_ch2.SyncEnable = 0;
codertargetdata.DMA_ch2.EnableDSTSync = 0;
codertargetdata.DMA_ch2.GenInterrupt = 'Never';
codertargetdata.DMA_ch2.EnableOverFlow = 0;

codertargetdata.DMA_ch3.EnableDMAChannel = 0;
codertargetdata.DMA_ch3.DataSize = '16 bit';
codertargetdata.DMA_ch3.InterruptSrc = 'None';
codertargetdata.DMA_ch3.ExternalPin = '0';
codertargetdata.DMA_ch3.BurstSize = '1';
codertargetdata.DMA_ch3.TransferSize = '1';
codertargetdata.DMA_ch3.SrcBeginAdd = '0xC000';
codertargetdata.DMA_ch3.DstBeginAdd = '0xD000';
codertargetdata.DMA_ch3.SrcBurstStep = '0';
codertargetdata.DMA_ch3.DstBurstStep = '0';
codertargetdata.DMA_ch3.SrcTransStep = '0';
codertargetdata.DMA_ch3.DstTransStep = '0';
codertargetdata.DMA_ch3.WrapSrcSize = '65536';
codertargetdata.DMA_ch3.WrapDstSize = '65536';
codertargetdata.DMA_ch3.SrcWrapStep = '0';
codertargetdata.DMA_ch3.DstWrapStep = '0';
codertargetdata.DMA_ch3.EnableOneShot = 0;
codertargetdata.DMA_ch3.EnableContinuous = 1;
codertargetdata.DMA_ch3.SyncEnable = 0;
codertargetdata.DMA_ch3.EnableDSTSync = 0;
codertargetdata.DMA_ch3.GenInterrupt = 'Never';
codertargetdata.DMA_ch3.EnableOverFlow = 0;

codertargetdata.DMA_ch4.EnableDMAChannel = 0;
codertargetdata.DMA_ch4.DataSize = '16 bit';
codertargetdata.DMA_ch4.InterruptSrc = 'None';
codertargetdata.DMA_ch4.ExternalPin = '0';
codertargetdata.DMA_ch4.BurstSize = '1';
codertargetdata.DMA_ch4.TransferSize = '1';
codertargetdata.DMA_ch4.SrcBeginAdd = '0xC000';
codertargetdata.DMA_ch4.DstBeginAdd = '0xD000';
codertargetdata.DMA_ch4.SrcBurstStep = '0';
codertargetdata.DMA_ch4.DstBurstStep = '0';
codertargetdata.DMA_ch4.SrcTransStep = '0';
codertargetdata.DMA_ch4.DstTransStep = '0';
codertargetdata.DMA_ch4.WrapSrcSize = '65536';
codertargetdata.DMA_ch4.WrapDstSize = '65536';
codertargetdata.DMA_ch4.SrcWrapStep = '0';
codertargetdata.DMA_ch4.DstWrapStep = '0';
codertargetdata.DMA_ch4.EnableOneShot = 0;
codertargetdata.DMA_ch4.EnableContinuous = 1;
codertargetdata.DMA_ch4.SyncEnable = 0;
codertargetdata.DMA_ch4.EnableDSTSync = 0;
codertargetdata.DMA_ch4.GenInterrupt = 'Never';
codertargetdata.DMA_ch4.EnableOverFlow = 0;

codertargetdata.DMA_ch5.EnableDMAChannel = 0;
codertargetdata.DMA_ch5.DataSize = '16 bit';
codertargetdata.DMA_ch5.InterruptSrc = 'None';
codertargetdata.DMA_ch5.ExternalPin = '0';
codertargetdata.DMA_ch5.BurstSize = '1';
codertargetdata.DMA_ch5.TransferSize = '1';
codertargetdata.DMA_ch5.SrcBeginAdd = '0xC000';
codertargetdata.DMA_ch5.DstBeginAdd = '0xD000';
codertargetdata.DMA_ch5.SrcBurstStep = '0';
codertargetdata.DMA_ch5.DstBurstStep = '0';
codertargetdata.DMA_ch5.SrcTransStep = '0';
codertargetdata.DMA_ch5.DstTransStep = '0';
codertargetdata.DMA_ch5.WrapSrcSize = '65536';
codertargetdata.DMA_ch5.WrapDstSize = '65536';
codertargetdata.DMA_ch5.SrcWrapStep = '0';
codertargetdata.DMA_ch5.DstWrapStep = '0';
codertargetdata.DMA_ch5.EnableOneShot = 0;
codertargetdata.DMA_ch5.EnableContinuous = 1;
codertargetdata.DMA_ch5.SyncEnable = 0;
codertargetdata.DMA_ch5.EnableDSTSync = 0;
codertargetdata.DMA_ch5.GenInterrupt = 'Never';
codertargetdata.DMA_ch5.EnableOverFlow = 0;

codertargetdata.DMA_ch6.EnableDMAChannel = 0;
codertargetdata.DMA_ch6.DataSize = '16 bit';
codertargetdata.DMA_ch6.InterruptSrc = 'None';
codertargetdata.DMA_ch6.ExternalPin = '0';
codertargetdata.DMA_ch6.BurstSize = '1';
codertargetdata.DMA_ch6.TransferSize = '1';
codertargetdata.DMA_ch6.SrcBeginAdd = '0xC000';
codertargetdata.DMA_ch6.DstBeginAdd = '0xD000';
codertargetdata.DMA_ch6.SrcBurstStep = '0';
codertargetdata.DMA_ch6.DstBurstStep = '0';
codertargetdata.DMA_ch6.SrcTransStep = '0';
codertargetdata.DMA_ch6.DstTransStep = '0';
codertargetdata.DMA_ch6.WrapSrcSize = '65536';
codertargetdata.DMA_ch6.WrapDstSize = '65536';
codertargetdata.DMA_ch6.SrcWrapStep = '0';
codertargetdata.DMA_ch6.DstWrapStep = '0';
codertargetdata.DMA_ch6.EnableOneShot = 0;
codertargetdata.DMA_ch6.EnableContinuous = 1;
codertargetdata.DMA_ch6.SyncEnable = 0;
codertargetdata.DMA_ch6.EnableDSTSync = 0;
codertargetdata.DMA_ch6.GenInterrupt = 'Never';
codertargetdata.DMA_ch6.EnableOverFlow = 0;

codertargetdata.XINT.GPIOXINT1SEL = '0';
codertargetdata.XINT.GPIOXINT2SEL = '0';
codertargetdata.XINT.GPIOXINT3SEL = '0';
codertargetdata.XINT.Polarity1 = 'Falling edge';
codertargetdata.XINT.Polarity2 = 'Falling edge';
codertargetdata.XINT.Polarity3 = 'Falling edge';

set_param(cs, 'CoderTargetData', codertargetdata);

% Model Referencing
cs.set_param('UpdateModelReferenceTargets', 'IfOutOfDateOrStructuralChange');   % Rebuild
cs.set_param('EnableParallelModelReferenceBuilds', 'off');   % Enable parallel model reference builds
cs.set_param('ModelReferenceNumInstancesAllowed', 'Multi');   % Total number of instances allowed per top model
cs.set_param('PropagateVarSize', 'Infer from blocks in model');   % Propagate sizes of variable-size signals
cs.set_param('ModelReferenceMinAlgLoopOccurrences', 'off');   % Minimize algebraic loop occurrences
cs.set_param('EnableRefExpFcnMdlSchedulingChecks', 'on');   % Enable strict scheduling checks for referenced models
cs.set_param('PropagateSignalLabelsOutOfModel', 'off');   % Propagate all signal labels out of the model
cs.set_param('ModelReferencePassRootInputsByReference', 'off');   % Pass fixed-size scalar root inputs by value for code generation
cs.set_param('ModelDependencies', '');   % Model dependencies
cs.set_param('ParallelModelReferenceErrorOnInvalidPool', 'on');   % Perform consistency check on parallel pool
cs.set_param('SupportModelReferenceSimTargetCustomCode', 'off');   % Include custom code for referenced models

% Simulation Target
cs.set_param('MATLABDynamicMemAlloc', 'on');   % Dynamic memory allocation in MATLAB functions
cs.set_param('MATLABDynamicMemAllocThreshold', 65536);   % Dynamic memory allocation threshold in MATLAB functions
cs.set_param('CompileTimeRecursionLimit', 50);   % Compile-time recursion limit for MATLAB functions
cs.set_param('EnableRuntimeRecursion', 'on');   % Enable run-time recursion for MATLAB functions
cs.set_param('SFSimEcho', 'on');   % Echo expressions without semicolons
cs.set_param('SimCtrlC', 'on');   % Ensure responsiveness
cs.set_param('SimIntegrity', 'on');   % Ensure memory integrity
cs.set_param('SimGenImportedTypeDefs', 'off');   % Generate typedefs for imported bus and enumeration types
cs.set_param('SimBuildMode', 'sf_incremental_build');   % Simulation target build mode
cs.set_param('SimReservedNameArray', []);   % Reserved names
cs.set_param('SimParseCustomCode', 'on');   % Import custom code
cs.set_param('SimAnalyzeCustomCode', 'off');   % Enable custom code analysis
cs.set_param('DefaultCustomCodeFunctionArrayLayout', 'NotSpecified');   % Default function array layout
cs.set_param('CustomCodeFunctionArrayLayout', []);   % Specify by function...
cs.set_param('SimCustomSourceCode', '');   % Source file
cs.set_param('SimCustomHeaderCode', ['#include "ReadRegisterScia_TXFFST.h"',newline,...
'#include "ReadRegisterScib_TXFFST.h"']);   % Header file
cs.set_param('SimCustomInitializer', '');   % Initialize function
cs.set_param('SimCustomTerminator', '');   % Terminate function
cs.set_param('SimUserIncludeDirs', '');   % Include directories
cs.set_param('SimUserSources', ['ReadRegisterScia_TXFFST.c',newline,'ReadRegisterScib_TXFFST.c']);   % Source files
cs.set_param('SimUserLibraries', '');   % Libraries
cs.set_param('SimUserDefines', '');   % Defines
cs.set_param('SFSimEnableDebug', 'off');   % Allow setting breakpoints during simulation

% Code Generation
cs.set_param('TargetLang', 'C');   % Language
cs.set_param('Toolchain', 'Texas Instruments Code Composer Studio (C2000)');   % Toolchain
cs.set_param('BuildConfiguration', 'Faster Builds');   % Build configuration
cs.set_param('ObjectivePriorities', []);   % Prioritized objectives
cs.set_param('CheckMdlBeforeBuild', 'Off');   % Check model before generating code
cs.set_param('GenCodeOnly', 'off');   % Generate code only
cs.set_param('PackageGeneratedCodeAndArtifacts', 'off');   % Package code and artifacts
cs.set_param('RTWVerbose', 'on');   % Verbose build
cs.set_param('RetainRTWFile', 'off');   % Retain .rtw file
cs.set_param('ProfileTLC', 'off');   % Profile TLC
cs.set_param('TLCDebug', 'off');   % Start TLC debugger when generating code
cs.set_param('TLCCoverage', 'off');   % Start TLC coverage when generating code
cs.set_param('TLCAssert', 'off');   % Enable TLC assertion
cs.set_param('RTWUseSimCustomCode', 'on');   % Use the same custom code settings as Simulation Target
% cs.set_param('CustomSourceCode', '');   % Source file
% cs.set_param('CustomHeaderCode', ['#include "ReadRegisterScia_TXFFST.h"',newline,...
% '#include "ReadRegisterScib_TXFFST.h"']);   % Header file
% cs.set_param('CustomInclude', '');   % Include directories
% cs.set_param('CustomSource', ['ReadRegisterScia_TXFFST.c',newline,'ReadRegisterScib_TXFFST.c']);   % Source files
% cs.set_param('CustomLibrary', '');   % Libraries
cs.set_param('CustomLAPACKCallback', '');   % Custom LAPACK library callback
cs.set_param('CustomFFTCallback', '');   % Custom FFT library callback
cs.set_param('CustomBLASCallback', '');   % Custom BLAS library callback
% cs.set_param('CustomDefine', '');   % Defines
% cs.set_param('CustomInitializer', '');   % Initialize function
% cs.set_param('CustomTerminator', '');   % Terminate function
cs.set_param('PostCodeGenCommand', 'codertarget.postCodeGenHookCommand(h)');   % Post code generation command
cs.set_param('TLCOptions', '');   % TLC command line options
cs.set_param('GenerateReport', 'on');   % Create code generation report
cs.set_param('GenerateComments', 'on');   % Include comments
cs.set_param('SimulinkBlockComments', 'on');   % Simulink block comments
cs.set_param('StateflowObjectComments', 'on');   % Stateflow object comments
cs.set_param('MATLABSourceComments', 'on');   % MATLAB source code as comments
cs.set_param('ShowEliminatedStatement', 'on');   % Show eliminated blocks
cs.set_param('ForceParamTrailComments', 'on');   % Verbose comments for 'Model default' storage class
cs.set_param('MaxIdLength', 31);   % Maximum identifier length
cs.set_param('UseSimReservedNames', 'off');   % Use the same reserved names as Simulation Target
cs.set_param('ReservedNameArray', []);   % Reserved names
cs.set_param('TargetLangStandard', 'C89/C90 (ANSI)');   % Standard math library
cs.set_param('CodeReplacementLibrary', 'TI C28x');   % Code replacement library
cs.set_param('UtilityFuncGeneration', 'Auto');   % Shared code placement
cs.set_param('CodeInterfacePackaging', 'Nonreusable function');   % Code interface packaging
cs.set_param('GRTInterface', 'off');   % Classic call interface
cs.set_param('SupportNonFinite', 'on');   % Support non-finite numbers
cs.set_param('MultiwordLength', 2048);   % Maximum word length
cs.set_param('CombineOutputUpdateFcns', 'on');   % Single output/update function
cs.set_param('MatFileLogging', 'off');   % MAT-file logging
cs.set_param('LogVarNameModifier', 'rt_');   % MAT-file variable name modifier
cs.set_param('ArrayLayout', 'Column-major');   % Array layout
cs.set_param('GenerateFullHeader', 'on');   % Generate full file banner
cs.set_param('InferredTypesCompatibility', 'off');   % Create preprocessor directive in rtwtypes.h
cs.set_param('TargetLibSuffix', '');   % Suffix applied to target library name
cs.set_param('TargetPreCompLibLocation', '');   % Precompiled library location
cs.set_param('LUTObjectStructOrderExplicitValues', 'Size,Breakpoints,Table');   % LUT object struct order for explicit value specification
cs.set_param('LUTObjectStructOrderEvenSpacing', 'Size,Breakpoints,Table');   % LUT object struct order for even spacing specification
cs.set_param('DynamicStringBufferSize', 256);   % Buffer size of dynamically-sized string (bytes)
cs.set_param('ExtMode', 'off');   % External mode
cs.set_param('RTWCAPIParams', 'off');   % Generate C API for parameters
cs.set_param('RTWCAPIRootIO', 'off');   % Generate C API for root-level I/O
cs.set_param('RTWCAPISignals', 'off');   % Generate C API for signals
cs.set_param('RTWCAPIStates', 'off');   % Generate C API for states
cs.set_param('GenerateASAP2', 'off');   % ASAP2 interface

% Simulink Coverage
cs.set_param('CovModelRefEnable', 'off');   % Record coverage for referenced models
cs.set_param('RecordCoverage', 'off');   % Record coverage for this model
cs.set_param('CovEnable', 'off');   % Enable coverage analysis