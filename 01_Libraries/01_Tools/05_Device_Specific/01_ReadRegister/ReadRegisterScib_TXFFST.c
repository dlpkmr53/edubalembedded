#include "ReadRegisterScib_TXFFST.h"
void ReadRegisterScib_TXFFST(unsigned int *ReturnValue)
{
#ifdef _WIN32
    *ReturnValue = 0;
    // as this funcion is used to call a processor-specific register, it cannot work on a Windows (or any other) PC
#else
    *ReturnValue = ScibRegs.SCIFFTX.bit.TXFFST;
#endif
}
