#ifdef _WIN32
// as this funcion is used to call a processor-specific register, it cannot work on a Windows (or any other) PC
#else
#include "F2806x_Device.h"
#endif

void ReadRegisterScia_TXFFST(unsigned int *ReturnValue);
