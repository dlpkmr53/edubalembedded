function InitRingBuffer
%Block Callback-Function, only makes sense when used as such...


%Find Parent Model Workspace
parentmodel = gcb;%get(gcbh,'parent');
while ~isempty(get_param(parentmodel,'parent'));
parentmodel = get_param(parentmodel,'parent');
end
hws = get_param(parentmodel,'modelworkspace');   


VariableDataType = get_param(gcb,'VariableDataType');
IndexDataType = get_param(gcb,'IndexDataType');
VariableLength = get_param(gcb,'VariableLength');
VariableName = get_param(gcb,'VariableName');
disp(['Initializing Ring Buffer: ' VariableName]);


Sig = Simulink.Signal;                      
set(Sig,'Complexity','real',...             
           'SamplingMode','Sample Based',...
           'DataType',VariableDataType,...          
           'InitialValue',['zeros(' num2str(VariableLength) ',1)']);  
hws.assignin([VariableName '_data'],Sig);   
Sig = Simulink.Signal;                      
set(Sig,'Complexity','real',...             
           'SamplingMode','Sample Based',...
           'DataType',IndexDataType,...          
           'InitialValue','0');             
hws.assignin([VariableName '_idx'],Sig);  
