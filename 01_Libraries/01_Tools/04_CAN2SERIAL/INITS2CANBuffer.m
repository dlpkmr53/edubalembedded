function INITS2CANBuffer

%Block Callback-Function, only makes sense when used as such...

disp('Initializing INITS2CAN Buffer');
%Find Parent Model Workspace
parentmodel = gcb;%get(gcbh,'parent');
while ~isempty(get_param(parentmodel,'parent'));
parentmodel = get_param(parentmodel,'parent');
end
hws = get_param(parentmodel,'modelworkspace');   


S2CANBufferLength = get_param(gcb,'S2CANBufferLength');

Sig = Simulink.Signal;                      
set(Sig,'Complexity','real',...             
           'SamplingMode','Sample Based',...
           'DataType','uint16',...          
           'InitialValue',['zeros(' num2str(S2CANBufferLength) ',1)']);  
hws.assignin('S2CANbuffer_DATALENGTH',Sig); 

hws = get_param(bdroot,'modelworkspace');                                             
Sig = Simulink.Signal;                      
set(Sig,'Complexity','real',...             
           'SamplingMode','Sample Based',...
           'DataType','uint8',...           
           'InitialValue',['zeros(' num2str(S2CANBufferLength) ',8)']);  
hws.assignin('S2CANbuffer_Bytes',Sig);      
                                            
hws = get_param(bdroot,'modelworkspace');   
Sig = Simulink.Signal;                      
set(Sig,'Complexity','real',...             
           'SamplingMode','Sample Based',...
           'DataType','uint16',...          
           'InitialValue',['zeros(' num2str(S2CANBufferLength) ',1)']);  
hws.assignin('S2CANbuffer_ID',Sig);
                                            
hws = get_param(bdroot,'modelworkspace');   
Sig = Simulink.Signal;                      
set(Sig,'Complexity','real',...             
           'SamplingMode','Sample Based',...
           'DataType','uint16',...          
           'InitialValue','0');             
hws.assignin('S2CANbuffer_INDEX',Sig);      

hws = get_param(bdroot,'modelworkspace');   
Sig = Simulink.Signal;                      
set(Sig,'Complexity','real',...             
           'SamplingMode','Sample Based',...
           'DataType','uint8',...          
           'InitialValue',['zeros(' num2str(S2CANBufferLength) ',1)']);             
hws.assignin('S2CANbuffer_Channel',Sig);   



