function [Body, Wheel] = createMS()
% Creates the geometry for one Mini Segway (Body part)
  Body = createMSBody();
  Wheel = createMSWheel();
end

function p = createMSBody()
% Creates the geometry for one Mini Segway (Body part)
  p = hgtransform;
  % Draw main body part
  body = rectangle('Parent',p);
  body.Position = [-0.0125, 0.08, 0.025, 0.14];
  body.FaceColor = 'black';
  body.EdgeColor = 'black';
  body.LineWidth = 0.5;
  % Draw head part
  head = rectangle('Parent',p,'Curvature', [0.1 0.1]);
  head.Position = [-0.069, 0.22, 0.099,0.015];
  head.FaceColor = 'black';
  head.EdgeColor = 'white';
  head.LineWidth = 0.5;
  % Draw battery holder
  batt = rectangle('Parent',p,'Curvature', [0.1 0.1]);
  batt.Position = [-0.069, 0.095, 0.049, 0.125];
  batt.FaceColor = 'black';
  batt.EdgeColor = 'white';
  batt.LineWidth = 0.5;
  % Draw bar
  bar = rectangle('Parent',p,'Curvature', [0.1 0.1]);
  bar.Position = [-0.049, 0.09, 0.049, 0.005];
  bar.FaceColor = 'black';
  bar.EdgeColor = 'white';
  bar.LineWidth = 0.5; 
end

function p = createMSWheel() 
% Creates the geometry for one Mini Segway (Wheel part)
  p = hgtransform;
  % Draw the outer part of the wheel
  wheel = rectangle('Parent',p,'Curvature',[1 1]);
  wheel.Position = [-0.04, 0, 0.08, 0.08];
  wheel.EdgeColor = 'black';
  wheel.FaceColor = 'none';
  wheel.LineWidth = 4;
  % Draw first wheel bar
  wheelb1 = rectangle('Parent',p,'Curvature',[0.2 0.2]);
  wheelb1.Position = [-0.0025, 0.04, 0.005, 0.04];
  wheelb1.EdgeColor = 'black';
  wheelb1.FaceColor = 'black';
  wheelb1.LineWidth = 1;
  % Draw second wheel bar
  wheelb2 = rectangle('Parent',p,'Curvature',[0.2 0.2]);
  wheelb2.Position = [0, 0.0375, 0.04, 0.005];
  wheelb2.EdgeColor = 'black';
  wheelb2.FaceColor = 'black';
  wheelb2.LineWidth = 1;
  % Draw third wheel bar
  wheelb3 = rectangle('Parent',p,'Curvature',[0.2 0.2]);
  wheelb3.Position = [-0.0025, 0, 0.005, 0.04];
  wheelb3.EdgeColor = 'black';
  wheelb3.FaceColor = 'black';
  wheelb3.LineWidth = 1;
  % Draw fourth wheel bar
  wheelb4 = rectangle('Parent',p,'Curvature',[0.2 0.2]);
  wheelb4.Position = [-0.04, 0.0375, 0.04, 0.005];
  wheelb4.EdgeColor = 'black';
  wheelb4.FaceColor = 'black';
  wheelb4.LineWidth = 1;
  % Draw the inner part of the wheel
  wheelc = rectangle('Parent',p,'Curvature',[1 1]);
  wheelc.Position = [-0.005, 0.04 - 0.005, 0.01, 0.01];
  wheelc.EdgeColor = 'white';
  wheelc.FaceColor = 'black';
  wheelc.LineWidth = 1;
end