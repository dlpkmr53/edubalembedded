function CRC16_Table = CalculateTable_CRC16(GenPoly)
% This function generates the CRC16 Table for all byte values 0-255
% Inputs:
% GenPoly - Generator polynomial for CRC [uint16]
% Outputs: 
% CRC16_Table - Table in the form CRC16_Table(divident + 1)

% Initialize the table elements with 0.
CRCtable = uint16(zeros(256,1));    
% Iterate over all byte values 0-255
for kk = uint16(1:length(CRCtable))
    % Move the current byte into MSB of the 16 bit CRC 
    curByte = uint16(bitshift((kk-1),8));
    % Calculate the CRC16 value for the current byte
    for jj = 1:8
        % check if most significant bit is 1
        if (bitand(curByte,uint16(hex2dec('8000'))) ~= 0)
            curByte = bitshift(curByte,1);     % Discard the bit
            curByte = bitxor(curByte,GenPoly); % XOR with GenPoly
        else
            curByte = bitshift(curByte,1); % Shift bits until most 
                                           % significant bit is 1
        end
    end
    % Store the calculated CRC16 value in the corresponding table entry
    CRCtable(kk,1) = curByte;
end
% Output table values
CRC16_Table = CRCtable(:);

% Note: the Table entry values are offset by one, for example: the table
% entry for the byte with value 4 is CRC16_Table(4+1). 
