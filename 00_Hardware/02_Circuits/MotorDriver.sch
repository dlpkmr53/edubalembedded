<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.4.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="16" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="130bmp" color="2" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="2" fill="9" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="motor_driver">
<packages>
<package name="HTSSOP-28">
<description>&lt;b&gt;HTSSOP-28 Package&lt;/b&gt;</description>
<smd name="15" x="4.225" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="29" x="0" y="0" dx="9.7" dy="3.4" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="16" x="3.575" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="17" x="2.925" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="18" x="2.275" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="19" x="1.625" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="20" x="0.975" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="21" x="0.325" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="22" x="-0.325" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="23" x="-0.975" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="24" x="-1.625" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="25" x="-2.275" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="26" x="-2.925" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="27" x="-3.575" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="28" x="-4.225" y="2.8" dx="1.6" dy="0.3" layer="1" rot="R90" stop="no"/>
<smd name="14" x="4.225" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no"/>
<smd name="13" x="3.575" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="12" x="2.925" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="11" x="2.275" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="10" x="1.625" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="9" x="0.975" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="8" x="0.325" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="7" x="-0.325" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="6" x="-0.975" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="5" x="-1.625" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="4" x="-2.275" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="3" x="-2.925" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="2" x="-3.575" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="1" x="-4.225" y="-2.8" dx="1.6" dy="0.3" layer="1" rot="R270" stop="no" thermals="no"/>
<wire x1="-4.85" y1="-2.2" x2="-4.85" y2="2.2" width="0.127" layer="21"/>
<wire x1="4.85" y1="2.2" x2="4.85" y2="-2.2" width="0.127" layer="21"/>
<circle x="-4.45" y="-1.8" radius="0.15" width="0.127" layer="21"/>
<wire x1="-4.85" y1="2.2" x2="4.85" y2="2.2" width="0.05" layer="51"/>
<wire x1="4.85" y1="2.2" x2="4.85" y2="-2.2" width="0.05" layer="51"/>
<wire x1="4.85" y1="-2.2" x2="-4.85" y2="-2.2" width="0.05" layer="51"/>
<wire x1="-4.85" y1="-2.2" x2="-4.85" y2="2.2" width="0.05" layer="51"/>
<rectangle x1="-4.375" y1="2.2" x2="-4.075" y2="3.2" layer="51"/>
<rectangle x1="-3.725" y1="2.2" x2="-3.425" y2="3.2" layer="51"/>
<rectangle x1="-3.075" y1="2.2" x2="-2.775" y2="3.2" layer="51"/>
<rectangle x1="-2.425" y1="2.2" x2="-2.125" y2="3.2" layer="51"/>
<rectangle x1="-1.775" y1="2.2" x2="-1.475" y2="3.2" layer="51"/>
<rectangle x1="-1.125" y1="2.2" x2="-0.825" y2="3.2" layer="51"/>
<rectangle x1="-0.475" y1="2.2" x2="-0.175" y2="3.2" layer="51"/>
<rectangle x1="0.175" y1="2.2" x2="0.475" y2="3.2" layer="51"/>
<rectangle x1="0.825" y1="2.2" x2="1.125" y2="3.2" layer="51"/>
<rectangle x1="1.475" y1="2.2" x2="1.775" y2="3.2" layer="51"/>
<rectangle x1="2.125" y1="2.2" x2="2.425" y2="3.2" layer="51"/>
<rectangle x1="2.775" y1="2.2" x2="3.075" y2="3.2" layer="51"/>
<rectangle x1="3.425" y1="2.2" x2="3.725" y2="3.2" layer="51"/>
<rectangle x1="4.075" y1="2.2" x2="4.375" y2="3.2" layer="51"/>
<rectangle x1="4.075" y1="-3.2" x2="4.375" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="3.425" y1="-3.2" x2="3.725" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="2.775" y1="-3.2" x2="3.075" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="2.125" y1="-3.2" x2="2.425" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="1.475" y1="-3.2" x2="1.775" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="0.825" y1="-3.2" x2="1.125" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="0.175" y1="-3.2" x2="0.475" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="-0.475" y1="-3.2" x2="-0.175" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="-1.125" y1="-3.2" x2="-0.825" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="-1.775" y1="-3.2" x2="-1.475" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="-2.425" y1="-3.2" x2="-2.125" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="-3.075" y1="-3.2" x2="-2.775" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="-3.725" y1="-3.2" x2="-3.425" y2="-2.2" layer="51" rot="R180"/>
<rectangle x1="-4.375" y1="-3.2" x2="-4.075" y2="-2.2" layer="51" rot="R180"/>
<circle x="-4.45" y="-1.8" radius="0.15" width="0.05" layer="51"/>
<text x="-5.2" y="-2.6" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.5" y="-2.6" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.59" y1="-1.55" x2="2.59" y2="1.55" layer="29"/>
<rectangle x1="-2.59" y1="-1.55" x2="2.59" y2="1.55" layer="31"/>
<rectangle x1="-4.445" y1="1.93" x2="-4.005" y2="3.67" layer="29"/>
<rectangle x1="-3.795" y1="1.93" x2="-3.355" y2="3.67" layer="29"/>
<rectangle x1="-3.145" y1="1.93" x2="-2.705" y2="3.67" layer="29"/>
<rectangle x1="-2.495" y1="1.93" x2="-2.055" y2="3.67" layer="29"/>
<rectangle x1="-1.845" y1="1.93" x2="-1.405" y2="3.67" layer="29"/>
<rectangle x1="-1.195" y1="1.93" x2="-0.755" y2="3.67" layer="29"/>
<rectangle x1="-0.545" y1="1.93" x2="-0.105" y2="3.67" layer="29"/>
<rectangle x1="0.105" y1="1.93" x2="0.545" y2="3.67" layer="29"/>
<rectangle x1="0.755" y1="1.93" x2="1.195" y2="3.67" layer="29"/>
<rectangle x1="1.405" y1="1.93" x2="1.845" y2="3.67" layer="29"/>
<rectangle x1="2.055" y1="1.93" x2="2.495" y2="3.67" layer="29"/>
<rectangle x1="2.705" y1="1.93" x2="3.145" y2="3.67" layer="29"/>
<rectangle x1="3.355" y1="1.93" x2="3.795" y2="3.67" layer="29"/>
<rectangle x1="4.005" y1="1.93" x2="4.445" y2="3.67" layer="29"/>
<rectangle x1="4.005" y1="-3.67" x2="4.445" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="3.355" y1="-3.67" x2="3.795" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="2.705" y1="-3.67" x2="3.145" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="2.055" y1="-3.67" x2="2.495" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="1.405" y1="-3.67" x2="1.845" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="0.755" y1="-3.67" x2="1.195" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="0.105" y1="-3.67" x2="0.545" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="-0.545" y1="-3.67" x2="-0.105" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="-1.195" y1="-3.67" x2="-0.755" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="-1.845" y1="-3.67" x2="-1.405" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="-2.495" y1="-3.67" x2="-2.055" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="-3.145" y1="-3.67" x2="-2.705" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="-3.795" y1="-3.67" x2="-3.355" y2="-1.93" layer="29" rot="R180"/>
<rectangle x1="-4.445" y1="-3.67" x2="-4.005" y2="-1.93" layer="29" rot="R180"/>
<pad name="P$1" x="-3.9" y="-1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$2" x="-2.6" y="-1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$3" x="-1.3" y="-1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$4" x="0" y="-1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$5" x="1.3" y="-1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$6" x="2.6" y="-1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$7" x="3.9" y="-1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$8" x="-3.9" y="0" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$9" x="-2.6" y="0" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$10" x="-1.3" y="0" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$11" x="0" y="0" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$12" x="1.3" y="0" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$13" x="2.6" y="0" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$14" x="3.9" y="0" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$15" x="-3.9" y="1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$16" x="-2.6" y="1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$17" x="-1.3" y="1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$18" x="0" y="1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$19" x="1.3" y="1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$20" x="2.6" y="1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<pad name="P$21" x="3.9" y="1.3" drill="0.3" diameter="0.4" stop="no" thermals="no"/>
<wire x1="-5" y1="-3.8" x2="-5" y2="3.8" width="0.127" layer="39"/>
<wire x1="-5" y1="3.8" x2="5" y2="3.8" width="0.127" layer="39"/>
<wire x1="5" y1="3.8" x2="5" y2="-3.8" width="0.127" layer="39"/>
<wire x1="5" y1="-3.8" x2="-5" y2="-3.8" width="0.127" layer="39"/>
<wire x1="-4.85" y1="-2.2" x2="-4.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="4.85" y1="-2.2" x2="4.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="4.85" y1="2.2" x2="4.5" y2="2.2" width="0.127" layer="21"/>
<wire x1="-4.85" y1="2.2" x2="-4.5" y2="2.2" width="0.127" layer="21"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.173" y1="0.653" x2="1.173" y2="0.653" width="0.0508" layer="39"/>
<wire x1="1.173" y1="0.653" x2="1.173" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="1.173" y1="-0.583" x2="-1.173" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="-1.173" y1="-0.583" x2="-1.173" y2="0.653" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.1" y="0.25"/>
<vertex x="0.1" y="0.25"/>
<vertex x="0.1" y="-0.25"/>
<vertex x="-0.1" y="-0.25"/>
</polygon>
</package>
<package name="153CLV-0605">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="2.4" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="1.1" x2="-3.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="2.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.4" x2="3.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.1" x2="3.3" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.4" x2="2.4" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="2.4" y1="3.3" x2="3.3" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-0.9" x2="2.95" y2="-0.95" width="0.2032" layer="21" curve="145.181395"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.9" width="0.2032" layer="21" curve="-145.181395"/>
<smd name="+" x="2.7" y="0" dx="3.5" dy="1.6" layer="1"/>
<smd name="-" x="-2.7" y="0" dx="3.5" dy="1.6" layer="1"/>
<text x="-3.39" y="3.63" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.425" y="-4.885" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-4.6" y1="-3.5" x2="-4.6" y2="3.5" width="0.127" layer="39"/>
<wire x1="-4.6" y1="3.5" x2="4.6" y2="3.5" width="0.127" layer="39"/>
<wire x1="4.6" y1="3.5" x2="4.6" y2="-3.5" width="0.127" layer="39"/>
<wire x1="4.6" y1="-3.5" x2="-4.6" y2="-3.5" width="0.127" layer="39"/>
<circle x="0" y="0" radius="3.0886875" width="0.127" layer="51"/>
<wire x1="-3.3" y1="3.3" x2="2.4" y2="3.3" width="0.127" layer="51"/>
<wire x1="2.4" y1="3.3" x2="3.3" y2="2.4" width="0.127" layer="51"/>
<wire x1="3.3" y1="2.4" x2="3.3" y2="-2.4" width="0.127" layer="51"/>
<wire x1="3.3" y1="-2.4" x2="2.4" y2="-3.3" width="0.127" layer="51"/>
<wire x1="2.4" y1="-3.3" x2="-3.3" y2="-3.3" width="0.127" layer="51"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="3.3" width="0.127" layer="51"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.4" y="0.8"/>
<vertex x="0.4" y="0.8"/>
<vertex x="0.4" y="-0.8"/>
<vertex x="-0.4" y="-0.8"/>
</polygon>
</package>
<package name="R6432">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.71" x2="2.387" y2="1.71" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.71" x2="2.387" y2="-1.71" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.325" y="0" dx="1" dy="3.65" layer="1"/>
<smd name="2" x="3.325" y="0" dx="1" dy="3.65" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.325" y1="-1.775" x2="-2.275" y2="1.775" layer="51"/>
<rectangle x1="2.275" y1="-1.775" x2="3.325" y2="1.775" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<wire x1="-2.6" y1="1.7" x2="2.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="2.6" y1="1.7" x2="2.6" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2.6" y1="-1.7" x2="-2.6" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-1.7" x2="-2.6" y2="1.7" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-2.6" y="1.7"/>
<vertex x="-2.3" y="1.7"/>
<vertex x="-2.3" y="-1.7"/>
<vertex x="-2.6" y="-1.7"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="2.3" y="1.7"/>
<vertex x="2.6" y="1.7"/>
<vertex x="2.6" y="-1.7"/>
<vertex x="2.3" y="-1.7"/>
</polygon>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<pad name="1" x="-1.75" y="0" drill="1.2" shape="long" rot="R90"/>
<pad name="2" x="1.75" y="0" drill="1.2" shape="long" rot="R90"/>
<text x="-4.0862" y="3.7688" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.08" y="-4.975" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.004" y1="-0.254" x2="-1.496" y2="0.254" layer="51"/>
<rectangle x1="1.496" y1="-0.254" x2="2.004" y2="0.254" layer="51"/>
<wire x1="-4.05" y1="-3.5" x2="-4.05" y2="3.5" width="0.127" layer="21"/>
<wire x1="-4.05" y1="3.5" x2="4.05" y2="3.5" width="0.127" layer="21"/>
<wire x1="4.05" y1="3.5" x2="4.05" y2="-3.5" width="0.127" layer="21"/>
<wire x1="4.05" y1="-3.5" x2="-4.05" y2="-3.5" width="0.127" layer="21"/>
</package>
<package name="1X02/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
<wire x1="0" y1="0.65" x2="0" y2="-0.65" width="0.127" layer="21"/>
</package>
<package name="4X10-BOOSTERPACK-BOTTOM">
<wire x1="1.27" y1="-5.715" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-4.445" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-5.715" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="6.35" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="5.715" x2="-0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="0.635" y1="8.89" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="6.35" x2="-1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="8.255" x2="-0.635" y2="8.89" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-6.985" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-8.255" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-9.525" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-10.795" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-13.97" x2="-0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-13.97" x2="-1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-12.065" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-13.335" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="41" x="0" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="42" x="0" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="43" x="0" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="44" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="45" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="46" x="0" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="47" x="0" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="48" x="0" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="49" x="0" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="50" x="0" y="-12.7" drill="1.016" diameter="1.8796"/>
<text x="4.7498" y="11.4808" size="1.27" layer="25" font="vector" ratio="10">J5</text>
<rectangle x1="-0.254" y1="-5.334" x2="0.254" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-2.794" x2="0.254" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="4.826" x2="0.254" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="7.366" x2="0.254" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="9.906" x2="0.254" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-7.874" x2="0.254" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-10.414" x2="0.254" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-12.954" x2="0.254" y2="-12.446" layer="51" rot="R270"/>
<wire x1="-0.635" y1="8.89" x2="-1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="11.43" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="0.635" y2="8.89" width="0.2286" layer="21"/>
<text x="7.7978" y="11.4808" size="1.27" layer="25" font="vector" ratio="10">J7</text>
<text x="37.7698" y="11.4808" size="1.27" layer="25" font="vector" ratio="10" align="bottom-right">J8</text>
<text x="40.8178" y="11.4808" size="1.27" layer="25" font="vector" ratio="10" align="bottom-right">J6</text>
<wire x1="3.81" y1="-4.445" x2="3.81" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-5.715" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-4.445" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-5.715" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="6.35" x2="3.81" y2="5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="5.715" x2="3.81" y2="4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="4.445" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="5.715" x2="1.905" y2="6.35" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.175" y1="8.89" x2="3.81" y2="8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="8.255" x2="3.81" y2="6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.175" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="6.35" x2="1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="8.255" x2="1.905" y2="8.89" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.81" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-8.255" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.81" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-10.795" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.81" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-13.335" x2="3.175" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-13.97" x2="1.905" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-13.97" x2="1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="61" x="2.54" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="62" x="2.54" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="63" x="2.54" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="64" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="65" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="66" x="2.54" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="67" x="2.54" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="68" x="2.54" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="69" x="2.54" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="70" x="2.54" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="2.286" y1="-5.334" x2="2.794" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-2.794" x2="2.794" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="4.826" x2="2.794" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="7.366" x2="2.794" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="9.906" x2="2.794" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-7.874" x2="2.794" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-10.414" x2="2.794" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-12.954" x2="2.794" y2="-12.446" layer="51" rot="R270"/>
<wire x1="1.905" y1="8.89" x2="1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="11.43" x2="3.81" y2="11.43" width="0.2286" layer="21"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="8.89" width="0.2286" layer="21"/>
<wire x1="3.81" y1="8.89" x2="3.175" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-1.27" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-3.175" x2="41.91" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-1.905" x2="42.545" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-4.445" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-5.715" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.2032" layer="21"/>
<wire x1="43.815" y1="1.27" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="43.815" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-1.27" x2="41.91" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-0.635" x2="41.91" y2="0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="0.635" x2="42.545" y2="1.27" width="0.2032" layer="21"/>
<wire x1="43.815" y1="6.35" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="4.445" x2="41.91" y2="5.715" width="0.2032" layer="21"/>
<wire x1="41.91" y1="5.715" x2="42.545" y2="6.35" width="0.2032" layer="21"/>
<wire x1="44.45" y1="3.175" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="43.815" y1="8.89" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="43.815" y2="6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="6.35" x2="41.91" y2="6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="6.985" x2="41.91" y2="8.255" width="0.2032" layer="21"/>
<wire x1="41.91" y1="8.255" x2="42.545" y2="8.89" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-6.985" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-8.255" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-9.525" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-10.795" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="43.815" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-13.97" x2="42.545" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-13.97" x2="41.91" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-12.065" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-13.335" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<pad name="80" x="43.18" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="79" x="43.18" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="78" x="43.18" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="77" x="43.18" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="76" x="43.18" y="0" drill="1.016" diameter="1.8796"/>
<pad name="75" x="43.18" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="74" x="43.18" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="73" x="43.18" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="72" x="43.18" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="71" x="43.18" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="42.926" y1="-5.334" x2="43.434" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-2.794" x2="43.434" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-0.254" x2="43.434" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="2.286" x2="43.434" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="4.826" x2="43.434" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="7.366" x2="43.434" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="9.906" x2="43.434" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-7.874" x2="43.434" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-10.414" x2="43.434" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-12.954" x2="43.434" y2="-12.446" layer="51" rot="R270"/>
<wire x1="42.545" y1="8.89" x2="41.91" y2="8.89" width="0.2286" layer="21"/>
<wire x1="41.91" y1="8.89" x2="41.91" y2="11.43" width="0.2286" layer="21"/>
<wire x1="41.91" y1="11.43" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="43.815" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.99" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-5.715" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-1.27" x2="46.99" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-1.905" x2="46.99" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-3.175" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-1.905" x2="45.085" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="1.27" x2="44.45" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.355" y1="1.27" x2="46.99" y2="0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="0.635" x2="46.99" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-0.635" x2="46.355" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-1.27" x2="44.45" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="0.635" x2="45.085" y2="1.27" width="0.2032" layer="21"/>
<wire x1="46.355" y1="6.35" x2="46.99" y2="5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="5.715" x2="46.99" y2="4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="4.445" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="5.715" x2="45.085" y2="6.35" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="46.355" y1="8.89" x2="46.99" y2="8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="8.255" x2="46.99" y2="6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="6.985" x2="46.355" y2="6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="6.35" x2="44.45" y2="6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="8.255" x2="45.085" y2="8.89" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.99" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-8.255" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.99" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-10.795" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.99" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-13.335" x2="46.355" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-13.97" x2="45.085" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-13.97" x2="44.45" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<pad name="60" x="45.72" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="59" x="45.72" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="58" x="45.72" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="57" x="45.72" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="56" x="45.72" y="0" drill="1.016" diameter="1.8796"/>
<pad name="55" x="45.72" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="54" x="45.72" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="53" x="45.72" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="52" x="45.72" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="51" x="45.72" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="45.466" y1="-5.334" x2="45.974" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-2.794" x2="45.974" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-0.254" x2="45.974" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="2.286" x2="45.974" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="4.826" x2="45.974" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="7.366" x2="45.974" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="9.906" x2="45.974" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-7.874" x2="45.974" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-10.414" x2="45.974" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-12.954" x2="45.974" y2="-12.446" layer="51" rot="R270"/>
<wire x1="45.085" y1="8.89" x2="44.45" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="11.43" x2="46.99" y2="11.43" width="0.2286" layer="21"/>
<wire x1="46.99" y1="11.43" x2="46.99" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="8.89" x2="46.355" y2="8.89" width="0.2286" layer="21"/>
<text x="7.62" y="9.652" size="0.889" layer="21" font="vector">5V</text>
<text x="7.62" y="7.112" size="0.889" layer="21" font="vector">GND</text>
<text x="7.62" y="4.572" size="0.889" layer="21" font="vector">AB7</text>
<text x="7.62" y="-10.668" size="0.889" layer="21" font="vector">AA4</text>
<text x="-0.254" y="11.938" size="0.889" layer="21" font="vector">41</text>
<text x="3.556" y="11.938" size="0.889" layer="21" font="vector" align="bottom-right">61</text>
<text x="-0.762" y="-15.24" size="0.889" layer="21" font="vector">50</text>
<text x="3.556" y="-15.24" size="0.889" layer="21" font="vector" align="bottom-right">70</text>
<text x="7.62" y="2.032" size="0.889" layer="21" font="vector">AB4</text>
<text x="7.62" y="-0.508" size="0.889" layer="21" font="vector">AA5</text>
<text x="7.62" y="-3.048" size="0.889" layer="21" font="vector">AB5</text>
<text x="7.62" y="-5.588" size="0.889" layer="21" font="vector">AA3</text>
<text x="7.62" y="-8.128" size="0.889" layer="21" font="vector">AB3</text>
<text x="42.418" y="11.938" size="0.889" layer="21" font="vector">80</text>
<text x="46.482" y="11.938" size="0.889" layer="21" font="vector" align="bottom-right">60</text>
<text x="42.672" y="-15.24" size="0.889" layer="21" font="vector">71</text>
<text x="46.736" y="-15.24" size="0.889" layer="21" font="vector" align="bottom-right">51</text>
<text x="37.338" y="7.112" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="4.572" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="2.032" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-0.508" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-3.048" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-10.668" size="0.889" layer="21" font="vector" align="bottom-right">DAC</text>
<text x="37.338" y="-13.208" size="0.889" layer="21" font="vector" align="bottom-right">DAC</text>
<text x="41.148" y="9.652" size="0.889" layer="21" font="vector" align="bottom-right">GND</text>
<text x="41.148" y="7.112" size="0.889" layer="21" font="vector" align="bottom-right">P27</text>
<text x="41.148" y="4.572" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-10.668" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-0.508" size="0.889" layer="21" font="vector" align="bottom-right">RST</text>
<text x="41.148" y="-3.048" size="0.889" layer="21" font="vector" align="bottom-right">MOSI</text>
<text x="41.148" y="-5.588" size="0.889" layer="21" font="vector" align="bottom-right">MISO</text>
<text x="41.148" y="-8.128" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-13.208" size="0.889" layer="21" font="vector" align="bottom-right">P56</text>
<text x="37.338" y="9.652" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="4.572" y="9.652" size="0.889" layer="21" font="vector">3V3</text>
<text x="4.572" y="4.572" size="0.889" layer="21" font="vector">RX</text>
<text x="4.572" y="2.032" size="0.889" layer="21" font="vector">TX</text>
<text x="4.572" y="-0.508" size="0.889" layer="21" font="vector">P20</text>
<text x="4.572" y="-5.588" size="0.889" layer="21" font="vector">CLK</text>
<text x="4.572" y="-8.128" size="0.889" layer="21" font="vector">P21</text>
<text x="4.572" y="-10.668" size="0.889" layer="21" font="vector">P23</text>
<text x="4.572" y="-13.208" size="0.889" layer="21" font="vector">P54</text>
<wire x1="7.112" y1="10.668" x2="7.112" y2="-13.208" width="0.0508" layer="21"/>
<wire x1="37.846" y1="10.668" x2="37.846" y2="-13.208" width="0.0508" layer="21"/>
</package>
<package name="S-PDSO-G8">
<smd name="3" x="0.325" y="-2.2" dx="1.45" dy="0.45" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="2" x="-0.325" y="-2.2" dx="1.45" dy="0.45" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="1" x="-0.975" y="-2.2" dx="1.45" dy="0.45" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="4" x="0.975" y="-2.2" dx="1.45" dy="0.45" layer="1" rot="R90" stop="no"/>
<smd name="5" x="0.975" y="2.2" dx="1.45" dy="0.45" layer="1" rot="R270" stop="no"/>
<smd name="6" x="0.325" y="2.2" dx="1.45" dy="0.45" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="7" x="-0.325" y="2.2" dx="1.45" dy="0.45" layer="1" rot="R270" stop="no" thermals="no"/>
<smd name="8" x="-0.975" y="2.2" dx="1.45" dy="0.45" layer="1" rot="R270" stop="no" thermals="no"/>
<rectangle x1="-1.25" y1="-2.975" x2="-0.7" y2="-1.425" layer="29"/>
<rectangle x1="-0.6" y1="-2.975" x2="-0.05" y2="-1.425" layer="29"/>
<rectangle x1="0.05" y1="-2.975" x2="0.6" y2="-1.425" layer="29"/>
<rectangle x1="0.7" y1="-2.975" x2="1.25" y2="-1.425" layer="29"/>
<rectangle x1="0.7" y1="1.425" x2="1.25" y2="2.975" layer="29" rot="R180"/>
<rectangle x1="0.05" y1="1.425" x2="0.6" y2="2.975" layer="29" rot="R180"/>
<rectangle x1="-0.6" y1="1.425" x2="-0.05" y2="2.975" layer="29" rot="R180"/>
<rectangle x1="-1.25" y1="1.425" x2="-0.7" y2="2.975" layer="29" rot="R180"/>
<wire x1="-1.35" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.35" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.35" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.35" y2="-1.5" width="0.127" layer="21"/>
<circle x="-1.2" y="-1.2" radius="0.111803125" width="0" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1" layer="51"/>
<rectangle x1="-1.1325" y1="-2.45" x2="-0.8175" y2="-1.5" layer="51"/>
<rectangle x1="-0.4825" y1="-2.45" x2="-0.1675" y2="-1.5" layer="51"/>
<rectangle x1="0.1675" y1="-2.45" x2="0.4825" y2="-1.5" layer="51"/>
<rectangle x1="0.8175" y1="-2.45" x2="1.1325" y2="-1.5" layer="51"/>
<rectangle x1="0.8175" y1="1.5" x2="1.1325" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="0.1675" y1="1.5" x2="0.4825" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="-0.4825" y1="1.5" x2="-0.1675" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="-1.1325" y1="1.5" x2="-0.8175" y2="2.45" layer="51" rot="R180"/>
<text x="-1.7" y="-1.5" size="0.5" layer="25" rot="R90">&gt;NAME</text>
<text x="2.2" y="-1.5" size="0.5" layer="27" rot="R90">&gt;VALUE</text>
<wire x1="1.7" y1="-3.1" x2="-1.6" y2="-3.1" width="0.127" layer="39"/>
<wire x1="-1.6" y1="-3.1" x2="-1.6" y2="3.1" width="0.127" layer="39"/>
<wire x1="-1.6" y1="3.1" x2="1.7" y2="3.1" width="0.127" layer="39"/>
<wire x1="1.7" y1="3.1" x2="1.7" y2="-3.1" width="0.127" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="DRV8842">
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="27.94" y2="-10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="15.24" width="0.254" layer="94"/>
<wire x1="27.94" y1="15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<pin name="CP1" x="-7.62" y="-15.24" length="middle" rot="R90"/>
<pin name="CP2" x="-5.08" y="-15.24" length="middle" rot="R90"/>
<pin name="VCP" x="-2.54" y="-15.24" length="middle" rot="R90"/>
<pin name="VM@4" x="0" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="OUT1@5" x="2.54" y="-15.24" length="middle" direction="out" rot="R90"/>
<pin name="ISEN@6" x="5.08" y="-15.24" length="middle" rot="R90"/>
<pin name="OUT2@7" x="7.62" y="-15.24" length="middle" direction="out" rot="R90"/>
<pin name="OUT2@8" x="10.16" y="-15.24" length="middle" direction="out" rot="R90"/>
<pin name="ISEN@9" x="12.7" y="-15.24" length="middle" rot="R90"/>
<pin name="OUT1@10" x="15.24" y="-15.24" length="middle" direction="out" rot="R90"/>
<pin name="VM@11" x="17.78" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="VREF@12" x="20.32" y="-15.24" length="middle" direction="in" rot="R90"/>
<pin name="VREF@13" x="22.86" y="-15.24" length="middle" direction="in" rot="R90"/>
<pin name="GND@14" x="25.4" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="V3P3OUT" x="25.4" y="20.32" length="middle" direction="out" rot="R270"/>
<pin name="NRESET" x="22.86" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="NSLEEP" x="20.32" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="NFAULT" x="17.78" y="20.32" length="middle" direction="out" rot="R270"/>
<pin name="DECAY" x="15.24" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="IN2" x="12.7" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="IN1" x="10.16" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="NC" x="7.62" y="20.32" length="middle" direction="nc" rot="R270"/>
<pin name="I0" x="5.08" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="I1" x="2.54" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="I2" x="0" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="I3" x="-2.54" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="I4" x="-5.08" y="20.32" length="middle" direction="in" rot="R270"/>
<pin name="GND@28" x="-7.62" y="20.32" length="middle" direction="pwr" rot="R270"/>
<pin name="GND@29" x="33.02" y="1.27" length="middle" direction="pwr" rot="R180"/>
<circle x="-8.9" y="-8.89" radius="0.5" width="0.254" layer="94"/>
<text x="-11.43" y="-10.16" size="1.27" layer="95" rot="R90">&gt;NAME</text>
<text x="30.48" y="-10.16" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CPOL-EU">
<wire x1="-1.524" y1="-0.889" x2="1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.889" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<text x="1.143" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="0.4064" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-4.5974" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.54" x2="1.651" y2="-1.651" layer="94"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="HEADER4X10-BOOSTERPACK-BOTTOM">
<wire x1="-2.54" y1="-12.7" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="15.24" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="15.24" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$1" x="5.08" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$2" x="5.08" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$3" x="5.08" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$4" x="5.08" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$5" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$6" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$7" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$8" x="5.08" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$9" x="5.08" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$10" x="5.08" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="53.34" y1="-12.7" x2="53.34" y2="15.24" width="0.254" layer="94"/>
<wire x1="53.34" y1="15.24" x2="58.42" y2="15.24" width="0.254" layer="94"/>
<wire x1="58.42" y1="15.24" x2="58.42" y2="-12.7" width="0.254" layer="94"/>
<wire x1="58.42" y1="-12.7" x2="53.34" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$20" x="60.96" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$19" x="60.96" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$18" x="60.96" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$17" x="60.96" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$16" x="60.96" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$15" x="60.96" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$14" x="60.96" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$13" x="60.96" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$12" x="60.96" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$11" x="60.96" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$21" x="20.32" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$22" x="20.32" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$23" x="20.32" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$24" x="20.32" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$25" x="20.32" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$26" x="20.32" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$27" x="20.32" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$28" x="20.32" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$29" x="20.32" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$30" x="20.32" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="38.1" y1="-12.7" x2="38.1" y2="15.24" width="0.254" layer="94"/>
<wire x1="38.1" y1="15.24" x2="43.18" y2="15.24" width="0.254" layer="94"/>
<wire x1="43.18" y1="15.24" x2="43.18" y2="-12.7" width="0.254" layer="94"/>
<wire x1="43.18" y1="-12.7" x2="38.1" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$40" x="45.72" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$39" x="45.72" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$38" x="45.72" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$37" x="45.72" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$36" x="45.72" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$35" x="45.72" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$34" x="45.72" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$33" x="45.72" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$32" x="45.72" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$31" x="45.72" y="-10.16" visible="pad" length="short" rot="R180"/>
<text x="-2.54" y="17.78" size="2.54" layer="95" font="vector">J5</text>
<text x="53.34" y="17.78" size="2.54" layer="95" font="vector">J6</text>
<text x="12.7" y="17.78" size="2.54" layer="95" font="vector">J7</text>
<text x="38.1" y="17.78" size="2.54" layer="95" font="vector">J8</text>
</symbol>
<symbol name="INA301">
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<pin name="VS" x="-15.24" y="2.54" length="middle"/>
<pin name="OUT" x="-15.24" y="0" length="middle"/>
<pin name="LIMIT" x="-15.24" y="-2.54" length="middle"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle"/>
<pin name="RESET" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="!ALERT!" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="IN-" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="IN+" x="15.24" y="2.54" length="middle" rot="R180"/>
<text x="-10.16" y="6.35" size="2.032" layer="95">&gt;NAME</text>
<text x="-10.16" y="-11.43" size="2.032" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DRV8842" prefix="U">
<gates>
<gate name="G$1" symbol="DRV8842" x="-7.62" y="-2.54"/>
</gates>
<devices>
<device name="A" package="HTSSOP-28">
<connects>
<connect gate="G$1" pin="CP1" pad="1"/>
<connect gate="G$1" pin="CP2" pad="2"/>
<connect gate="G$1" pin="DECAY" pad="19"/>
<connect gate="G$1" pin="GND@14" pad="14"/>
<connect gate="G$1" pin="GND@28" pad="28"/>
<connect gate="G$1" pin="GND@29" pad="29 P$1 P$2 P$3 P$4 P$5 P$6 P$7 P$8 P$9 P$10 P$11 P$12 P$13 P$14 P$15 P$16 P$17 P$18 P$19 P$20 P$21"/>
<connect gate="G$1" pin="I0" pad="23"/>
<connect gate="G$1" pin="I1" pad="24"/>
<connect gate="G$1" pin="I2" pad="25"/>
<connect gate="G$1" pin="I3" pad="26"/>
<connect gate="G$1" pin="I4" pad="27"/>
<connect gate="G$1" pin="IN1" pad="21"/>
<connect gate="G$1" pin="IN2" pad="20"/>
<connect gate="G$1" pin="ISEN@6" pad="6"/>
<connect gate="G$1" pin="ISEN@9" pad="9"/>
<connect gate="G$1" pin="NC" pad="22"/>
<connect gate="G$1" pin="NFAULT" pad="18"/>
<connect gate="G$1" pin="NRESET" pad="16"/>
<connect gate="G$1" pin="NSLEEP" pad="17"/>
<connect gate="G$1" pin="OUT1@10" pad="10"/>
<connect gate="G$1" pin="OUT1@5" pad="5"/>
<connect gate="G$1" pin="OUT2@7" pad="7"/>
<connect gate="G$1" pin="OUT2@8" pad="8"/>
<connect gate="G$1" pin="V3P3OUT" pad="15"/>
<connect gate="G$1" pin="VCP" pad="3"/>
<connect gate="G$1" pin="VM@11" pad="11"/>
<connect gate="G$1" pin="VM@4" pad="4"/>
<connect gate="G$1" pin="VREF@12" pad="12"/>
<connect gate="G$1" pin="VREF@13" pad="13"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C">
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="1.27"/>
</gates>
<devices>
<device name="A" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ECAP" prefix="C">
<gates>
<gate name="G$1" symbol="CPOL-EU" x="0" y="1.27"/>
</gates>
<devices>
<device name="A" package="153CLV-0605">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R">
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="R6432">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER4X10-BOOSTERPACK-BOTTOM" prefix="A" uservalue="yes">
<gates>
<gate name="G$1" symbol="HEADER4X10-BOOSTERPACK-BOTTOM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4X10-BOOSTERPACK-BOTTOM">
<connects>
<connect gate="G$1" pin="P$1" pad="41"/>
<connect gate="G$1" pin="P$10" pad="50"/>
<connect gate="G$1" pin="P$11" pad="51"/>
<connect gate="G$1" pin="P$12" pad="52"/>
<connect gate="G$1" pin="P$13" pad="53"/>
<connect gate="G$1" pin="P$14" pad="54"/>
<connect gate="G$1" pin="P$15" pad="55"/>
<connect gate="G$1" pin="P$16" pad="56"/>
<connect gate="G$1" pin="P$17" pad="57"/>
<connect gate="G$1" pin="P$18" pad="58"/>
<connect gate="G$1" pin="P$19" pad="59"/>
<connect gate="G$1" pin="P$2" pad="42"/>
<connect gate="G$1" pin="P$20" pad="60"/>
<connect gate="G$1" pin="P$21" pad="61"/>
<connect gate="G$1" pin="P$22" pad="62"/>
<connect gate="G$1" pin="P$23" pad="63"/>
<connect gate="G$1" pin="P$24" pad="64"/>
<connect gate="G$1" pin="P$25" pad="65"/>
<connect gate="G$1" pin="P$26" pad="66"/>
<connect gate="G$1" pin="P$27" pad="67"/>
<connect gate="G$1" pin="P$28" pad="68"/>
<connect gate="G$1" pin="P$29" pad="69"/>
<connect gate="G$1" pin="P$3" pad="43"/>
<connect gate="G$1" pin="P$30" pad="70"/>
<connect gate="G$1" pin="P$31" pad="71"/>
<connect gate="G$1" pin="P$32" pad="72"/>
<connect gate="G$1" pin="P$33" pad="73"/>
<connect gate="G$1" pin="P$34" pad="74"/>
<connect gate="G$1" pin="P$35" pad="75"/>
<connect gate="G$1" pin="P$36" pad="76"/>
<connect gate="G$1" pin="P$37" pad="77"/>
<connect gate="G$1" pin="P$38" pad="78"/>
<connect gate="G$1" pin="P$39" pad="79"/>
<connect gate="G$1" pin="P$4" pad="44"/>
<connect gate="G$1" pin="P$40" pad="80"/>
<connect gate="G$1" pin="P$5" pad="45"/>
<connect gate="G$1" pin="P$6" pad="46"/>
<connect gate="G$1" pin="P$7" pad="47"/>
<connect gate="G$1" pin="P$8" pad="48"/>
<connect gate="G$1" pin="P$9" pad="49"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INA301A1">
<gates>
<gate name="G$1" symbol="INA301" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="S-PDSO-G8">
<connects>
<connect gate="G$1" pin="!ALERT!" pad="6"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="IN+" pad="8"/>
<connect gate="G$1" pin="IN-" pad="7"/>
<connect gate="G$1" pin="LIMIT" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="VS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26997/1" library_version="2">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:27060/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DOCSMAL" urn="urn:adsk.eagle:symbol:13873/1" library_version="1">
<wire x1="88.9" y1="0" x2="88.9" y2="5.08" width="0.254" layer="94"/>
<wire x1="88.9" y1="5.08" x2="149.86" y2="5.08" width="0.254" layer="94"/>
<wire x1="149.86" y1="5.08" x2="149.86" y2="0" width="0.254" layer="94"/>
<wire x1="149.86" y1="5.08" x2="180.34" y2="5.08" width="0.254" layer="94"/>
<wire x1="88.9" y1="5.08" x2="88.9" y2="10.16" width="0.254" layer="94"/>
<wire x1="88.9" y1="10.16" x2="180.34" y2="10.16" width="0.254" layer="94"/>
<text x="90.17" y="6.35" size="2.54" layer="94">Date:</text>
<text x="101.6" y="6.35" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="151.13" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="165.1" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="90.17" y="1.27" size="2.54" layer="94">TITLE:</text>
<text x="106.68" y="1.27" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<frame x1="0" y1="0" x2="180.34" y2="264.16" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4-SMALL-DOCFIELD" urn="urn:adsk.eagle:component:13936/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, with small doc field</description>
<gates>
<gate name="/1" symbol="DOCSMAL" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MainBoard">
<packages>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.073" y1="0.583" x2="1.073" y2="0.583" width="0.0508" layer="39"/>
<wire x1="1.073" y1="0.583" x2="1.073" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="1.073" y1="-0.583" x2="-1.073" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="-1.073" y1="-0.583" x2="-1.073" y2="0.583" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1" thermals="no"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1" thermals="no"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.2" y="0.25"/>
<vertex x="0.2" y="0.25"/>
<vertex x="0.2" y="-0.25"/>
<vertex x="-0.2" y="-0.25"/>
</polygon>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.683" x2="1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.683" x2="1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.683" x2="-1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.683" x2="-1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.2" y="0.42"/>
<vertex x="0.2" y="0.42"/>
<vertex x="0.2" y="-0.4"/>
<vertex x="-0.2" y="-0.4"/>
</polygon>
</package>
<package name="C0603">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1" stop="no" thermals="no" cream="no"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
<rectangle x1="-0.425" y1="-0.225" x2="-0.075" y2="0.225" layer="29"/>
<rectangle x1="0.075" y1="-0.225" x2="0.425" y2="0.225" layer="29"/>
<rectangle x1="-0.425" y1="-0.225" x2="-0.075" y2="0.225" layer="31"/>
<rectangle x1="0.075" y1="-0.225" x2="0.425" y2="0.225" layer="31"/>
<wire x1="-0.5" y1="-0.3" x2="-0.5" y2="0.3" width="0.05" layer="39"/>
<wire x1="-0.5" y1="0.3" x2="0.5" y2="0.3" width="0.05" layer="39"/>
<wire x1="0.5" y1="0.3" x2="0.5" y2="-0.3" width="0.05" layer="39"/>
<wire x1="0.5" y1="-0.3" x2="-0.5" y2="-0.3" width="0.05" layer="39"/>
<wire x1="0" y1="0.07" x2="0" y2="-0.07" width="0.17" layer="21"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1" thermals="no"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1" thermals="no"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.5" y="1.25"/>
<vertex x="0.5" y="1.25"/>
<vertex x="0.5" y="-1.25"/>
<vertex x="-0.5" y="-1.25"/>
</polygon>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1" thermals="no"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1" thermals="no"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.1" y="0.65"/>
<vertex x="0.1" y="0.65"/>
<vertex x="0.1" y="-0.65"/>
<vertex x="-0.1" y="-0.65"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="0" y="2.54" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="0" y="-5.08" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C">
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="1.27"/>
</gates>
<devices>
<device name="A" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="motor_driver" deviceset="DRV8842" device="A"/>
<part name="C1" library="motor_driver" deviceset="CAP" device="A" value="0.01uF"/>
<part name="C2" library="motor_driver" deviceset="CAP" device="A" value="0.1uF"/>
<part name="C3" library="motor_driver" deviceset="CAP" device="A" value="0.1uF"/>
<part name="C4" library="motor_driver" deviceset="CAP" device="A" value="0.1uF"/>
<part name="C5" library="motor_driver" deviceset="CAP" device="A" value="0.47uF"/>
<part name="C6" library="motor_driver" deviceset="ECAP" device="A" value="100uF"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VCC" device="" value="VCC"/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R5" library="motor_driver" deviceset="RES" device="B" value="30m"/>
<part name="SUPPLY9" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY12" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U2" library="motor_driver" deviceset="DRV8842" device="A"/>
<part name="C8" library="motor_driver" deviceset="CAP" device="A" value="0.01uF"/>
<part name="C9" library="motor_driver" deviceset="CAP" device="A" value="0.47uF"/>
<part name="SUPPLY13" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY14" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY15" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY16" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY17" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C10" library="motor_driver" deviceset="CAP" device="A" value="0.1uF"/>
<part name="R12" library="motor_driver" deviceset="RES" device="B" value="30m"/>
<part name="SUPPLY20" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C12" library="motor_driver" deviceset="CAP" device="A" value="0.1uF"/>
<part name="C13" library="motor_driver" deviceset="CAP" device="A" value="0.1uF"/>
<part name="SUPPLY21" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY22" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="VM" library="motor_driver" deviceset="PINHD-1X2" device="" value="+"/>
<part name="M1" library="motor_driver" deviceset="PINHD-1X2" device="" value="L"/>
<part name="M2" library="motor_driver" deviceset="PINHD-1X2" device="" value="R"/>
<part name="SUPPLY23" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4-SMALL-DOCFIELD" device="" value="MD"/>
<part name="C14" library="motor_driver" deviceset="ECAP" device="A" value="100uF"/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY24" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VCC" device="" value="VCC"/>
<part name="R1" library="motor_driver" deviceset="RES" device="C" value="1M"/>
<part name="R15" library="motor_driver" deviceset="RES" device="C" value="1K"/>
<part name="R4" library="motor_driver" deviceset="RES" device="C" value="10K"/>
<part name="R3" library="motor_driver" deviceset="RES" device="C" value="16.5K"/>
<part name="R2" library="motor_driver" deviceset="RES" device="C" value="1.8K"/>
<part name="R11" library="motor_driver" deviceset="RES" device="C" value="1M"/>
<part name="R16" library="motor_driver" deviceset="RES" device="C" value="1K"/>
<part name="R10" library="motor_driver" deviceset="RES" device="C" value="10K"/>
<part name="R9" library="motor_driver" deviceset="RES" device="C" value="16.5K"/>
<part name="R8" library="motor_driver" deviceset="RES" device="C" value="1.8K"/>
<part name="A1" library="motor_driver" deviceset="HEADER4X10-BOOSTERPACK-BOTTOM" device="" value=" "/>
<part name="SUPPLY25" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY26" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U3" library="motor_driver" deviceset="INA301A1" device="A"/>
<part name="U4" library="motor_driver" deviceset="INA301A1" device="A"/>
<part name="C7" library="MainBoard" deviceset="CAP" device="B" value="1000 pF"/>
<part name="C11" library="MainBoard" deviceset="CAP" device="B" value="1000 pF"/>
<part name="SUPPLY10" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R6" library="motor_driver" deviceset="RES" device="C" value="10K"/>
<part name="R7" library="motor_driver" deviceset="RES" device="C" value="11.3K"/>
<part name="SUPPLY18" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY19" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C16" library="motor_driver" deviceset="CAP" device="A" value="0.1 uF"/>
<part name="R13" library="motor_driver" deviceset="RES" device="C" value="10K"/>
<part name="R14" library="motor_driver" deviceset="RES" device="C" value="11.3K"/>
<part name="SUPPLY27" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY28" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY30" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C15" library="motor_driver" deviceset="CAP" device="A" value="0.1 uF"/>
<part name="R17" library="motor_driver" deviceset="RES" device="C" value="56"/>
<part name="R18" library="motor_driver" deviceset="RES" device="C" value="56"/>
<part name="C17" library="MainBoard" deviceset="CAP" device="B" value="2200 pF"/>
<part name="C18" library="MainBoard" deviceset="CAP" device="B" value="2200 pF"/>
<part name="SUPPLY31" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY32" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="-91.44" y1="-22.86" x2="83.82" y2="-22.86" width="0.1524" layer="97"/>
<wire x1="83.82" y1="-22.86" x2="83.82" y2="-113.03" width="0.1524" layer="97"/>
<wire x1="83.82" y1="-113.03" x2="-91.44" y2="-113.03" width="0.1524" layer="97"/>
<wire x1="-91.44" y1="-113.03" x2="-91.44" y2="-22.86" width="0.1524" layer="97"/>
<wire x1="-91.44" y1="-19.05" x2="83.82" y2="-19.05" width="0.1524" layer="97"/>
<wire x1="83.82" y1="-19.05" x2="83.82" y2="68.58" width="0.1524" layer="97"/>
<wire x1="83.82" y1="68.58" x2="-91.44" y2="68.58" width="0.1524" layer="97"/>
<wire x1="-91.44" y1="68.58" x2="-91.44" y2="-19.05" width="0.1524" layer="97"/>
<text x="62.23" y="-16.51" size="1.778" layer="97">Driver for Motor 1</text>
<text x="62.23" y="-111.76" size="1.778" layer="97">Driver for Motor 2</text>
<wire x1="-91.44" y1="-118.11" x2="83.82" y2="-118.11" width="0.1524" layer="97"/>
<wire x1="83.82" y1="-118.11" x2="83.82" y2="-165.1" width="0.1524" layer="97"/>
<wire x1="83.82" y1="-165.1" x2="-91.44" y2="-165.1" width="0.1524" layer="97"/>
<wire x1="-91.44" y1="-165.1" x2="-91.44" y2="-118.11" width="0.1524" layer="97"/>
<text x="62.23" y="-163.83" size="1.778" layer="97">Connectors</text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="34.29" y="49.53" rot="R270"/>
<instance part="C1" gate="G$1" x="5.08" y="62.23"/>
<instance part="C2" gate="G$1" x="-15.24" y="53.34"/>
<instance part="C3" gate="G$1" x="-26.67" y="40.64"/>
<instance part="C4" gate="G$1" x="-15.24" y="40.64"/>
<instance part="C5" gate="G$1" x="57.15" y="15.24"/>
<instance part="C6" gate="G$1" x="-38.1" y="40.64"/>
<instance part="SUPPLY1" gate="GND" x="57.15" y="-8.89"/>
<instance part="SUPPLY2" gate="GND" x="67.31" y="54.61"/>
<instance part="SUPPLY3" gate="GND" x="16.51" y="-8.89"/>
<instance part="SUPPLY4" gate="GND" x="35.56" y="-8.89"/>
<instance part="SUPPLY5" gate="GND" x="-38.1" y="30.48"/>
<instance part="SUPPLY" gate="G$1" x="-38.1" y="62.23"/>
<instance part="SUPPLY7" gate="GND" x="-15.24" y="30.48"/>
<instance part="SUPPLY8" gate="GND" x="-26.67" y="30.48"/>
<instance part="R5" gate="G$1" x="-81.28" y="46.99" smashed="yes" rot="R90">
<attribute name="NAME" x="-79.375" y="47.8536" size="1.778" layer="95"/>
<attribute name="VALUE" x="-79.375" y="44.323" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="GND" x="71.12" y="-8.89"/>
<instance part="SUPPLY12" gate="GND" x="-81.28" y="36.83"/>
<instance part="U2" gate="G$1" x="33.02" y="-43.18" rot="R270"/>
<instance part="C8" gate="G$1" x="3.81" y="-30.48"/>
<instance part="C9" gate="G$1" x="55.88" y="-77.47"/>
<instance part="SUPPLY13" gate="GND" x="55.88" y="-101.6"/>
<instance part="SUPPLY14" gate="GND" x="66.04" y="-38.1"/>
<instance part="SUPPLY15" gate="GND" x="15.24" y="-101.6"/>
<instance part="SUPPLY16" gate="GND" x="34.29" y="-101.6"/>
<instance part="SUPPLY17" gate="GND" x="69.85" y="-101.6"/>
<instance part="C10" gate="G$1" x="-20.32" y="-36.83"/>
<instance part="R12" gate="G$1" x="-81.28" y="-43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="-79.375" y="-42.3164" size="1.778" layer="95"/>
<attribute name="VALUE" x="-79.375" y="-45.847" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY20" gate="GND" x="-81.28" y="-53.34"/>
<instance part="C12" gate="G$1" x="-31.75" y="-49.53"/>
<instance part="C13" gate="G$1" x="-20.32" y="-49.53"/>
<instance part="SUPPLY21" gate="GND" x="-20.32" y="-59.69"/>
<instance part="SUPPLY22" gate="GND" x="-31.75" y="-59.69"/>
<instance part="VM" gate="G$1" x="66.04" y="-125.73"/>
<instance part="M1" gate="G$1" x="66.04" y="-139.7"/>
<instance part="M2" gate="G$1" x="66.04" y="-153.67"/>
<instance part="SUPPLY23" gate="GND" x="53.34" y="-129.54"/>
<instance part="FRAME1" gate="/1" x="-93.98" y="-190.5"/>
<instance part="C14" gate="G$1" x="-43.18" y="-49.53"/>
<instance part="SUPPLY6" gate="GND" x="-43.18" y="-59.69"/>
<instance part="SUPPLY24" gate="G$1" x="-43.18" y="-29.21"/>
<instance part="R1" gate="G$1" x="-26.67" y="52.07" smashed="yes" rot="R90">
<attribute name="NAME" x="-25.4" y="53.5686" size="1.778" layer="95"/>
<attribute name="VALUE" x="-25.4" y="48.768" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="67.31" y="44.45"/>
<instance part="R4" gate="G$1" x="71.12" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="73.66" y="26.8986" size="1.778" layer="95"/>
<attribute name="VALUE" x="73.66" y="23.368" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="71.12" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="73.66" y="14.1986" size="1.778" layer="95"/>
<attribute name="VALUE" x="73.66" y="10.668" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="71.12" y="0" smashed="yes" rot="R90">
<attribute name="NAME" x="73.66" y="1.4986" size="1.778" layer="95"/>
<attribute name="VALUE" x="73.66" y="-2.032" size="1.778" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="-31.75" y="-38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="-29.21" y="-36.6014" size="1.778" layer="95"/>
<attribute name="VALUE" x="-29.21" y="-41.402" size="1.778" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="66.04" y="-48.26"/>
<instance part="R10" gate="G$1" x="69.85" y="-67.31" smashed="yes" rot="R90">
<attribute name="NAME" x="72.39" y="-65.8114" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.39" y="-69.342" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="69.85" y="-80.01" smashed="yes" rot="R90">
<attribute name="NAME" x="72.39" y="-78.5114" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.39" y="-82.042" size="1.778" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="69.85" y="-92.71" smashed="yes" rot="R90">
<attribute name="NAME" x="72.39" y="-91.2114" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.39" y="-94.742" size="1.778" layer="96"/>
</instance>
<instance part="A1" gate="G$1" x="-83.82" y="-142.24"/>
<instance part="SUPPLY25" gate="GND" x="-52.07" y="-135.89"/>
<instance part="SUPPLY26" gate="GND" x="-10.16" y="-135.89"/>
<instance part="U3" gate="G$1" x="-41.91" y="10.16"/>
<instance part="U4" gate="G$1" x="-36.83" y="-81.28"/>
<instance part="C7" gate="G$1" x="-72.39" y="48.26"/>
<instance part="C11" gate="G$1" x="-71.12" y="-41.91"/>
<instance part="SUPPLY10" gate="GND" x="-25.4" y="-8.89"/>
<instance part="SUPPLY11" gate="GND" x="-58.42" y="-8.89"/>
<instance part="R6" gate="G$1" x="-15.24" y="13.97" smashed="yes" rot="R90">
<attribute name="NAME" x="-12.7" y="15.4686" size="1.778" layer="95"/>
<attribute name="VALUE" x="-12.7" y="11.938" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="-64.77" y="1.27" smashed="yes" rot="R90">
<attribute name="NAME" x="-69.85" y="1.4986" size="1.778" layer="95"/>
<attribute name="VALUE" x="-72.39" y="-2.032" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY18" gate="GND" x="-64.77" y="-8.89"/>
<instance part="SUPPLY19" gate="GND" x="-74.93" y="-8.89"/>
<instance part="C16" gate="G$1" x="-73.66" y="-90.17" smashed="yes">
<attribute name="NAME" x="-78.486" y="-89.789" size="1.778" layer="95"/>
<attribute name="VALUE" x="-81.026" y="-94.869" size="1.778" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="-7.62" y="-77.47" smashed="yes" rot="R90">
<attribute name="NAME" x="-9.1186" y="-81.28" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-4.318" y="-81.28" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R14" gate="G$1" x="-63.5" y="-90.17" smashed="yes" rot="R90">
<attribute name="NAME" x="-69.85" y="-89.9414" size="1.778" layer="95"/>
<attribute name="VALUE" x="-71.12" y="-94.742" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY27" gate="GND" x="-53.34" y="-100.33"/>
<instance part="SUPPLY28" gate="GND" x="-20.32" y="-100.33"/>
<instance part="SUPPLY29" gate="GND" x="-63.5" y="-100.33"/>
<instance part="SUPPLY30" gate="GND" x="-73.66" y="-100.33"/>
<instance part="C15" gate="G$1" x="-74.93" y="2.54" smashed="yes">
<attribute name="NAME" x="-79.756" y="2.921" size="1.778" layer="95"/>
<attribute name="VALUE" x="-82.296" y="-2.159" size="1.778" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="20.32" y="-123.19" smashed="yes">
<attribute name="NAME" x="19.05" y="-121.6914" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.05" y="-126.492" size="1.778" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="20.32" y="-144.78" smashed="yes">
<attribute name="NAME" x="19.05" y="-143.2814" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.05" y="-148.082" size="1.778" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="11.43" y="-129.54" smashed="yes">
<attribute name="NAME" x="5.334" y="-129.159" size="1.778" layer="95"/>
<attribute name="VALUE" x="1.524" y="-134.239" size="1.778" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="11.43" y="-151.13" smashed="yes">
<attribute name="NAME" x="5.334" y="-150.749" size="1.778" layer="95"/>
<attribute name="VALUE" x="1.524" y="-155.829" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY31" gate="GND" x="11.43" y="-138.43"/>
<instance part="SUPPLY32" gate="GND" x="11.43" y="-160.02"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="55.88" y1="-82.55" x2="55.88" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND@28"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="53.34" y1="-35.56" x2="66.04" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND@14"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="17.78" y1="-68.58" x2="15.24" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-68.58" x2="15.24" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND@29"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="34.29" y1="-76.2" x2="34.29" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="69.85" y1="-97.79" x2="69.85" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="-81.28" y1="-50.8" x2="-81.28" y2="-49.53" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="-49.53" x2="-81.28" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-46.99" x2="-71.12" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-49.53" x2="-81.28" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-49.53" x2="-68.58" y2="-49.53" width="0.1524" layer="91"/>
<label x="-68.58" y="-49.53" size="0.762" layer="95" xref="yes"/>
<junction x="-71.12" y="-49.53"/>
<junction x="-81.28" y="-49.53"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<wire x1="-20.32" y1="-54.61" x2="-20.32" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="-31.75" y1="-57.15" x2="-31.75" y2="-54.61" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="-"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="-38.1" y1="35.56" x2="-38.1" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="-26.67" y1="35.56" x2="-26.67" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="-15.24" y1="35.56" x2="-15.24" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND@14"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="19.05" y1="24.13" x2="16.51" y2="24.13" width="0.1524" layer="91"/>
<wire x1="16.51" y1="24.13" x2="16.51" y2="-6.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="41.91" x2="-81.28" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="40.64" x2="-81.28" y2="39.37" width="0.1524" layer="91"/>
<wire x1="-72.39" y1="43.18" x2="-72.39" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-72.39" y1="40.64" x2="-81.28" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-72.39" y1="40.64" x2="-69.85" y2="40.64" width="0.1524" layer="91"/>
<label x="-69.85" y="40.64" size="0.762" layer="95" xref="yes"/>
<junction x="-72.39" y="40.64"/>
<junction x="-81.28" y="40.64"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND@29"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="35.56" y1="16.51" x2="35.56" y2="-6.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND@28"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="54.61" y1="57.15" x2="67.31" y2="57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="71.12" y1="-6.35" x2="71.12" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="57.15" y1="-6.35" x2="57.15" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VM" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<wire x1="63.5" y1="-125.73" x2="53.34" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-125.73" x2="53.34" y2="-127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="-"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="-43.18" y1="-54.61" x2="-43.18" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$22"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<wire x1="-63.5" y1="-132.08" x2="-52.07" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="-52.07" y1="-132.08" x2="-52.07" y2="-133.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$20"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<wire x1="-22.86" y1="-129.54" x2="-10.16" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-129.54" x2="-10.16" y2="-133.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RESET"/>
<wire x1="-26.67" y1="5.08" x2="-25.4" y2="5.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="-25.4" y1="5.08" x2="-25.4" y2="-6.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="-57.15" y1="5.08" x2="-58.42" y2="5.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="-58.42" y1="5.08" x2="-58.42" y2="-6.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="IN-"/>
<wire x1="-26.67" y1="10.16" x2="-25.4" y2="10.16" width="0.1524" layer="91"/>
<label x="-25.4" y="10.16" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="-64.77" y1="-3.81" x2="-64.77" y2="-6.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="-74.93" y1="-2.54" x2="-74.93" y2="-6.35" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN-"/>
<wire x1="-21.59" y1="-81.28" x2="-20.32" y2="-81.28" width="0.1524" layer="91"/>
<label x="-20.32" y="-81.28" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="-52.07" y1="-86.36" x2="-53.34" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="-53.34" y1="-97.79" x2="-53.34" y2="-86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
<wire x1="-63.5" y1="-95.25" x2="-63.5" y2="-97.79" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="RESET"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="-21.59" y1="-86.36" x2="-20.32" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-86.36" x2="-20.32" y2="-97.79" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="-73.66" y1="-95.25" x2="-73.66" y2="-97.79" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="11.43" y1="-157.48" x2="11.43" y2="-156.21" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="11.43" y1="-135.89" x2="11.43" y2="-134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="DECAY"/>
<wire x1="54.61" y1="34.29" x2="57.15" y2="34.29" width="0.1524" layer="91"/>
<label x="57.15" y="34.29" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="DECAY"/>
<wire x1="53.34" y1="-58.42" x2="58.42" y2="-58.42" width="0.1524" layer="91"/>
<label x="58.42" y="-58.42" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="NRESET" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="NRESET"/>
<wire x1="54.61" y1="26.67" x2="57.15" y2="26.67" width="0.1524" layer="91"/>
<label x="57.15" y="26.67" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$37"/>
<wire x1="-38.1" y1="-137.16" x2="-36.83" y2="-137.16" width="0.1524" layer="91"/>
<label x="-36.83" y="-137.16" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="NSLEEP" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="NSLEEP"/>
<wire x1="54.61" y1="29.21" x2="57.15" y2="29.21" width="0.1524" layer="91"/>
<label x="57.15" y="29.21" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$38"/>
<wire x1="-38.1" y1="-134.62" x2="-36.83" y2="-134.62" width="0.1524" layer="91"/>
<label x="-36.83" y="-134.62" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="NFAULT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="NFAULT"/>
<wire x1="54.61" y1="31.75" x2="71.12" y2="31.75" width="0.1524" layer="91"/>
<wire x1="71.12" y1="31.75" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<wire x1="71.12" y1="31.75" x2="74.93" y2="31.75" width="0.1524" layer="91"/>
<label x="74.93" y="31.75" size="0.762" layer="95" xref="yes"/>
<junction x="71.12" y="31.75"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$15"/>
<wire x1="-22.86" y1="-142.24" x2="-20.32" y2="-142.24" width="0.1524" layer="91"/>
<label x="-20.32" y="-142.24" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="IN2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN2"/>
<wire x1="54.61" y1="36.83" x2="57.15" y2="36.83" width="0.1524" layer="91"/>
<label x="57.15" y="36.83" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$39"/>
<wire x1="-38.1" y1="-132.08" x2="-36.83" y2="-132.08" width="0.1524" layer="91"/>
<label x="-36.83" y="-132.08" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="IN1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IN1"/>
<wire x1="54.61" y1="39.37" x2="57.15" y2="39.37" width="0.1524" layer="91"/>
<label x="57.15" y="39.37" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$40"/>
<wire x1="-38.1" y1="-129.54" x2="-36.83" y2="-129.54" width="0.1524" layer="91"/>
<label x="-36.83" y="-129.54" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="VREF" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VREF@13"/>
<wire x1="19.05" y1="26.67" x2="16.51" y2="26.67" width="0.1524" layer="91"/>
<wire x1="16.51" y1="26.67" x2="16.51" y2="29.21" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VREF@12"/>
<wire x1="16.51" y1="29.21" x2="19.05" y2="29.21" width="0.1524" layer="91"/>
<wire x1="16.51" y1="26.67" x2="15.24" y2="26.67" width="0.1524" layer="91"/>
<label x="15.24" y="26.67" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="16.51" y="26.67"/>
</segment>
<segment>
<wire x1="71.12" y1="7.62" x2="71.12" y2="6.35" width="0.1524" layer="91"/>
<wire x1="71.12" y1="6.35" x2="71.12" y2="5.08" width="0.1524" layer="91"/>
<wire x1="71.12" y1="6.35" x2="74.93" y2="6.35" width="0.1524" layer="91"/>
<label x="74.93" y="6.35" size="0.762" layer="95" xref="yes"/>
<junction x="71.12" y="6.35"/>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="OUT1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="OUT1@5"/>
<wire x1="19.05" y1="46.99" x2="6.35" y2="46.99" width="0.1524" layer="91"/>
<wire x1="6.35" y1="46.99" x2="6.35" y2="34.29" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT1@10"/>
<wire x1="6.35" y1="34.29" x2="19.05" y2="34.29" width="0.1524" layer="91"/>
<label x="6.35" y="34.29" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="1"/>
<wire x1="63.5" y1="-137.16" x2="52.07" y2="-137.16" width="0.1524" layer="91"/>
<label x="52.07" y="-137.16" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ISEN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="ISEN@9"/>
<wire x1="19.05" y1="36.83" x2="11.43" y2="36.83" width="0.1524" layer="91"/>
<wire x1="11.43" y1="36.83" x2="11.43" y2="44.45" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="ISEN@6"/>
<wire x1="11.43" y1="44.45" x2="19.05" y2="44.45" width="0.1524" layer="91"/>
<label x="11.43" y="36.83" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="52.07" x2="-81.28" y2="53.34" width="0.1524" layer="91"/>
<label x="-81.28" y="57.15" size="0.762" layer="95" xref="yes"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="53.34" x2="-81.28" y2="57.15" width="0.1524" layer="91"/>
<wire x1="-72.39" y1="50.8" x2="-72.39" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-72.39" y1="53.34" x2="-81.28" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-72.39" y1="53.34" x2="-69.85" y2="53.34" width="0.1524" layer="91"/>
<label x="-69.85" y="53.34" size="0.762" layer="95" xref="yes"/>
<junction x="-72.39" y="53.34"/>
<junction x="-81.28" y="53.34"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="IN+"/>
<wire x1="-26.67" y1="12.7" x2="-25.4" y2="12.7" width="0.1524" layer="91"/>
<label x="-25.4" y="12.7" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="OUT2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="OUT2@8"/>
<wire x1="19.05" y1="39.37" x2="16.51" y2="39.37" width="0.1524" layer="91"/>
<wire x1="16.51" y1="39.37" x2="16.51" y2="41.91" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT2@7"/>
<wire x1="16.51" y1="41.91" x2="19.05" y2="41.91" width="0.1524" layer="91"/>
<label x="16.51" y="39.37" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="M1" gate="G$1" pin="2"/>
<wire x1="63.5" y1="-139.7" x2="52.07" y2="-139.7" width="0.1524" layer="91"/>
<label x="52.07" y="-139.7" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VCP" class="0">
<segment>
<wire x1="-26.67" y1="57.15" x2="-26.67" y2="59.69" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-26.67" y1="59.69" x2="-15.24" y2="59.69" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="59.69" x2="-15.24" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="59.69" x2="-10.16" y2="59.69" width="0.1524" layer="91"/>
<label x="-10.16" y="59.69" size="0.762" layer="95" xref="yes"/>
<junction x="-15.24" y="59.69"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VCP"/>
<wire x1="19.05" y1="52.07" x2="16.51" y2="52.07" width="0.1524" layer="91"/>
<label x="16.51" y="52.07" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V3P3OUT" class="0">
<segment>
<wire x1="72.39" y1="44.45" x2="74.93" y2="44.45" width="0.1524" layer="91"/>
<label x="74.93" y="44.45" size="0.762" layer="95" xref="yes"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="71.12" y1="20.32" x2="71.12" y2="19.05" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="V3P3OUT"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="71.12" y1="19.05" x2="71.12" y2="17.78" width="0.1524" layer="91"/>
<wire x1="54.61" y1="24.13" x2="57.15" y2="24.13" width="0.1524" layer="91"/>
<wire x1="57.15" y1="24.13" x2="57.15" y2="19.05" width="0.1524" layer="91"/>
<wire x1="57.15" y1="19.05" x2="57.15" y2="17.78" width="0.1524" layer="91"/>
<wire x1="71.12" y1="19.05" x2="57.15" y2="19.05" width="0.1524" layer="91"/>
<wire x1="71.12" y1="19.05" x2="74.93" y2="19.05" width="0.1524" layer="91"/>
<label x="74.93" y="19.05" size="0.762" layer="95" xref="yes"/>
<junction x="71.12" y="19.05"/>
<junction x="57.15" y="19.05"/>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VSNS" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="P$23"/>
<wire x1="-63.5" y1="-134.62" x2="-60.96" y2="-134.62" width="0.1524" layer="91"/>
<label x="-60.96" y="-134.62" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="15.24" y1="-123.19" x2="11.43" y2="-123.19" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-123.19" x2="11.43" y2="-127" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-123.19" x2="8.89" y2="-123.19" width="0.1524" layer="91"/>
<label x="8.89" y="-123.19" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="11.43" y="-123.19"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="CP2"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="17.78" y1="-38.1" x2="3.81" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="3.81" y1="-38.1" x2="3.81" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="CP1"/>
<wire x1="17.78" y1="-35.56" x2="17.78" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-25.4" x2="3.81" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="3.81" y1="-25.4" x2="3.81" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V3P3OUT2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="V3P3OUT"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="53.34" y1="-68.58" x2="55.88" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-68.58" x2="55.88" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-73.66" x2="55.88" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-72.39" x2="69.85" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-73.66" x2="55.88" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-74.93" x2="69.85" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-73.66" x2="72.39" y2="-73.66" width="0.1524" layer="91"/>
<label x="72.39" y="-73.66" size="0.762" layer="95" xref="yes"/>
<junction x="69.85" y="-73.66"/>
<junction x="55.88" y="-73.66"/>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="71.12" y1="-48.26" x2="73.66" y2="-48.26" width="0.1524" layer="91"/>
<label x="73.66" y="-48.26" size="0.762" layer="95" xref="yes"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VCP2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VCP"/>
<wire x1="17.78" y1="-40.64" x2="15.24" y2="-40.64" width="0.1524" layer="91"/>
<label x="15.24" y="-40.64" size="0.762" layer="95" ratio="6" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-31.75" y1="-33.02" x2="-31.75" y2="-31.75" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-31.75" y1="-31.75" x2="-20.32" y2="-31.75" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-31.75" x2="-20.32" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-31.75" x2="-15.24" y2="-31.75" width="0.1524" layer="91"/>
<label x="-15.24" y="-31.75" size="0.762" layer="95" xref="yes"/>
<junction x="-20.32" y="-31.75"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="OUT12" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUT1@5"/>
<pinref part="U2" gate="G$1" pin="OUT1@10"/>
<wire x1="17.78" y1="-58.42" x2="3.81" y2="-58.42" width="0.1524" layer="91"/>
<label x="3.81" y="-58.42" size="0.762" layer="95" ratio="6" rot="R180" xref="yes"/>
<wire x1="17.78" y1="-45.72" x2="3.81" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="3.81" y1="-45.72" x2="3.81" y2="-58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="M2" gate="G$1" pin="1"/>
<wire x1="63.5" y1="-151.13" x2="54.61" y2="-151.13" width="0.1524" layer="91"/>
<label x="54.61" y="-151.13" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ISEN2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ISEN@6"/>
<pinref part="U2" gate="G$1" pin="ISEN@9"/>
<wire x1="17.78" y1="-55.88" x2="10.16" y2="-55.88" width="0.1524" layer="91"/>
<label x="10.16" y="-55.88" size="0.762" layer="95" ratio="6" rot="R180" xref="yes"/>
<wire x1="17.78" y1="-48.26" x2="10.16" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-48.26" x2="10.16" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="-38.1" x2="-81.28" y2="-36.83" width="0.1524" layer="91"/>
<label x="-81.28" y="-33.655" size="0.762" layer="95" xref="yes"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="-36.83" x2="-81.28" y2="-33.655" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-39.37" x2="-71.12" y2="-36.83" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-36.83" x2="-81.28" y2="-36.83" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-36.83" x2="-68.58" y2="-36.83" width="0.1524" layer="91"/>
<label x="-68.58" y="-36.83" size="0.762" layer="95" xref="yes"/>
<junction x="-71.12" y="-36.83"/>
<junction x="-81.28" y="-36.83"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IN+"/>
<wire x1="-21.59" y1="-78.74" x2="-20.32" y2="-78.74" width="0.1524" layer="91"/>
<label x="-20.32" y="-78.74" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="OUT22" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUT2@7"/>
<wire x1="17.78" y1="-50.8" x2="16.51" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="OUT2@8"/>
<wire x1="17.78" y1="-53.34" x2="16.51" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="16.51" y1="-53.34" x2="16.51" y2="-50.8" width="0.1524" layer="91"/>
<label x="16.51" y="-53.34" size="0.762" layer="95" ratio="6" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="M2" gate="G$1" pin="2"/>
<wire x1="63.5" y1="-153.67" x2="54.61" y2="-153.67" width="0.1524" layer="91"/>
<label x="54.61" y="-153.67" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VREF2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VREF@12"/>
<wire x1="17.78" y1="-63.5" x2="15.24" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-63.5" x2="15.24" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VREF@13"/>
<wire x1="15.24" y1="-66.04" x2="17.78" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-66.04" x2="12.7" y2="-66.04" width="0.1524" layer="91"/>
<label x="12.7" y="-66.04" size="0.762" layer="95" ratio="6" rot="R180" xref="yes"/>
<junction x="15.24" y="-66.04"/>
</segment>
<segment>
<wire x1="69.85" y1="-85.09" x2="69.85" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-86.36" x2="69.85" y2="-87.63" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-86.36" x2="73.66" y2="-86.36" width="0.1524" layer="91"/>
<label x="73.66" y="-86.36" size="0.762" layer="95" ratio="6" xref="yes"/>
<junction x="69.85" y="-86.36"/>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="R8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="NRESET2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="NRESET"/>
<wire x1="53.34" y1="-66.04" x2="58.42" y2="-66.04" width="0.1524" layer="91"/>
<label x="58.42" y="-66.04" size="0.762" layer="95" ratio="6" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$12"/>
<wire x1="-22.86" y1="-149.86" x2="-20.32" y2="-149.86" width="0.1524" layer="91"/>
<label x="-20.32" y="-149.86" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="NSLEEP2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="NSLEEP"/>
<wire x1="53.34" y1="-63.5" x2="58.42" y2="-63.5" width="0.1524" layer="91"/>
<label x="58.42" y="-63.5" size="0.762" layer="95" ratio="6" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$13"/>
<wire x1="-22.86" y1="-147.32" x2="-20.32" y2="-147.32" width="0.1524" layer="91"/>
<label x="-20.32" y="-147.32" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="NFAULT2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="NFAULT"/>
<wire x1="53.34" y1="-60.96" x2="69.85" y2="-60.96" width="0.1524" layer="91"/>
<label x="72.39" y="-60.96" size="0.762" layer="95" ratio="6" xref="yes"/>
<wire x1="69.85" y1="-60.96" x2="72.39" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-62.23" x2="69.85" y2="-60.96" width="0.1524" layer="91"/>
<junction x="69.85" y="-60.96"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$14"/>
<wire x1="-22.86" y1="-144.78" x2="-20.32" y2="-144.78" width="0.1524" layer="91"/>
<label x="-20.32" y="-144.78" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="IN22" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IN2"/>
<wire x1="53.34" y1="-55.88" x2="58.42" y2="-55.88" width="0.1524" layer="91"/>
<label x="58.42" y="-55.88" size="0.762" layer="95" ratio="6" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$35"/>
<wire x1="-38.1" y1="-142.24" x2="-36.83" y2="-142.24" width="0.1524" layer="91"/>
<label x="-36.83" y="-142.24" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="IN12" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IN1"/>
<wire x1="53.34" y1="-53.34" x2="58.42" y2="-53.34" width="0.1524" layer="91"/>
<label x="58.42" y="-53.34" size="0.762" layer="95" ratio="6" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$36"/>
<wire x1="-38.1" y1="-139.7" x2="-36.83" y2="-139.7" width="0.1524" layer="91"/>
<label x="-36.83" y="-139.7" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="VSNS2" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="P$29"/>
<wire x1="-63.5" y1="-149.86" x2="-60.96" y2="-149.86" width="0.1524" layer="91"/>
<label x="-60.96" y="-149.86" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="15.24" y1="-144.78" x2="11.43" y2="-144.78" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-144.78" x2="11.43" y2="-148.59" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-144.78" x2="8.89" y2="-144.78" width="0.1524" layer="91"/>
<label x="8.89" y="-144.78" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="11.43" y="-144.78"/>
</segment>
</net>
<net name="I02" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="I4"/>
<wire x1="53.34" y1="-38.1" x2="58.42" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="I0"/>
<wire x1="53.34" y1="-48.26" x2="58.42" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-38.1" x2="58.42" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="I3"/>
<wire x1="58.42" y1="-40.64" x2="58.42" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-43.18" x2="58.42" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-45.72" x2="58.42" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-40.64" x2="58.42" y2="-40.64" width="0.1524" layer="91"/>
<junction x="58.42" y="-40.64"/>
<pinref part="U2" gate="G$1" pin="I2"/>
<wire x1="53.34" y1="-43.18" x2="58.42" y2="-43.18" width="0.1524" layer="91"/>
<junction x="58.42" y="-43.18"/>
<pinref part="U2" gate="G$1" pin="I1"/>
<wire x1="53.34" y1="-45.72" x2="58.42" y2="-45.72" width="0.1524" layer="91"/>
<junction x="58.42" y="-45.72"/>
<wire x1="58.42" y1="-48.26" x2="60.96" y2="-48.26" width="0.1524" layer="91"/>
<junction x="58.42" y="-48.26"/>
<pinref part="R16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CP2"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="19.05" y1="54.61" x2="5.08" y2="54.61" width="0.1524" layer="91"/>
<wire x1="5.08" y1="54.61" x2="5.08" y2="57.15" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CP1"/>
<wire x1="19.05" y1="57.15" x2="15.24" y2="57.15" width="0.1524" layer="91"/>
<wire x1="15.24" y1="57.15" x2="15.24" y2="67.31" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="15.24" y1="67.31" x2="5.08" y2="67.31" width="0.1524" layer="91"/>
<wire x1="5.08" y1="67.31" x2="5.08" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="I0"/>
<wire x1="54.61" y1="44.45" x2="57.15" y2="44.45" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="I1"/>
<wire x1="57.15" y1="44.45" x2="62.23" y2="44.45" width="0.1524" layer="91"/>
<wire x1="54.61" y1="46.99" x2="57.15" y2="46.99" width="0.1524" layer="91"/>
<wire x1="57.15" y1="46.99" x2="57.15" y2="44.45" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="I2"/>
<wire x1="54.61" y1="49.53" x2="57.15" y2="49.53" width="0.1524" layer="91"/>
<wire x1="57.15" y1="49.53" x2="57.15" y2="46.99" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="I3"/>
<wire x1="54.61" y1="52.07" x2="57.15" y2="52.07" width="0.1524" layer="91"/>
<wire x1="57.15" y1="52.07" x2="57.15" y2="49.53" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="I4"/>
<wire x1="54.61" y1="54.61" x2="57.15" y2="54.61" width="0.1524" layer="91"/>
<wire x1="57.15" y1="54.61" x2="57.15" y2="52.07" width="0.1524" layer="91"/>
<junction x="57.15" y="52.07"/>
<junction x="57.15" y="49.53"/>
<junction x="57.15" y="46.99"/>
<junction x="57.15" y="44.45"/>
<pinref part="R15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="SUPPLY" gate="G$1" pin="VCC"/>
<pinref part="C6" gate="G$1" pin="+"/>
<wire x1="-38.1" y1="59.69" x2="-38.1" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="45.72" x2="-38.1" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="48.26" x2="-15.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="45.72" x2="-15.24" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="46.99" x2="-26.67" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="45.72" x2="-26.67" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="45.72" x2="-26.67" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="45.72" x2="-38.1" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="45.72" x2="-10.16" y2="45.72" width="0.1524" layer="91"/>
<label x="-10.16" y="45.72" size="0.762" layer="95" xref="yes"/>
<junction x="-15.24" y="45.72"/>
<junction x="-26.67" y="45.72"/>
<junction x="-38.1" y="45.72"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VM@4"/>
<wire x1="19.05" y1="49.53" x2="1.27" y2="49.53" width="0.1524" layer="91"/>
<wire x1="1.27" y1="49.53" x2="1.27" y2="31.75" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VM@11"/>
<wire x1="1.27" y1="31.75" x2="19.05" y2="31.75" width="0.1524" layer="91"/>
<label x="1.27" y="31.75" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VM@4"/>
<pinref part="U2" gate="G$1" pin="VM@11"/>
<wire x1="17.78" y1="-60.96" x2="-2.54" y2="-60.96" width="0.1524" layer="91"/>
<label x="-2.54" y="-60.96" size="0.762" layer="95" ratio="6" rot="R180" xref="yes"/>
<wire x1="17.78" y1="-43.18" x2="-2.54" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-43.18" x2="-2.54" y2="-60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-41.91" x2="-20.32" y2="-44.45" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-44.45" x2="-20.32" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="-31.75" y1="-43.18" x2="-31.75" y2="-44.45" width="0.1524" layer="91"/>
<wire x1="-31.75" y1="-44.45" x2="-31.75" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-44.45" x2="-31.75" y2="-44.45" width="0.1524" layer="91"/>
<junction x="-31.75" y="-44.45"/>
<junction x="-20.32" y="-44.45"/>
<pinref part="C14" gate="G$1" pin="+"/>
<wire x1="-43.18" y1="-46.99" x2="-43.18" y2="-44.45" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-44.45" x2="-31.75" y2="-44.45" width="0.1524" layer="91"/>
<pinref part="SUPPLY24" gate="G$1" pin="VCC"/>
<wire x1="-43.18" y1="-31.75" x2="-43.18" y2="-44.45" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<junction x="-43.18" y="-44.45"/>
</segment>
<segment>
<pinref part="VM" gate="G$1" pin="1"/>
<wire x1="63.5" y1="-123.19" x2="53.34" y2="-123.19" width="0.1524" layer="91"/>
<label x="53.34" y="-123.19" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ALERT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="!ALERT!"/>
<wire x1="-26.67" y1="7.62" x2="-15.24" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="7.62" x2="-15.24" y2="8.89" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="7.62" x2="-12.7" y2="7.62" width="0.1524" layer="91"/>
<label x="-12.7" y="7.62" size="0.762" layer="95" xref="yes"/>
<junction x="-15.24" y="7.62"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$19"/>
<wire x1="-22.86" y1="-132.08" x2="-20.32" y2="-132.08" width="0.1524" layer="91"/>
<label x="-20.32" y="-132.08" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VS"/>
<wire x1="-57.15" y1="12.7" x2="-74.93" y2="12.7" width="0.1524" layer="91"/>
<label x="-77.47" y="12.7" size="0.762" layer="95" rot="R180" xref="yes"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-74.93" y1="12.7" x2="-77.47" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="19.05" x2="-15.24" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="20.32" x2="-74.93" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-74.93" y1="20.32" x2="-74.93" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-74.93" y1="5.08" x2="-74.93" y2="12.7" width="0.1524" layer="91"/>
<junction x="-74.93" y="12.7"/>
<pinref part="C15" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="-72.39" x2="-7.62" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-71.12" x2="-73.66" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-71.12" x2="-73.66" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VS"/>
<wire x1="-52.07" y1="-78.74" x2="-73.66" y2="-78.74" width="0.1524" layer="91"/>
<label x="-76.2" y="-78.74" size="0.762" layer="95" rot="R180" xref="yes"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-78.74" x2="-76.2" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-87.63" x2="-73.66" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-73.66" y="-78.74"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$1"/>
<wire x1="-78.74" y1="-129.54" x2="-76.2" y2="-129.54" width="0.1524" layer="91"/>
<label x="-76.2" y="-129.54" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="LIMIT"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-57.15" y1="7.62" x2="-64.77" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="7.62" x2="-64.77" y2="6.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ALERT2" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="!ALERT!"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="-21.59" y1="-83.82" x2="-7.62" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-83.82" x2="-7.62" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-83.82" x2="-5.08" y2="-83.82" width="0.1524" layer="91"/>
<junction x="-7.62" y="-83.82"/>
<label x="-5.08" y="-83.82" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$18"/>
<wire x1="-22.86" y1="-134.62" x2="-20.32" y2="-134.62" width="0.1524" layer="91"/>
<label x="-20.32" y="-134.62" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="LIMIT"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-52.07" y1="-83.82" x2="-63.5" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-83.82" x2="-63.5" y2="-85.09" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VO" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="OUT"/>
<wire x1="-57.15" y1="10.16" x2="-59.69" y2="10.16" width="0.1524" layer="91"/>
<label x="-59.69" y="10.16" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="25.4" y1="-123.19" x2="27.94" y2="-123.19" width="0.1524" layer="91"/>
<label x="27.94" y="-123.19" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="VO2" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT"/>
<wire x1="-52.07" y1="-81.28" x2="-54.61" y2="-81.28" width="0.1524" layer="91"/>
<label x="-54.61" y="-81.28" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="25.4" y1="-144.78" x2="27.94" y2="-144.78" width="0.1524" layer="91"/>
<label x="27.94" y="-144.78" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,19.05,49.53,U1,VM,VCC,,,"/>
<approved hash="104,1,19.05,31.75,U1,VM,VCC,,,"/>
<approved hash="202,1,54.61,34.29,U1,DECAY,,,,"/>
<approved hash="104,1,17.78,-43.18,U2,VM,VCC,,,"/>
<approved hash="104,1,17.78,-60.96,U2,VM,VCC,,,"/>
<approved hash="202,1,53.34,-58.42,U2,DECAY,,,,"/>
<approved hash="206,1,19.05,46.99,OUT1,,,,,"/>
<approved hash="206,1,19.05,34.29,OUT1,,,,,"/>
<approved hash="206,1,19.05,39.37,OUT2,,,,,"/>
<approved hash="206,1,19.05,41.91,OUT2,,,,,"/>
<approved hash="206,1,17.78,-45.72,OUT12,,,,,"/>
<approved hash="206,1,17.78,-58.42,OUT12,,,,,"/>
<approved hash="206,1,17.78,-50.8,OUT22,,,,,"/>
<approved hash="206,1,17.78,-53.34,OUT22,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
