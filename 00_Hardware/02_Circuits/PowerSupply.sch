<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.4.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="130bmp" color="2" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="2" fill="9" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DOCSMAL" urn="urn:adsk.eagle:symbol:13873/1" library_version="1">
<wire x1="88.9" y1="0" x2="88.9" y2="5.08" width="0.254" layer="94"/>
<wire x1="88.9" y1="5.08" x2="149.86" y2="5.08" width="0.254" layer="94"/>
<wire x1="149.86" y1="5.08" x2="149.86" y2="0" width="0.254" layer="94"/>
<wire x1="149.86" y1="5.08" x2="180.34" y2="5.08" width="0.254" layer="94"/>
<wire x1="88.9" y1="5.08" x2="88.9" y2="10.16" width="0.254" layer="94"/>
<wire x1="88.9" y1="10.16" x2="180.34" y2="10.16" width="0.254" layer="94"/>
<text x="90.17" y="6.35" size="2.54" layer="94">Date:</text>
<text x="101.6" y="6.35" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="151.13" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="165.1" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="90.17" y="1.27" size="2.54" layer="94">TITLE:</text>
<text x="106.68" y="1.27" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<frame x1="0" y1="0" x2="180.34" y2="264.16" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4-SMALL-DOCFIELD" urn="urn:adsk.eagle:component:13936/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, with small doc field</description>
<gates>
<gate name="/1" symbol="DOCSMAL" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power_supply">
<packages>
<package name="R6432">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.71" x2="2.387" y2="1.71" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.71" x2="2.387" y2="-1.71" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.325" y="0" dx="1" dy="3.65" layer="1" thermals="no"/>
<smd name="2" x="3.325" y="0" dx="1" dy="3.65" layer="1" thermals="no"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.325" y1="-1.775" x2="-2.275" y2="1.775" layer="51"/>
<rectangle x1="2.275" y1="-1.775" x2="3.325" y2="1.775" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<wire x1="-2.6" y1="1.7" x2="2.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="2.6" y1="1.7" x2="2.6" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2.6" y1="-1.7" x2="-2.6" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-1.7" x2="-2.6" y2="1.7" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-2.6" y="1.7"/>
<vertex x="-2.3" y="1.7"/>
<vertex x="-2.3" y="-1.7"/>
<vertex x="-2.6" y="-1.7"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="2.3" y="1.7"/>
<vertex x="2.6" y="1.7"/>
<vertex x="2.6" y="-1.7"/>
<vertex x="2.3" y="-1.7"/>
</polygon>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
<wire x1="0" y1="0.65" x2="0" y2="-0.65" width="0.127" layer="21"/>
</package>
<package name="JST-XH">
<pad name="3" x="1.25" y="0" drill="0.9" shape="square"/>
<pad name="4" x="3.75" y="0" drill="0.9" shape="square"/>
<pad name="2" x="-1.25" y="0" drill="0.9" shape="square"/>
<pad name="1" x="-3.75" y="0" drill="0.9" shape="square"/>
<wire x1="-6.25" y1="-2.3" x2="-6.25" y2="9.2" width="0.127" layer="21"/>
<wire x1="-6.25" y1="9.2" x2="6.25" y2="9.2" width="0.127" layer="21"/>
<wire x1="6.25" y1="9.2" x2="6.25" y2="-2.3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-2.3" x2="5.85" y2="-2.3" width="0.06" layer="51"/>
<wire x1="5.85" y1="-2.3" x2="5.85" y2="2.2" width="0.06" layer="51"/>
<wire x1="5.85" y1="2.2" x2="4.45" y2="2.2" width="0.06" layer="51"/>
<wire x1="-5.85" y1="2.2" x2="-5.85" y2="-2.3" width="0.06" layer="51"/>
<wire x1="-5.85" y1="-2.3" x2="-6.25" y2="-2.3" width="0.06" layer="51"/>
<wire x1="-6.25" y1="-2.3" x2="-6.25" y2="9.2" width="0.06" layer="51"/>
<wire x1="-6.25" y1="9.2" x2="6.25" y2="9.2" width="0.06" layer="51"/>
<wire x1="6.25" y1="9.2" x2="6.25" y2="-2.3" width="0.06" layer="51"/>
<wire x1="4.45" y1="2.2" x2="4.45" y2="2.85" width="0.06" layer="51"/>
<wire x1="4.45" y1="2.85" x2="3.05" y2="2.85" width="0.06" layer="51"/>
<wire x1="3.05" y1="2.85" x2="3.05" y2="2.2" width="0.06" layer="51"/>
<wire x1="1.95" y1="2.2" x2="1.95" y2="2.85" width="0.06" layer="51"/>
<wire x1="1.95" y1="2.85" x2="0.55" y2="2.85" width="0.06" layer="51"/>
<wire x1="0.55" y1="2.85" x2="0.55" y2="2.2" width="0.06" layer="51"/>
<wire x1="-0.55" y1="2.2" x2="-0.55" y2="2.85" width="0.06" layer="51"/>
<wire x1="-0.55" y1="2.85" x2="-1.95" y2="2.85" width="0.06" layer="51"/>
<wire x1="-1.95" y1="2.85" x2="-1.95" y2="2.2" width="0.06" layer="51"/>
<wire x1="-3.05" y1="2.2" x2="-3.05" y2="2.85" width="0.06" layer="51"/>
<wire x1="-3.05" y1="2.85" x2="-4.45" y2="2.85" width="0.06" layer="51"/>
<wire x1="-4.45" y1="2.85" x2="-4.45" y2="2.2" width="0.06" layer="51"/>
<wire x1="-5.85" y1="2.2" x2="-4.45" y2="2.2" width="0.06" layer="51"/>
<wire x1="3.05" y1="2.2" x2="1.95" y2="2.2" width="0.06" layer="51"/>
<wire x1="0.55" y1="2.2" x2="-0.55" y2="2.2" width="0.06" layer="51"/>
<wire x1="-3.05" y1="2.2" x2="-1.95" y2="2.2" width="0.06" layer="51"/>
<wire x1="-6.25" y1="-2.3" x2="-5.85" y2="-2.3" width="0.127" layer="21"/>
<wire x1="-5.85" y1="-2.3" x2="-5.85" y2="2.2" width="0.127" layer="21"/>
<wire x1="-5.85" y1="2.2" x2="-4.45" y2="2.2" width="0.127" layer="21"/>
<wire x1="-4.45" y1="2.2" x2="-4.45" y2="2.85" width="0.127" layer="21"/>
<wire x1="-4.45" y1="2.85" x2="-3.05" y2="2.85" width="0.127" layer="21"/>
<wire x1="-3.05" y1="2.85" x2="-3.05" y2="2.2" width="0.127" layer="21"/>
<wire x1="-3.05" y1="2.2" x2="-1.95" y2="2.2" width="0.127" layer="21"/>
<wire x1="-1.95" y1="2.2" x2="-1.95" y2="2.85" width="0.127" layer="21"/>
<wire x1="-1.95" y1="2.85" x2="-0.55" y2="2.85" width="0.127" layer="21"/>
<wire x1="-0.55" y1="2.85" x2="-0.55" y2="2.2" width="0.127" layer="21"/>
<wire x1="-0.55" y1="2.2" x2="0.55" y2="2.2" width="0.127" layer="21"/>
<wire x1="0.55" y1="2.2" x2="0.55" y2="2.85" width="0.127" layer="21"/>
<wire x1="0.55" y1="2.85" x2="1.95" y2="2.85" width="0.127" layer="21"/>
<wire x1="1.95" y1="2.85" x2="1.95" y2="2.2" width="0.127" layer="21"/>
<wire x1="1.95" y1="2.2" x2="3.05" y2="2.2" width="0.127" layer="21"/>
<wire x1="3.05" y1="2.2" x2="3.05" y2="2.85" width="0.127" layer="21"/>
<wire x1="3.05" y1="2.85" x2="4.45" y2="2.85" width="0.127" layer="21"/>
<wire x1="4.45" y1="2.85" x2="4.45" y2="2.2" width="0.127" layer="21"/>
<wire x1="4.45" y1="2.2" x2="5.85" y2="2.2" width="0.127" layer="21"/>
<wire x1="5.85" y1="2.2" x2="5.85" y2="-2.3" width="0.127" layer="21"/>
<wire x1="5.85" y1="-2.3" x2="6.25" y2="-2.3" width="0.127" layer="21"/>
</package>
<package name="D_R-PDSO-G14">
<description>&lt;b&gt;PLASTIC SMALL-OUTLINE PACKAGE SO 14&lt;/b&gt; JEDEC MS-012, D Type&lt;p&gt;
Source: www.ti.com/.. slvs087l.pdf</description>
<wire x1="4.375" y1="1.95" x2="4.375" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-4.375" y1="-1.95" x2="-4.375" y2="1.95" width="0.2032" layer="21"/>
<circle x="-3.53" y="-1.3" radius="0.325" width="0.2032" layer="21"/>
<smd name="1" x="-3.81" y="-2.65" dx="0.6" dy="1.5" layer="1"/>
<text x="-4.445" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.01" y1="-3.05" x2="-3.61" y2="-1.98" layer="51"/>
<wire x1="-4.375" y1="1.95" x2="-4.25" y2="1.95" width="0.2032" layer="21"/>
<wire x1="4.375" y1="1.95" x2="4.25" y2="1.95" width="0.2032" layer="21"/>
<wire x1="4.375" y1="-1.95" x2="4.25" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-4.375" y1="-1.95" x2="-4.25" y2="-1.95" width="0.2032" layer="21"/>
<smd name="2" x="-2.54" y="-2.65" dx="0.6" dy="1.5" layer="1"/>
<smd name="3" x="-1.27" y="-2.65" dx="0.6" dy="1.5" layer="1"/>
<smd name="4" x="0" y="-2.65" dx="0.6" dy="1.5" layer="1"/>
<smd name="5" x="1.27" y="-2.65" dx="0.6" dy="1.5" layer="1"/>
<smd name="6" x="2.54" y="-2.65" dx="0.6" dy="1.5" layer="1"/>
<smd name="7" x="3.81" y="-2.65" dx="0.6" dy="1.5" layer="1"/>
<smd name="8" x="3.81" y="2.65" dx="0.6" dy="1.5" layer="1" rot="R180"/>
<smd name="9" x="2.54" y="2.65" dx="0.6" dy="1.5" layer="1" rot="R180"/>
<smd name="10" x="1.27" y="2.65" dx="0.6" dy="1.5" layer="1" rot="R180"/>
<smd name="11" x="0" y="2.65" dx="0.6" dy="1.5" layer="1" rot="R180"/>
<smd name="12" x="-1.27" y="2.65" dx="0.6" dy="1.5" layer="1" rot="R180"/>
<smd name="13" x="-2.54" y="2.65" dx="0.6" dy="1.5" layer="1" rot="R180"/>
<smd name="14" x="-3.81" y="2.65" dx="0.6" dy="1.5" layer="1" rot="R180"/>
<wire x1="-4.375" y1="-1.95" x2="4.375" y2="-1.95" width="0.127" layer="51"/>
<wire x1="4.375" y1="-1.95" x2="4.375" y2="1.95" width="0.127" layer="51"/>
<wire x1="4.375" y1="1.95" x2="-4.375" y2="1.95" width="0.127" layer="51"/>
<wire x1="-4.375" y1="1.95" x2="-4.375" y2="-1.95" width="0.127" layer="51"/>
<circle x="-3.53" y="-1.3" radius="0.325" width="0.127" layer="51"/>
<rectangle x1="-2.74" y1="-3.05" x2="-2.34" y2="-1.98" layer="51"/>
<rectangle x1="-1.47" y1="-3.05" x2="-1.07" y2="-1.98" layer="51"/>
<rectangle x1="-0.2" y1="-3.05" x2="0.2" y2="-1.98" layer="51"/>
<rectangle x1="1.07" y1="-3.05" x2="1.47" y2="-1.98" layer="51"/>
<rectangle x1="2.34" y1="-3.05" x2="2.74" y2="-1.98" layer="51"/>
<rectangle x1="3.61" y1="-3.05" x2="4.01" y2="-1.98" layer="51"/>
<rectangle x1="3.61" y1="1.98" x2="4.01" y2="3.05" layer="51" rot="R180"/>
<rectangle x1="2.34" y1="1.98" x2="2.74" y2="3.05" layer="51" rot="R180"/>
<rectangle x1="1.07" y1="1.98" x2="1.47" y2="3.05" layer="51" rot="R180"/>
<rectangle x1="-0.2" y1="1.98" x2="0.2" y2="3.05" layer="51" rot="R180"/>
<rectangle x1="-1.47" y1="1.98" x2="-1.07" y2="3.05" layer="51" rot="R180"/>
<rectangle x1="-2.74" y1="1.98" x2="-2.34" y2="3.05" layer="51" rot="R180"/>
<rectangle x1="-4.01" y1="1.98" x2="-3.61" y2="3.05" layer="51" rot="R180"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.4" y="0.8"/>
<vertex x="0.4" y="0.8"/>
<vertex x="0.4" y="-0.8"/>
<vertex x="-0.4" y="-0.8"/>
</polygon>
</package>
<package name="VQFN(20)">
<smd name="PGND" x="0" y="0" dx="2.55" dy="1.55" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6" x="0.25" y="-1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R90" stop="no" thermals="no" cream="no"/>
<rectangle x1="0.04" y1="-2.22" x2="0.46" y2="-1.44" layer="29"/>
<circle x="0.25" y="-1.44" radius="0.21" width="0" layer="29"/>
<rectangle x1="-1.525" y1="-1.025" x2="1.525" y2="1.025" layer="29"/>
<smd name="5" x="-0.25" y="-1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="4" x="-0.75" y="-1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="3" x="-1.25" y="-1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="2" x="-1.75" y="-1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="7" x="0.75" y="-1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="8" x="1.25" y="-1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="9" x="1.75" y="-1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="15" x="0.25" y="1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="16" x="-0.25" y="1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="17" x="-0.75" y="1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="18" x="-1.25" y="1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="19" x="-1.75" y="1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="14" x="0.75" y="1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="13" x="1.25" y="1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="12" x="1.75" y="1.725" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R270" stop="no" thermals="no" cream="no"/>
<circle x="-0.25" y="-1.44" radius="0.21" width="0" layer="29"/>
<circle x="-0.75" y="-1.44" radius="0.21" width="0" layer="29"/>
<circle x="-1.25" y="-1.44" radius="0.21" width="0" layer="29"/>
<circle x="-1.75" y="-1.44" radius="0.21" width="0" layer="29"/>
<circle x="0.75" y="-1.44" radius="0.21" width="0" layer="29"/>
<circle x="1.25" y="-1.44" radius="0.21" width="0" layer="29"/>
<circle x="1.75" y="-1.44" radius="0.21" width="0" layer="29"/>
<circle x="1.75" y="1.44" radius="0.21" width="0" layer="29"/>
<circle x="1.25" y="1.44" radius="0.21" width="0" layer="29"/>
<circle x="0.75" y="1.44" radius="0.21" width="0" layer="29"/>
<circle x="0.25" y="1.44" radius="0.21" width="0" layer="29"/>
<circle x="-0.25" y="1.44" radius="0.21" width="0" layer="29"/>
<circle x="-0.75" y="1.44" radius="0.21" width="0" layer="29"/>
<circle x="-1.25" y="1.44" radius="0.21" width="0" layer="29"/>
<circle x="-1.75" y="1.44" radius="0.21" width="0" layer="29"/>
<rectangle x1="-0.46" y1="-2.22" x2="-0.04" y2="-1.44" layer="29"/>
<rectangle x1="-0.96" y1="-2.22" x2="-0.54" y2="-1.44" layer="29"/>
<rectangle x1="-1.46" y1="-2.22" x2="-1.04" y2="-1.44" layer="29"/>
<rectangle x1="-1.96" y1="-2.22" x2="-1.54" y2="-1.44" layer="29"/>
<rectangle x1="0.54" y1="-2.22" x2="0.96" y2="-1.44" layer="29"/>
<rectangle x1="1.04" y1="-2.22" x2="1.46" y2="-1.44" layer="29"/>
<rectangle x1="1.54" y1="-2.22" x2="1.96" y2="-1.44" layer="29"/>
<rectangle x1="1.54" y1="1.44" x2="1.96" y2="2.22" layer="29" rot="R180"/>
<rectangle x1="1.04" y1="1.44" x2="1.46" y2="2.22" layer="29" rot="R180"/>
<rectangle x1="0.54" y1="1.44" x2="0.96" y2="2.22" layer="29" rot="R180"/>
<rectangle x1="0.04" y1="1.44" x2="0.46" y2="2.22" layer="29" rot="R180"/>
<rectangle x1="-0.46" y1="1.44" x2="-0.04" y2="2.22" layer="29" rot="R180"/>
<rectangle x1="-0.96" y1="1.44" x2="-0.54" y2="2.22" layer="29" rot="R180"/>
<rectangle x1="-1.46" y1="1.44" x2="-1.04" y2="2.22" layer="29" rot="R180"/>
<rectangle x1="-1.96" y1="1.44" x2="-1.54" y2="2.22" layer="29" rot="R180"/>
<smd name="20" x="-2.225" y="0.75" dx="0.85" dy="0.28" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="1" x="-2.225" y="-0.75" dx="0.85" dy="0.28" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="10" x="2.225" y="-0.75" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R180" stop="no" thermals="no" cream="no"/>
<smd name="11" x="2.225" y="0.75" dx="0.85" dy="0.28" layer="1" roundness="100" rot="R180" stop="no" thermals="no" cream="no"/>
<circle x="1.94" y="-0.75" radius="0.21" width="0" layer="29"/>
<circle x="1.94" y="0.75" radius="0.21" width="0" layer="29"/>
<circle x="-1.94" y="0.75" radius="0.21" width="0" layer="29"/>
<circle x="-1.94" y="-0.75" radius="0.21" width="0" layer="29"/>
<rectangle x1="-2.54" y1="-1.14" x2="-2.12" y2="-0.36" layer="29" rot="R270"/>
<rectangle x1="-2.54" y1="0.36" x2="-2.12" y2="1.14" layer="29" rot="R270"/>
<rectangle x1="2.12" y1="0.36" x2="2.54" y2="1.14" layer="29" rot="R90"/>
<rectangle x1="2.12" y1="-1.14" x2="2.54" y2="-0.36" layer="29" rot="R90"/>
<rectangle x1="1.52" y1="0.16" x2="2.65" y2="0.39" layer="29"/>
<rectangle x1="1.52" y1="-0.16" x2="1.925" y2="0.16" layer="29"/>
<rectangle x1="1.52" y1="-0.39" x2="2.65" y2="-0.16" layer="29"/>
<rectangle x1="-2.65" y1="0.16" x2="-1.52" y2="0.39" layer="29" rot="R180"/>
<rectangle x1="-2.65" y1="-0.39" x2="-1.52" y2="-0.16" layer="29" rot="R180"/>
<pad name="P$22" x="0" y="-0.5" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$23" x="-1" y="-0.5" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$24" x="1" y="-0.5" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$25" x="1" y="0.5" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$26" x="0" y="0.5" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$27" x="-1" y="0.5" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<rectangle x1="0.15" y1="0.15" x2="1.47" y2="0.97" layer="31"/>
<rectangle x1="0.15" y1="0.15" x2="1.47" y2="0.97" layer="31"/>
<rectangle x1="0.15" y1="-0.97" x2="1.47" y2="-0.15" layer="31"/>
<rectangle x1="-1.47" y1="0.15" x2="-0.15" y2="0.97" layer="31"/>
<rectangle x1="-1.47" y1="-0.97" x2="-0.15" y2="-0.15" layer="31"/>
<rectangle x1="-2.65" y1="-0.39" x2="-1.75" y2="-0.16" layer="31"/>
<rectangle x1="-2.65" y1="0.16" x2="-1.75" y2="0.39" layer="31"/>
<rectangle x1="1.75" y1="0.16" x2="2.65" y2="0.39" layer="31" rot="R180"/>
<rectangle x1="1.75" y1="-0.39" x2="2.65" y2="-0.16" layer="31" rot="R180"/>
<rectangle x1="0.135" y1="-2.125" x2="0.365" y2="-1.44" layer="31"/>
<circle x="0.25" y="-1.44" radius="0.115" width="0" layer="31"/>
<rectangle x1="-0.365" y1="-2.125" x2="-0.135" y2="-1.44" layer="31"/>
<rectangle x1="-0.865" y1="-2.125" x2="-0.635" y2="-1.44" layer="31"/>
<rectangle x1="-1.365" y1="-2.125" x2="-1.135" y2="-1.44" layer="31"/>
<rectangle x1="-1.865" y1="-2.125" x2="-1.635" y2="-1.44" layer="31"/>
<rectangle x1="0.635" y1="-2.125" x2="0.865" y2="-1.44" layer="31"/>
<rectangle x1="1.135" y1="-2.125" x2="1.365" y2="-1.44" layer="31"/>
<rectangle x1="1.635" y1="-2.125" x2="1.865" y2="-1.44" layer="31"/>
<circle x="-0.25" y="-1.44" radius="0.115" width="0" layer="31"/>
<circle x="-0.75" y="-1.44" radius="0.115" width="0" layer="31"/>
<circle x="-1.25" y="-1.44" radius="0.115" width="0" layer="31"/>
<circle x="-1.75" y="-1.44" radius="0.115" width="0" layer="31"/>
<circle x="0.75" y="-1.44" radius="0.115" width="0" layer="31"/>
<circle x="1.25" y="-1.44" radius="0.115" width="0" layer="31"/>
<circle x="1.75" y="-1.44" radius="0.115" width="0" layer="31"/>
<rectangle x1="1.635" y1="1.44" x2="1.865" y2="2.125" layer="31" rot="R180"/>
<circle x="1.75" y="1.44" radius="0.115" width="0" layer="31"/>
<rectangle x1="1.135" y1="1.44" x2="1.365" y2="2.125" layer="31" rot="R180"/>
<rectangle x1="0.635" y1="1.44" x2="0.865" y2="2.125" layer="31" rot="R180"/>
<rectangle x1="0.135" y1="1.44" x2="0.365" y2="2.125" layer="31" rot="R180"/>
<rectangle x1="-0.365" y1="1.44" x2="-0.135" y2="2.125" layer="31" rot="R180"/>
<rectangle x1="-0.865" y1="1.44" x2="-0.635" y2="2.125" layer="31" rot="R180"/>
<rectangle x1="-1.365" y1="1.44" x2="-1.135" y2="2.125" layer="31" rot="R180"/>
<rectangle x1="-1.865" y1="1.44" x2="-1.635" y2="2.125" layer="31" rot="R180"/>
<circle x="1.25" y="1.44" radius="0.115" width="0" layer="31"/>
<circle x="0.75" y="1.44" radius="0.115" width="0" layer="31"/>
<circle x="0.25" y="1.44" radius="0.115" width="0" layer="31"/>
<circle x="-0.25" y="1.44" radius="0.115" width="0" layer="31"/>
<circle x="-0.75" y="1.44" radius="0.115" width="0" layer="31"/>
<circle x="-1.25" y="1.44" radius="0.115" width="0" layer="31"/>
<circle x="-1.75" y="1.44" radius="0.115" width="0" layer="31"/>
<rectangle x1="-2.3975" y1="0.4075" x2="-2.1675" y2="1.0925" layer="31" rot="R270"/>
<rectangle x1="-2.3975" y1="-1.0925" x2="-2.1675" y2="-0.4075" layer="31" rot="R270"/>
<circle x="-1.94" y="0.75" radius="0.115" width="0" layer="31"/>
<circle x="-1.94" y="-0.75" radius="0.115" width="0" layer="31"/>
<rectangle x1="2.1675" y1="-1.0925" x2="2.3975" y2="-0.4075" layer="31" rot="R90"/>
<rectangle x1="2.1675" y1="0.4075" x2="2.3975" y2="1.0925" layer="31" rot="R90"/>
<circle x="1.94" y="0.75" radius="0.115" width="0" layer="31"/>
<circle x="1.94" y="-0.75" radius="0.115" width="0" layer="31"/>
<wire x1="2.05" y1="1.75" x2="2.25" y2="1.75" width="0.127" layer="21"/>
<wire x1="2.25" y1="1.75" x2="2.25" y2="1.05" width="0.127" layer="21"/>
<wire x1="2.25" y1="-1.05" x2="2.25" y2="-1.75" width="0.127" layer="21"/>
<wire x1="2.25" y1="-1.75" x2="2.05" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-2.05" y1="-1.75" x2="-2.25" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-1.75" x2="-2.25" y2="-1.05" width="0.127" layer="21"/>
<wire x1="-2.25" y1="1.05" x2="-2.25" y2="1.75" width="0.127" layer="21"/>
<wire x1="-2.25" y1="1.75" x2="-2.05" y2="1.75" width="0.127" layer="21"/>
<circle x="-2.05" y="-1.15" radius="0.111803125" width="0" layer="21"/>
<wire x1="-2.25" y1="1.75" x2="-2.25" y2="-1.75" width="0.1" layer="51"/>
<wire x1="-2.25" y1="-1.75" x2="2.25" y2="-1.75" width="0.1" layer="51"/>
<wire x1="2.25" y1="-1.75" x2="2.25" y2="1.75" width="0.1" layer="51"/>
<wire x1="2.25" y1="1.75" x2="-2.25" y2="1.75" width="0.1" layer="51"/>
<rectangle x1="-2.25" y1="-1.75" x2="0" y2="0" layer="51"/>
<wire x1="-2.8" y1="2.3" x2="2.8" y2="2.3" width="0.127" layer="39"/>
<wire x1="2.8" y1="2.3" x2="2.8" y2="-2.3" width="0.127" layer="39"/>
<wire x1="2.8" y1="-2.3" x2="-2.8" y2="-2.3" width="0.127" layer="39"/>
<wire x1="-2.8" y1="-2.3" x2="-2.8" y2="2.3" width="0.127" layer="39"/>
<rectangle x1="-1.89" y1="1.44" x2="-1.61" y2="1.76" layer="51"/>
<rectangle x1="-1.39" y1="1.44" x2="-1.11" y2="1.76" layer="51"/>
<rectangle x1="-0.89" y1="1.44" x2="-0.61" y2="1.76" layer="51"/>
<rectangle x1="-0.39" y1="1.44" x2="-0.11" y2="1.76" layer="51"/>
<rectangle x1="0.11" y1="1.44" x2="0.39" y2="1.76" layer="51"/>
<rectangle x1="0.61" y1="1.44" x2="0.89" y2="1.76" layer="51"/>
<rectangle x1="1.11" y1="1.44" x2="1.39" y2="1.76" layer="51"/>
<rectangle x1="1.61" y1="1.44" x2="1.89" y2="1.76" layer="51"/>
<rectangle x1="1.96" y1="0.59" x2="2.24" y2="0.91" layer="51" rot="R270"/>
<rectangle x1="1.96" y1="-0.91" x2="2.24" y2="-0.59" layer="51" rot="R270"/>
<rectangle x1="1.61" y1="-1.76" x2="1.89" y2="-1.44" layer="51" rot="R180"/>
<rectangle x1="1.11" y1="-1.76" x2="1.39" y2="-1.44" layer="51" rot="R180"/>
<rectangle x1="0.61" y1="-1.76" x2="0.89" y2="-1.44" layer="51" rot="R180"/>
<rectangle x1="0.11" y1="-1.76" x2="0.39" y2="-1.44" layer="51" rot="R180"/>
<rectangle x1="-2.24" y1="0.59" x2="-1.96" y2="0.91" layer="51" rot="R90"/>
<polygon width="0" layer="1">
<vertex x="-1.525" y="0.39"/>
<vertex x="-1.525" y="-0.39"/>
<vertex x="-1.525" y="-1.025"/>
</polygon>
<text x="-2.9" y="-1.5" size="0.5" layer="25" rot="R90">&gt;NAME</text>
<text x="3.4" y="-1.6" size="0.5" layer="27" rot="R90">&gt;VALUE</text>
<polygon width="0.21" layer="1">
<vertex x="-2.545" y="0.285"/>
<vertex x="-1.42" y="0.285"/>
<vertex x="-1.42" y="0.92"/>
<vertex x="1.42" y="0.92"/>
<vertex x="1.42" y="0.285"/>
<vertex x="2.545" y="0.285"/>
<vertex x="2.545" y="0.265"/>
<vertex x="1.82" y="0.265"/>
<vertex x="1.82" y="-0.265"/>
<vertex x="2.545" y="-0.265"/>
<vertex x="2.545" y="-0.285"/>
<vertex x="1.42" y="-0.285"/>
<vertex x="1.42" y="-0.92"/>
<vertex x="-1.42" y="-0.92"/>
<vertex x="-1.42" y="-0.285"/>
<vertex x="-2.545" y="-0.285"/>
<vertex x="-2.545" y="-0.265"/>
<vertex x="-1.82" y="-0.265"/>
<vertex x="-1.82" y="0.265"/>
<vertex x="-2.545" y="0.265"/>
</polygon>
<rectangle x1="-1.925" y1="-0.2" x2="-1.5" y2="0.2" layer="29"/>
</package>
<package name="VQFN-16">
<description>&lt;b&gt;RTE (S-PWQFN-N16)&lt;/b&gt; 3x3 mm&lt;p&gt;
Source: http://www.ti.com/lit/ds/symlink/tps43061.pdf</description>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.025" width="0.1016" layer="21"/>
<wire x1="1.025" y1="1.5" x2="1.5" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.05" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="1.05" x2="-1.5" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.025" width="0.1016" layer="21"/>
<wire x1="-1.025" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.025" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="1.5" y1="-1.025" x2="1.5" y2="-1.5" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="0.15" width="0.01" layer="49"/>
<smd name="17" x="0" y="0" dx="1.7" dy="1.7" layer="1" roundness="10" stop="no" thermals="no" cream="no"/>
<smd name="1" x="-1.4" y="0.75" dx="0.6" dy="0.25" layer="1" roundness="40" stop="no" thermals="no"/>
<smd name="2" x="-1.4" y="0.25" dx="0.6" dy="0.25" layer="1" roundness="40" stop="no" thermals="no"/>
<smd name="3" x="-1.4" y="-0.25" dx="0.6" dy="0.25" layer="1" roundness="40" stop="no" thermals="no"/>
<smd name="4" x="-1.4" y="-0.75" dx="0.6" dy="0.25" layer="1" roundness="40" stop="no" thermals="no"/>
<smd name="5" x="-0.75" y="-1.4" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R90" stop="no" thermals="no"/>
<smd name="6" x="-0.25" y="-1.4" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R90" stop="no" thermals="no"/>
<smd name="7" x="0.25" y="-1.4" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R90" stop="no" thermals="no"/>
<smd name="8" x="0.75" y="-1.4" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R90" stop="no" thermals="no"/>
<smd name="9" x="1.4" y="-0.75" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R180" stop="no" thermals="no"/>
<smd name="10" x="1.4" y="-0.25" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R180" stop="no" thermals="no"/>
<smd name="11" x="1.4" y="0.25" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R180" stop="no" thermals="no"/>
<smd name="12" x="1.4" y="0.75" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R180" stop="no" thermals="no"/>
<smd name="13" x="0.75" y="1.4" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R270" stop="no" thermals="no"/>
<smd name="14" x="0.25" y="1.4" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R270" stop="no" thermals="no"/>
<smd name="15" x="-0.25" y="1.4" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R270" stop="no" thermals="no"/>
<smd name="16" x="-0.75" y="1.4" dx="0.6" dy="0.25" layer="1" roundness="40" rot="R270" stop="no" thermals="no"/>
<text x="-2" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.5" y1="1" x2="-1" y2="1.5" layer="21"/>
<rectangle x1="-1.5" y1="0" x2="0" y2="1.5" layer="51"/>
<rectangle x1="1.1" y1="0.625" x2="1.5" y2="0.875" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.127" layer="39"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.127" layer="39"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.127" layer="39"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.127" layer="39"/>
<pad name="P$1" x="0" y="0" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$2" x="0.6" y="-0.6" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$3" x="0.6" y="0.6" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$4" x="-0.6" y="0.6" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<pad name="P$5" x="-0.6" y="-0.6" drill="0.2" diameter="0.4064" stop="no" thermals="no"/>
<rectangle x1="1.1" y1="0.125" x2="1.5" y2="0.375" layer="51"/>
<rectangle x1="1.1" y1="-0.375" x2="1.5" y2="-0.125" layer="51"/>
<rectangle x1="1.1" y1="-0.875" x2="1.5" y2="-0.625" layer="51"/>
<rectangle x1="0.55" y1="-1.425" x2="0.95" y2="-1.175" layer="51" rot="R270"/>
<rectangle x1="0.05" y1="-1.425" x2="0.45" y2="-1.175" layer="51" rot="R270"/>
<rectangle x1="-0.45" y1="-1.425" x2="-0.05" y2="-1.175" layer="51" rot="R270"/>
<rectangle x1="-0.95" y1="-1.425" x2="-0.55" y2="-1.175" layer="51" rot="R270"/>
<rectangle x1="-1.5" y1="-0.875" x2="-1.1" y2="-0.625" layer="51" rot="R180"/>
<rectangle x1="-1.5" y1="-0.375" x2="-1.1" y2="-0.125" layer="51" rot="R180"/>
<rectangle x1="0.55" y1="1.175" x2="0.95" y2="1.425" layer="51" rot="R90"/>
<rectangle x1="0.05" y1="1.175" x2="0.45" y2="1.425" layer="51" rot="R90"/>
<rectangle x1="-0.445" y1="-1.77" x2="-0.055" y2="-1.03" layer="29"/>
<rectangle x1="0.055" y1="-1.77" x2="0.445" y2="-1.03" layer="29"/>
<rectangle x1="0.555" y1="-1.77" x2="0.945" y2="-1.03" layer="29"/>
<rectangle x1="-0.945" y1="-1.77" x2="-0.555" y2="-1.03" layer="29"/>
<rectangle x1="1.205" y1="-1.12" x2="1.595" y2="-0.38" layer="29" rot="R90"/>
<rectangle x1="1.205" y1="-0.62" x2="1.595" y2="0.12" layer="29" rot="R90"/>
<rectangle x1="1.205" y1="-0.12" x2="1.595" y2="0.62" layer="29" rot="R90"/>
<rectangle x1="1.205" y1="0.38" x2="1.595" y2="1.12" layer="29" rot="R90"/>
<rectangle x1="0.555" y1="1.03" x2="0.945" y2="1.77" layer="29" rot="R180"/>
<rectangle x1="0.055" y1="1.03" x2="0.445" y2="1.77" layer="29" rot="R180"/>
<rectangle x1="-0.445" y1="1.03" x2="-0.055" y2="1.77" layer="29" rot="R180"/>
<rectangle x1="-0.945" y1="1.03" x2="-0.555" y2="1.77" layer="29" rot="R180"/>
<rectangle x1="-1.595" y1="0.38" x2="-1.205" y2="1.12" layer="29" rot="R270"/>
<rectangle x1="-1.595" y1="-0.12" x2="-1.205" y2="0.62" layer="29" rot="R270"/>
<rectangle x1="-1.595" y1="-0.62" x2="-1.205" y2="0.12" layer="29" rot="R270"/>
<rectangle x1="-1.595" y1="-1.12" x2="-1.205" y2="-0.38" layer="29" rot="R270"/>
<rectangle x1="-0.775" y1="-0.775" x2="0.775" y2="0.775" layer="31"/>
<rectangle x1="-0.85" y1="-0.85" x2="0.85" y2="0.85" layer="29"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.073" y1="0.583" x2="1.073" y2="0.583" width="0.0508" layer="39"/>
<wire x1="1.073" y1="0.583" x2="1.073" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="1.073" y1="-0.583" x2="-1.073" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="-1.073" y1="-0.583" x2="-1.073" y2="0.583" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1" thermals="no"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1" thermals="no"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.1" y="0.25"/>
<vertex x="0.1" y="0.25"/>
<vertex x="0.1" y="-0.25"/>
<vertex x="-0.1" y="-0.25"/>
</polygon>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.683" x2="1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.683" x2="1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.683" x2="-1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.683" x2="-1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.1" y="0.432"/>
<vertex x="0.1" y="0.432"/>
<vertex x="0.1" y="-0.4"/>
<vertex x="-0.1" y="-0.4"/>
</polygon>
</package>
<package name="C0603">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1" stop="no" thermals="no" cream="no"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
<rectangle x1="-0.425" y1="-0.225" x2="-0.075" y2="0.225" layer="29"/>
<rectangle x1="0.075" y1="-0.225" x2="0.425" y2="0.225" layer="29"/>
<rectangle x1="-0.425" y1="-0.225" x2="-0.075" y2="0.225" layer="31"/>
<rectangle x1="0.075" y1="-0.225" x2="0.425" y2="0.225" layer="31"/>
<wire x1="-0.5" y1="-0.3" x2="-0.5" y2="0.3" width="0.05" layer="39"/>
<wire x1="-0.5" y1="0.3" x2="0.5" y2="0.3" width="0.05" layer="39"/>
<wire x1="0.5" y1="0.3" x2="0.5" y2="-0.3" width="0.05" layer="39"/>
<wire x1="0.5" y1="-0.3" x2="-0.5" y2="-0.3" width="0.05" layer="39"/>
<wire x1="0" y1="0.13" x2="0" y2="-0.13" width="0.127" layer="21"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1" thermals="no"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1" thermals="no"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.4" y="1.25"/>
<vertex x="0.4" y="1.25"/>
<vertex x="0.4" y="-1.25"/>
<vertex x="-0.4" y="-1.25"/>
</polygon>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
<wire x1="0" y1="0.65" x2="0" y2="-0.65" width="0.127" layer="21"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.682" y1="-0.331" x2="0.682" y2="-0.331" width="0.1524" layer="51"/>
<wire x1="0.682" y1="0.331" x2="-0.632" y2="0.331" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1" thermals="no"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1" thermals="no"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.55" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.55" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.15" y="0.6"/>
<vertex x="0.15" y="0.6"/>
<vertex x="0.15" y="-0.6"/>
<vertex x="-0.15" y="-0.6"/>
</polygon>
</package>
<package name="SRP1270">
<smd name="1" x="-5.5" y="0" dx="4" dy="4.5" layer="1" thermals="no"/>
<smd name="2" x="5.5" y="0" dx="4" dy="4.5" layer="1" rot="R180" thermals="no"/>
<polygon width="0.127" layer="21">
<vertex x="6.5" y="6"/>
<vertex x="6.5" y="1.75"/>
<vertex x="6.5" y="-1.75"/>
<vertex x="6.5" y="-6.5"/>
</polygon>
<polygon width="0.0635" layer="51">
<vertex x="6.5" y="1.75"/>
<vertex x="5.5" y="1.75"/>
<vertex x="5.5" y="-1.75"/>
<vertex x="6.95" y="-1.75"/>
<vertex x="6.95" y="6" curve="90"/>
<vertex x="6" y="7"/>
<vertex x="6" y="6.5"/>
<vertex x="6.5" y="6"/>
</polygon>
<polygon width="0.0635" layer="51">
<vertex x="-6.5" y="-1.75"/>
<vertex x="-5.5" y="-1.75"/>
<vertex x="-5.5" y="1.75"/>
<vertex x="-6.95" y="1.75"/>
<vertex x="-6.95" y="-6" curve="90"/>
<vertex x="-6" y="-7"/>
<vertex x="-6" y="-6.5"/>
<vertex x="-6.5" y="-6"/>
</polygon>
<text x="-2.25" y="-1" size="1.778" layer="51">1R2</text>
<text x="-6.5" y="6.75" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.75" y="-8.25" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.7" y1="-7.1" x2="-7.7" y2="7.1" width="0.127" layer="39"/>
<wire x1="-7.7" y1="7.1" x2="7.7" y2="7.1" width="0.127" layer="39"/>
<wire x1="7.7" y1="7.1" x2="7.7" y2="-7.1" width="0.127" layer="39"/>
<wire x1="7.7" y1="-7.1" x2="-7.7" y2="-7.1" width="0.127" layer="39"/>
<wire x1="-6.5" y1="2.4" x2="-6.5" y2="6.5" width="0.1778" layer="21"/>
<wire x1="-6.5" y1="6.5" x2="6" y2="6.5" width="0.1778" layer="21"/>
<wire x1="6" y1="6.5" x2="6.5" y2="6" width="0.1778" layer="21"/>
<wire x1="6.5" y1="6" x2="6.5" y2="2.4" width="0.1778" layer="21"/>
<wire x1="6.5" y1="-2.5" x2="6.5" y2="-6.5" width="0.1778" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="-6" y2="-6.5" width="0.1778" layer="21"/>
<wire x1="-6" y1="-6.5" x2="-6.5" y2="-6" width="0.1778" layer="21"/>
<wire x1="-6.5" y1="-6" x2="-6.5" y2="-2.5" width="0.1778" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="6.5" y2="6" width="0.14" layer="51"/>
<wire x1="6.5" y1="6" x2="6" y2="6.5" width="0.14" layer="51"/>
<wire x1="6" y1="6.5" x2="-6.5" y2="6.5" width="0.14" layer="51"/>
<wire x1="-6.5" y1="6.5" x2="-6.5" y2="-6" width="0.14" layer="51"/>
<wire x1="-6.5" y1="-6" x2="-6" y2="-6.5" width="0.14" layer="51"/>
<wire x1="-6" y1="-6.5" x2="6.5" y2="-6.5" width="0.14" layer="51"/>
</package>
<package name="PA4332">
<smd name="P$1" x="0" y="-1.5" dx="3.7" dy="1.1" layer="1" thermals="no"/>
<smd name="P$2" x="0" y="1.5" dx="3.7" dy="1.1" layer="1" thermals="no"/>
<wire x1="-1.6" y1="-2.1" x2="-2.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-2.1" y1="-1.6" x2="-2.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-2.1" y1="1.6" x2="-1.6" y2="2.1" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="2.1" x2="1.6" y2="2.1" width="0.1016" layer="51"/>
<wire x1="1.6" y1="2.1" x2="2.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="2.1" y1="1.6" x2="2.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="2.1" y1="-1.6" x2="1.6" y2="-2.1" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-2.1" x2="-1.6" y2="-2.1" width="0.1016" layer="51"/>
<wire x1="-2.1" y1="-1.6" x2="-2.1" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.6" x2="-2" y2="1.7" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.6" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2" y1="1.7" x2="2.1" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.6" x2="2.1" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.6" x2="2" y2="-1.7" width="0.127" layer="21"/>
<polygon width="0.1" layer="51">
<vertex x="-1.5" y="1.7"/>
<vertex x="-1.5" y="1.2" curve="90"/>
<vertex x="-1.4" y="1.1"/>
<vertex x="1.4" y="1.1" curve="90"/>
<vertex x="1.5" y="1.2"/>
<vertex x="1.5" y="1.85" curve="90"/>
<vertex x="1.4" y="1.95"/>
<vertex x="-1.4" y="1.95" curve="90"/>
<vertex x="-1.5" y="1.85"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="-1.5" y="-1.35"/>
<vertex x="-1.5" y="-1.85" curve="90"/>
<vertex x="-1.4" y="-1.95"/>
<vertex x="1.4" y="-1.95" curve="90"/>
<vertex x="1.5" y="-1.85"/>
<vertex x="1.5" y="-1.2" curve="90"/>
<vertex x="1.4" y="-1.1"/>
<vertex x="-1.4" y="-1.1" curve="90"/>
<vertex x="-1.5" y="-1.2"/>
</polygon>
<text x="-1.9" y="2.3" size="0.6096" layer="25">&gt;NAME</text>
<text x="-2.2" y="-2.9" size="0.6096" layer="27">&gt;VALUE</text>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="39"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="39"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="39"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="39"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1" thermals="no"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1" thermals="no"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.6" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.4" y="-0.8"/>
<vertex x="-0.4" y="0.9"/>
<vertex x="0.4" y="0.9"/>
<vertex x="0.4" y="0.6"/>
<vertex x="0.4" y="-0.9"/>
<vertex x="-0.4" y="-0.9"/>
</polygon>
</package>
<package name="SRP1040">
<smd name="1" x="0" y="-4.45" dx="4.5" dy="3.5" layer="1" thermals="no"/>
<smd name="2" x="0" y="4.45" dx="4.5" dy="3.5" layer="1" rot="R180" thermals="no"/>
<wire x1="-4.4" y1="5.35" x2="-1.45" y2="5.35" width="0.1016" layer="51"/>
<wire x1="1.45" y1="5.35" x2="5" y2="5.35" width="0.1016" layer="51"/>
<wire x1="5" y1="5.35" x2="5" y2="-4.75" width="0.1016" layer="51"/>
<wire x1="-5" y1="-5.35" x2="-5" y2="4.75" width="0.1016" layer="51"/>
<wire x1="-4.4" y1="5.35" x2="-5" y2="4.75" width="0.1016" layer="51"/>
<wire x1="5" y1="-4.75" x2="4.4" y2="-5.35" width="0.1016" layer="51"/>
<wire x1="1.45" y1="4.75" x2="-1.45" y2="4.75" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="4.75" x2="-1.45" y2="5.35" width="0.1016" layer="51"/>
<wire x1="1.45" y1="4.75" x2="1.45" y2="5.35" width="0.1016" layer="51"/>
<wire x1="4.4" y1="-5.35" x2="1.45" y2="-5.35" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-5.35" x2="-5" y2="-5.35" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-4.75" x2="1.45" y2="-4.75" width="0.1016" layer="51"/>
<wire x1="1.45" y1="-4.75" x2="1.45" y2="-5.35" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-4.75" x2="-1.45" y2="-5.35" width="0.1016" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="1.45" y="4.75"/>
<vertex x="1.45" y="5.9"/>
<vertex x="-5" y="5.9"/>
<vertex x="-5.1" y="5.9" curve="90"/>
<vertex x="-5.95" y="5.05" curve="90"/>
<vertex x="-5.25" y="4.35"/>
<vertex x="-5" y="4.35"/>
<vertex x="-5" y="4.75"/>
<vertex x="-4.4" y="5.35"/>
<vertex x="-1.45" y="5.35"/>
<vertex x="-1.45" y="4.75"/>
</polygon>
<polygon width="0.1016" layer="51">
<vertex x="-1.45" y="-4.75"/>
<vertex x="-1.45" y="-5.9"/>
<vertex x="5" y="-5.9"/>
<vertex x="5.1" y="-5.9" curve="90"/>
<vertex x="5.95" y="-5.05" curve="90"/>
<vertex x="5.25" y="-4.35"/>
<vertex x="5" y="-4.35"/>
<vertex x="5" y="-4.75"/>
<vertex x="4.4" y="-5.35"/>
<vertex x="1.45" y="-5.35"/>
<vertex x="1.45" y="-4.75"/>
</polygon>
<text x="0.65" y="-1.6" size="1.27" layer="51" rot="R90">2R2</text>
<wire x1="-5" y1="4.75" x2="-4.4" y2="5.35" width="0.127" layer="21"/>
<wire x1="-4.4" y1="5.35" x2="-2.4" y2="5.35" width="0.127" layer="21"/>
<wire x1="-5" y1="4.75" x2="-5" y2="-5.35" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.35" x2="-2.4" y2="-5.35" width="0.127" layer="21"/>
<wire x1="2.4" y1="-5.35" x2="4.4" y2="-5.35" width="0.127" layer="21"/>
<wire x1="4.4" y1="-5.35" x2="5" y2="-4.75" width="0.127" layer="21"/>
<wire x1="5" y1="-4.75" x2="5" y2="5.35" width="0.127" layer="21"/>
<wire x1="5" y1="5.35" x2="2.4" y2="5.35" width="0.127" layer="21"/>
<text x="0.65" y="-1.6" size="1.27" layer="21" rot="R90">2R2</text>
<text x="-3.9" y="6.45" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.15" y="-7.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="PINHD4">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="QUADOPAMP">
<wire x1="-8.89" y1="3.81" x2="-6.35" y2="3.81" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="3.81" x2="-3.81" y2="3.81" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="3.81" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-5.08" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-8.89" y2="3.81" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="5.08" x2="-3.175" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="4.445" x2="-6.35" y2="5.715" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="4.445" x2="-3.81" y2="5.715" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.81" x2="3.81" y2="3.81" width="0.4064" layer="94"/>
<wire x1="3.81" y1="3.81" x2="6.35" y2="3.81" width="0.4064" layer="94"/>
<wire x1="6.35" y1="3.81" x2="8.89" y2="3.81" width="0.4064" layer="94"/>
<wire x1="8.89" y1="3.81" x2="5.08" y2="10.16" width="0.4064" layer="94"/>
<wire x1="5.08" y1="10.16" x2="1.27" y2="3.81" width="0.4064" layer="94"/>
<wire x1="3.175" y1="5.08" x2="4.445" y2="5.08" width="0.1524" layer="94"/>
<wire x1="3.81" y1="4.445" x2="3.81" y2="5.715" width="0.1524" layer="94"/>
<wire x1="6.35" y1="5.715" x2="6.35" y2="4.445" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="-3.81" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-6.35" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-3.81" x2="-8.89" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="-8.89" y1="-3.81" x2="-5.08" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-1.27" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="-3.175" y1="-5.08" x2="-4.445" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-4.445" x2="-3.81" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-5.715" x2="-6.35" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-3.81" x2="6.35" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-3.81" x2="3.81" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-3.81" x2="1.27" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="5.08" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="8.89" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="4.445" y1="-5.08" x2="3.175" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-5.715" x2="6.35" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-4.445" x2="3.81" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="-13.97" y1="13.97" x2="13.97" y2="13.97" width="0.254" layer="94"/>
<wire x1="13.97" y1="13.97" x2="13.97" y2="11.43" width="0.254" layer="94"/>
<wire x1="13.97" y1="11.43" x2="13.97" y2="7.62" width="0.254" layer="94"/>
<wire x1="13.97" y1="7.62" x2="13.97" y2="3.81" width="0.254" layer="94"/>
<wire x1="13.97" y1="3.81" x2="13.97" y2="-3.81" width="0.254" layer="94"/>
<wire x1="13.97" y1="-3.81" x2="13.97" y2="-7.62" width="0.254" layer="94"/>
<wire x1="13.97" y1="-7.62" x2="13.97" y2="-11.43" width="0.254" layer="94"/>
<wire x1="13.97" y1="-11.43" x2="13.97" y2="-13.97" width="0.254" layer="94"/>
<wire x1="13.97" y1="-13.97" x2="-13.97" y2="-13.97" width="0.254" layer="94"/>
<wire x1="-13.97" y1="-13.97" x2="-13.97" y2="-11.43" width="0.254" layer="94"/>
<pin name="OA" x="-19.05" y="11.43" visible="pad" length="middle" direction="out"/>
<pin name="A-" x="-19.05" y="7.62" visible="pad" length="middle" direction="in"/>
<pin name="A+" x="-19.05" y="3.81" visible="pad" length="middle" direction="in"/>
<pin name="V+" x="-19.05" y="0" length="middle" direction="pwr"/>
<pin name="B+" x="-19.05" y="-3.81" visible="pad" length="middle" direction="in"/>
<pin name="B-" x="-19.05" y="-7.62" visible="pad" length="middle" direction="in"/>
<pin name="OB" x="-19.05" y="-11.43" visible="pad" length="middle" direction="out"/>
<pin name="OC" x="19.05" y="-11.43" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="C-" x="19.05" y="-7.62" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="C+" x="19.05" y="-3.81" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="V-" x="19.05" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="D+" x="19.05" y="3.81" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="D-" x="19.05" y="7.62" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="0D" x="19.05" y="11.43" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="-13.97" y1="-11.43" x2="-13.97" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-13.97" y1="-7.62" x2="-13.97" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-13.97" y1="-3.81" x2="-13.97" y2="3.81" width="0.254" layer="94"/>
<wire x1="-13.97" y1="3.81" x2="-13.97" y2="7.62" width="0.254" layer="94"/>
<wire x1="-13.97" y1="7.62" x2="-13.97" y2="11.43" width="0.254" layer="94"/>
<wire x1="-13.97" y1="11.43" x2="-13.97" y2="13.97" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="11.43" width="0.254" layer="94"/>
<wire x1="-5.08" y1="11.43" x2="-13.97" y2="11.43" width="0.254" layer="94"/>
<wire x1="-6.35" y1="3.81" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="2.54" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-13.97" y2="7.62" width="0.254" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="-11.43" y2="1.27" width="0.254" layer="94"/>
<wire x1="-11.43" y1="1.27" x2="-11.43" y2="3.81" width="0.254" layer="94"/>
<wire x1="-11.43" y1="3.81" x2="-13.97" y2="3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="11.43" width="0.254" layer="94"/>
<wire x1="5.08" y1="11.43" x2="13.97" y2="11.43" width="0.254" layer="94"/>
<wire x1="6.35" y1="3.81" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="13.97" y2="7.62" width="0.254" layer="94"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="11.43" y2="1.27" width="0.254" layer="94"/>
<wire x1="11.43" y1="1.27" x2="11.43" y2="3.81" width="0.254" layer="94"/>
<wire x1="11.43" y1="3.81" x2="13.97" y2="3.81" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-5.08" y2="-11.43" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-11.43" x2="-13.97" y2="-11.43" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-3.81" x2="-6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-13.97" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-11.43" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-11.43" y1="-1.27" x2="-11.43" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-11.43" y1="-3.81" x2="-13.97" y2="-3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="-11.43" width="0.254" layer="94"/>
<wire x1="5.08" y1="-11.43" x2="13.97" y2="-11.43" width="0.254" layer="94"/>
<wire x1="6.35" y1="-3.81" x2="6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="13.97" y2="-7.62" width="0.254" layer="94"/>
<wire x1="3.81" y1="-3.81" x2="3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="11.43" y2="-1.27" width="0.254" layer="94"/>
<wire x1="11.43" y1="-1.27" x2="11.43" y2="-3.81" width="0.254" layer="94"/>
<wire x1="11.43" y1="-3.81" x2="13.97" y2="-3.81" width="0.254" layer="94"/>
<text x="-12.7" y="15.24" size="1.27" layer="95">&gt;NAME</text>
<text x="-12.7" y="-16.51" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TPS61088">
<wire x1="-17.78" y1="15.24" x2="-17.78" y2="-15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="-17.78" y2="15.24" width="0.254" layer="94"/>
<pin name="VCC" x="-7.62" y="20.32" length="middle" rot="R270"/>
<pin name="EN" x="-22.86" y="10.16" length="middle"/>
<pin name="FSW" x="-22.86" y="7.62" length="middle"/>
<pin name="SW@4" x="-22.86" y="5.08" length="middle"/>
<pin name="SW@5" x="-22.86" y="2.54" length="middle"/>
<pin name="SW@6" x="-22.86" y="0" length="middle"/>
<pin name="SW@7" x="-22.86" y="-2.54" length="middle"/>
<pin name="BOOT" x="-22.86" y="-5.08" length="middle"/>
<pin name="VIN" x="-22.86" y="-7.62" length="middle"/>
<pin name="SS" x="-7.62" y="-20.32" length="middle" rot="R90"/>
<pin name="NC@11" x="2.54" y="-20.32" length="middle" rot="R90"/>
<pin name="NC@12" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="MODE" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="VOUT@14" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="VOUT@15" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="VOUT@16" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="FB" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="COMP" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="ILIM" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="AGND" x="2.54" y="20.32" length="middle" rot="R270"/>
<pin name="PGND" x="-2.54" y="-20.32" length="middle" rot="R90"/>
<wire x1="17.78" y1="15.24" x2="17.78" y2="-15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="-15.24" x2="-17.78" y2="-15.24" width="0.254" layer="94"/>
<text x="-17.78" y="17.78" size="1.27" layer="95">&gt;NAME</text>
<text x="-17.78" y="-17.78" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="TPS62142">
<wire x1="-12.7" y1="-12.7" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<pin name="SW@1" x="-17.78" y="7.62" length="middle"/>
<pin name="SW@2" x="-17.78" y="5.08" length="middle"/>
<pin name="SW@3" x="-17.78" y="2.54" length="middle"/>
<pin name="PG@4" x="-17.78" y="0" length="middle"/>
<pin name="FB@5" x="-17.78" y="-2.54" length="middle"/>
<pin name="AGND@6" x="-17.78" y="-5.08" length="middle"/>
<pin name="FSW@7" x="-17.78" y="-7.62" length="middle"/>
<pin name="DEF@8" x="-17.78" y="-10.16" length="middle"/>
<pin name="SS/TR@9" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="AVIN@10" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="PVIN@11" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="PVIN@12" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="EN@13" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="VOS@14" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="PGND@15" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="PGND@16" x="17.78" y="7.62" length="middle" rot="R180"/>
<text x="-12.7" y="11.43" size="1.27" layer="95">&gt;NAME</text>
<text x="-12.7" y="-15.24" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="0" y="2.54" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="0" y="-5.08" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
</symbol>
<symbol name="L-EU">
<text x="-1.4986" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.302" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-3.556" x2="1.016" y2="3.556" layer="94"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES" prefix="R">
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="R6432">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XH-CONNECTOR" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD4" x="2.54" y="-1.27"/>
</gates>
<devices>
<device name="A" package="JST-XH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LMC6044" prefix="U">
<gates>
<gate name="G$1" symbol="QUADOPAMP" x="0" y="0"/>
</gates>
<devices>
<device name="IM" package="D_R-PDSO-G14">
<connects>
<connect gate="G$1" pin="0D" pad="14"/>
<connect gate="G$1" pin="A+" pad="3"/>
<connect gate="G$1" pin="A-" pad="2"/>
<connect gate="G$1" pin="B+" pad="5"/>
<connect gate="G$1" pin="B-" pad="6"/>
<connect gate="G$1" pin="C+" pad="10"/>
<connect gate="G$1" pin="C-" pad="9"/>
<connect gate="G$1" pin="D+" pad="12"/>
<connect gate="G$1" pin="D-" pad="13"/>
<connect gate="G$1" pin="OA" pad="1"/>
<connect gate="G$1" pin="OB" pad="7"/>
<connect gate="G$1" pin="OC" pad="8"/>
<connect gate="G$1" pin="V+" pad="4"/>
<connect gate="G$1" pin="V-" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS61088RHLR">
<gates>
<gate name="G$1" symbol="TPS61088" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="VQFN(20)">
<connects>
<connect gate="G$1" pin="AGND" pad="20"/>
<connect gate="G$1" pin="BOOT" pad="8"/>
<connect gate="G$1" pin="COMP" pad="18"/>
<connect gate="G$1" pin="EN" pad="2"/>
<connect gate="G$1" pin="FB" pad="17"/>
<connect gate="G$1" pin="FSW" pad="3"/>
<connect gate="G$1" pin="ILIM" pad="19"/>
<connect gate="G$1" pin="MODE" pad="13"/>
<connect gate="G$1" pin="NC@11" pad="11"/>
<connect gate="G$1" pin="NC@12" pad="12"/>
<connect gate="G$1" pin="PGND" pad="P$22 P$23 P$24 P$25 P$26 P$27 PGND"/>
<connect gate="G$1" pin="SS" pad="10"/>
<connect gate="G$1" pin="SW@4" pad="4"/>
<connect gate="G$1" pin="SW@5" pad="5"/>
<connect gate="G$1" pin="SW@6" pad="6"/>
<connect gate="G$1" pin="SW@7" pad="7"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="VIN" pad="9"/>
<connect gate="G$1" pin="VOUT@14" pad="14"/>
<connect gate="G$1" pin="VOUT@15" pad="15"/>
<connect gate="G$1" pin="VOUT@16" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS62142RGTR">
<gates>
<gate name="G$1" symbol="TPS62142" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="VQFN-16">
<connects>
<connect gate="G$1" pin="AGND@6" pad="6"/>
<connect gate="G$1" pin="AVIN@10" pad="10"/>
<connect gate="G$1" pin="DEF@8" pad="8"/>
<connect gate="G$1" pin="EN@13" pad="13"/>
<connect gate="G$1" pin="FB@5" pad="5"/>
<connect gate="G$1" pin="FSW@7" pad="7"/>
<connect gate="G$1" pin="PG@4" pad="4"/>
<connect gate="G$1" pin="PGND@15" pad="15"/>
<connect gate="G$1" pin="PGND@16" pad="16 17 P$1 P$2 P$3 P$4 P$5"/>
<connect gate="G$1" pin="PVIN@11" pad="11"/>
<connect gate="G$1" pin="PVIN@12" pad="12"/>
<connect gate="G$1" pin="SS/TR@9" pad="9"/>
<connect gate="G$1" pin="SW@1" pad="1"/>
<connect gate="G$1" pin="SW@2" pad="2"/>
<connect gate="G$1" pin="SW@3" pad="3"/>
<connect gate="G$1" pin="VOS@14" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C">
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="1.27"/>
</gates>
<devices>
<device name="A" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="F" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IND">
<gates>
<gate name="G$1" symbol="L-EU" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="SRP1270">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="PA4332">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="SRP1040">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26997/1" library_version="2">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:27060/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="motor_driver">
<packages>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<pad name="1" x="-1.75" y="0" drill="1.2" shape="long" rot="R90"/>
<pad name="2" x="1.75" y="0" drill="1.2" shape="long" rot="R90"/>
<text x="-4.0862" y="3.7688" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.08" y="-4.975" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.004" y1="-0.254" x2="-1.496" y2="0.254" layer="51"/>
<rectangle x1="1.496" y1="-0.254" x2="2.004" y2="0.254" layer="51"/>
<wire x1="-4.05" y1="-3.5" x2="-4.05" y2="3.5" width="0.127" layer="21"/>
<wire x1="-4.05" y1="3.5" x2="4.05" y2="3.5" width="0.127" layer="21"/>
<wire x1="4.05" y1="3.5" x2="4.05" y2="-3.5" width="0.127" layer="21"/>
<wire x1="4.05" y1="-3.5" x2="-4.05" y2="-3.5" width="0.127" layer="21"/>
</package>
<package name="1X02/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="EAGLE_BoosterPack_Library">
<packages>
<package name="4X10-BOOSTERPACK">
<wire x1="1.27" y1="-5.715" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-4.445" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-5.715" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="6.35" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="5.715" x2="-0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="0.635" y1="8.89" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="6.35" x2="-1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="8.255" x2="-0.635" y2="8.89" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-6.985" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-8.255" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-9.525" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-10.795" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-13.97" x2="-0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-13.97" x2="-1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-12.065" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-13.335" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="1" x="0" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="0" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="4" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="0" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="0" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="8" x="0" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="9" x="0" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="10" x="0" y="-12.7" drill="1.016" diameter="1.8796"/>
<text x="4.7498" y="11.4808" size="1.27" layer="25" font="vector" ratio="10">J1</text>
<rectangle x1="-0.254" y1="-5.334" x2="0.254" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-2.794" x2="0.254" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="4.826" x2="0.254" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="7.366" x2="0.254" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="9.906" x2="0.254" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-7.874" x2="0.254" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-10.414" x2="0.254" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-12.954" x2="0.254" y2="-12.446" layer="51" rot="R270"/>
<wire x1="-0.635" y1="8.89" x2="-1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="11.43" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="0.635" y2="8.89" width="0.2286" layer="21"/>
<text x="7.7978" y="11.4808" size="1.27" layer="25" font="vector" ratio="10">J3</text>
<text x="37.7698" y="11.4808" size="1.27" layer="25" font="vector" ratio="10" align="bottom-right">J4</text>
<text x="40.8178" y="11.4808" size="1.27" layer="25" font="vector" ratio="10" align="bottom-right">J2</text>
<wire x1="3.81" y1="-4.445" x2="3.81" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-5.715" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-4.445" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-5.715" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="6.35" x2="3.81" y2="5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="5.715" x2="3.81" y2="4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="4.445" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="5.715" x2="1.905" y2="6.35" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.175" y1="8.89" x2="3.81" y2="8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="8.255" x2="3.81" y2="6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.175" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="6.35" x2="1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="8.255" x2="1.905" y2="8.89" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.81" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-8.255" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.81" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-10.795" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.81" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-13.335" x2="3.175" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-13.97" x2="1.905" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-13.97" x2="1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="21" x="2.54" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="22" x="2.54" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="23" x="2.54" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="24" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="25" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="26" x="2.54" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="27" x="2.54" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="28" x="2.54" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="29" x="2.54" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="30" x="2.54" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="2.286" y1="-5.334" x2="2.794" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-2.794" x2="2.794" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="4.826" x2="2.794" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="7.366" x2="2.794" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="9.906" x2="2.794" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-7.874" x2="2.794" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-10.414" x2="2.794" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-12.954" x2="2.794" y2="-12.446" layer="51" rot="R270"/>
<wire x1="1.905" y1="8.89" x2="1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="11.43" x2="3.81" y2="11.43" width="0.2286" layer="21"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="8.89" width="0.2286" layer="21"/>
<wire x1="3.81" y1="8.89" x2="3.175" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-1.27" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-3.175" x2="41.91" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-1.905" x2="42.545" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-4.445" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-5.715" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.2032" layer="21"/>
<wire x1="43.815" y1="1.27" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="43.815" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-1.27" x2="41.91" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-0.635" x2="41.91" y2="0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="0.635" x2="42.545" y2="1.27" width="0.2032" layer="21"/>
<wire x1="43.815" y1="6.35" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="4.445" x2="41.91" y2="5.715" width="0.2032" layer="21"/>
<wire x1="41.91" y1="5.715" x2="42.545" y2="6.35" width="0.2032" layer="21"/>
<wire x1="44.45" y1="3.175" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="43.815" y1="8.89" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="43.815" y2="6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="6.35" x2="41.91" y2="6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="6.985" x2="41.91" y2="8.255" width="0.2032" layer="21"/>
<wire x1="41.91" y1="8.255" x2="42.545" y2="8.89" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-6.985" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-8.255" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-9.525" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-10.795" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="43.815" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-13.97" x2="42.545" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-13.97" x2="41.91" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-12.065" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-13.335" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<pad name="40" x="43.18" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="39" x="43.18" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="38" x="43.18" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="37" x="43.18" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="36" x="43.18" y="0" drill="1.016" diameter="1.8796"/>
<pad name="35" x="43.18" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="34" x="43.18" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="33" x="43.18" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="32" x="43.18" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="31" x="43.18" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="42.926" y1="-5.334" x2="43.434" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-2.794" x2="43.434" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-0.254" x2="43.434" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="2.286" x2="43.434" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="4.826" x2="43.434" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="7.366" x2="43.434" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="9.906" x2="43.434" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-7.874" x2="43.434" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-10.414" x2="43.434" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-12.954" x2="43.434" y2="-12.446" layer="51" rot="R270"/>
<wire x1="42.545" y1="8.89" x2="41.91" y2="8.89" width="0.2286" layer="21"/>
<wire x1="41.91" y1="8.89" x2="41.91" y2="11.43" width="0.2286" layer="21"/>
<wire x1="41.91" y1="11.43" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="43.815" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.99" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-5.715" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-1.27" x2="46.99" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-1.905" x2="46.99" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-3.175" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-1.905" x2="45.085" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="1.27" x2="44.45" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.355" y1="1.27" x2="46.99" y2="0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="0.635" x2="46.99" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-0.635" x2="46.355" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-1.27" x2="44.45" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="0.635" x2="45.085" y2="1.27" width="0.2032" layer="21"/>
<wire x1="46.355" y1="6.35" x2="46.99" y2="5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="5.715" x2="46.99" y2="4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="4.445" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="5.715" x2="45.085" y2="6.35" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="46.355" y1="8.89" x2="46.99" y2="8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="8.255" x2="46.99" y2="6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="6.985" x2="46.355" y2="6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="6.35" x2="44.45" y2="6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="8.255" x2="45.085" y2="8.89" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.99" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-8.255" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.99" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-10.795" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.99" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-13.335" x2="46.355" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-13.97" x2="45.085" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-13.97" x2="44.45" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<pad name="20" x="45.72" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="19" x="45.72" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="18" x="45.72" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="17" x="45.72" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="16" x="45.72" y="0" drill="1.016" diameter="1.8796"/>
<pad name="15" x="45.72" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="14" x="45.72" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="13" x="45.72" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="12" x="45.72" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="11" x="45.72" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="45.466" y1="-5.334" x2="45.974" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-2.794" x2="45.974" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-0.254" x2="45.974" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="2.286" x2="45.974" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="4.826" x2="45.974" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="7.366" x2="45.974" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="9.906" x2="45.974" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-7.874" x2="45.974" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-10.414" x2="45.974" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-12.954" x2="45.974" y2="-12.446" layer="51" rot="R270"/>
<wire x1="45.085" y1="8.89" x2="44.45" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="11.43" x2="46.99" y2="11.43" width="0.2286" layer="21"/>
<wire x1="46.99" y1="11.43" x2="46.99" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="8.89" x2="46.355" y2="8.89" width="0.2286" layer="21"/>
<text x="7.62" y="9.652" size="0.889" layer="21" font="vector">5V</text>
<text x="7.62" y="7.112" size="0.889" layer="21" font="vector">GND</text>
<text x="7.62" y="4.572" size="0.889" layer="21" font="vector">AA7</text>
<text x="7.62" y="-10.668" size="0.889" layer="21" font="vector">AA1</text>
<text x="-0.254" y="11.938" size="0.889" layer="21" font="vector">1</text>
<text x="3.556" y="11.938" size="0.889" layer="21" font="vector" align="bottom-right">21</text>
<text x="-0.762" y="-15.24" size="0.889" layer="21" font="vector">10</text>
<text x="3.556" y="-15.24" size="0.889" layer="21" font="vector" align="bottom-right">30</text>
<text x="7.62" y="2.032" size="0.889" layer="21" font="vector">AB1</text>
<text x="7.62" y="-0.508" size="0.889" layer="21" font="vector">AA2</text>
<text x="7.62" y="-3.048" size="0.889" layer="21" font="vector">AB2</text>
<text x="7.62" y="-5.588" size="0.889" layer="21" font="vector">AA0</text>
<text x="7.62" y="-8.128" size="0.889" layer="21" font="vector">AB0</text>
<text x="42.418" y="11.938" size="0.889" layer="21" font="vector">40</text>
<text x="46.482" y="11.938" size="0.889" layer="21" font="vector" align="bottom-right">20</text>
<text x="42.672" y="-15.24" size="0.889" layer="21" font="vector">31</text>
<text x="46.736" y="-15.24" size="0.889" layer="21" font="vector" align="bottom-right">11</text>
<text x="37.338" y="7.112" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="4.572" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="2.032" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-0.508" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-3.048" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-5.588" size="0.889" layer="21" font="vector" align="bottom-right">P13</text>
<text x="37.338" y="-10.668" size="0.889" layer="21" font="vector" align="bottom-right">DAC</text>
<text x="37.338" y="-13.208" size="0.889" layer="21" font="vector" align="bottom-right">DAC</text>
<text x="41.148" y="9.652" size="0.889" layer="21" font="vector" align="bottom-right">GND</text>
<text x="41.148" y="7.112" size="0.889" layer="21" font="vector" align="bottom-right">P19</text>
<text x="41.148" y="4.572" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-10.668" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-0.508" size="0.889" layer="21" font="vector" align="bottom-right">RST</text>
<text x="41.148" y="-3.048" size="0.889" layer="21" font="vector" align="bottom-right">MOSI</text>
<text x="41.148" y="-5.588" size="0.889" layer="21" font="vector" align="bottom-right">MISO</text>
<text x="41.148" y="-8.128" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-13.208" size="0.889" layer="21" font="vector" align="bottom-right">P55</text>
<text x="37.338" y="9.652" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="4.572" y="9.652" size="0.889" layer="21" font="vector">3V3</text>
<text x="4.572" y="7.112" size="0.889" layer="21" font="vector">AA6</text>
<text x="4.572" y="4.572" size="0.889" layer="21" font="vector">RX</text>
<text x="4.572" y="2.032" size="0.889" layer="21" font="vector">TX</text>
<text x="4.572" y="-0.508" size="0.889" layer="21" font="vector">P12</text>
<text x="4.572" y="-3.048" size="0.889" layer="21" font="vector">AB6</text>
<text x="4.572" y="-5.588" size="0.889" layer="21" font="vector">CLK</text>
<text x="4.572" y="-8.128" size="0.889" layer="21" font="vector">P22</text>
<text x="4.572" y="-10.668" size="0.889" layer="21" font="vector">SCL</text>
<text x="4.572" y="-13.208" size="0.889" layer="21" font="vector">SDA</text>
<wire x1="7.112" y1="10.668" x2="7.112" y2="-13.208" width="0.0508" layer="21"/>
<wire x1="37.846" y1="10.668" x2="37.846" y2="-13.208" width="0.0508" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="HEADER4X10-BOOSTERPACK">
<wire x1="-2.54" y1="-12.7" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="15.24" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="15.24" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$1" x="5.08" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$2" x="5.08" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$3" x="5.08" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$4" x="5.08" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$5" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$6" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$7" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$8" x="5.08" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$9" x="5.08" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$10" x="5.08" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="53.34" y1="-12.7" x2="53.34" y2="15.24" width="0.254" layer="94"/>
<wire x1="53.34" y1="15.24" x2="58.42" y2="15.24" width="0.254" layer="94"/>
<wire x1="58.42" y1="15.24" x2="58.42" y2="-12.7" width="0.254" layer="94"/>
<wire x1="58.42" y1="-12.7" x2="53.34" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$20" x="60.96" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$19" x="60.96" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$18" x="60.96" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$17" x="60.96" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$16" x="60.96" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$15" x="60.96" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$14" x="60.96" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$13" x="60.96" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$12" x="60.96" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$11" x="60.96" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$21" x="20.32" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$22" x="20.32" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$23" x="20.32" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$24" x="20.32" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$25" x="20.32" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$26" x="20.32" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$27" x="20.32" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$28" x="20.32" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$29" x="20.32" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$30" x="20.32" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="38.1" y1="-12.7" x2="38.1" y2="15.24" width="0.254" layer="94"/>
<wire x1="38.1" y1="15.24" x2="43.18" y2="15.24" width="0.254" layer="94"/>
<wire x1="43.18" y1="15.24" x2="43.18" y2="-12.7" width="0.254" layer="94"/>
<wire x1="43.18" y1="-12.7" x2="38.1" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$40" x="45.72" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$39" x="45.72" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$38" x="45.72" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$37" x="45.72" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$36" x="45.72" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$35" x="45.72" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$34" x="45.72" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$33" x="45.72" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$32" x="45.72" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$31" x="45.72" y="-10.16" visible="pad" length="short" rot="R180"/>
<text x="-2.54" y="17.78" size="2.54" layer="95" font="vector">J1</text>
<text x="53.34" y="17.78" size="2.54" layer="95" font="vector">J2</text>
<text x="12.7" y="17.78" size="2.54" layer="95" font="vector">J3</text>
<text x="38.1" y="17.78" size="2.54" layer="95" font="vector">J4</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HEADER4X10-BOOSTERPACK" prefix="A" uservalue="yes">
<gates>
<gate name="G$1" symbol="HEADER4X10-BOOSTERPACK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4X10-BOOSTERPACK">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$11" pad="11"/>
<connect gate="G$1" pin="P$12" pad="12"/>
<connect gate="G$1" pin="P$13" pad="13"/>
<connect gate="G$1" pin="P$14" pad="14"/>
<connect gate="G$1" pin="P$15" pad="15"/>
<connect gate="G$1" pin="P$16" pad="16"/>
<connect gate="G$1" pin="P$17" pad="17"/>
<connect gate="G$1" pin="P$18" pad="18"/>
<connect gate="G$1" pin="P$19" pad="19"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$20" pad="20"/>
<connect gate="G$1" pin="P$21" pad="21"/>
<connect gate="G$1" pin="P$22" pad="22"/>
<connect gate="G$1" pin="P$23" pad="23"/>
<connect gate="G$1" pin="P$24" pad="24"/>
<connect gate="G$1" pin="P$25" pad="25"/>
<connect gate="G$1" pin="P$26" pad="26"/>
<connect gate="G$1" pin="P$27" pad="27"/>
<connect gate="G$1" pin="P$28" pad="28"/>
<connect gate="G$1" pin="P$29" pad="29"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$30" pad="30"/>
<connect gate="G$1" pin="P$31" pad="31"/>
<connect gate="G$1" pin="P$32" pad="32"/>
<connect gate="G$1" pin="P$33" pad="33"/>
<connect gate="G$1" pin="P$34" pad="34"/>
<connect gate="G$1" pin="P$35" pad="35"/>
<connect gate="G$1" pin="P$36" pad="36"/>
<connect gate="G$1" pin="P$37" pad="37"/>
<connect gate="G$1" pin="P$38" pad="38"/>
<connect gate="G$1" pin="P$39" pad="39"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$40" pad="40"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X02" urn="urn:adsk.eagle:footprint:22309/1" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90" urn="urn:adsk.eagle:footprint:22310/1" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:22435/2" type="model" library_version="2">
<description>PIN HEADER</description>
</package3d>
<package3d name="1X02/90" urn="urn:adsk.eagle:package:22437/1" type="box" library_version="2">
<description>PIN HEADER</description>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD2" urn="urn:adsk.eagle:symbol:22308/1" library_version="2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X2" urn="urn:adsk.eagle:component:22516/2" prefix="JP" uservalue="yes" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22435/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22437/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4-SMALL-DOCFIELD" device="" value="PS"/>
<part name="SUPPLY13" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VCC" device=""/>
<part name="12V" library="motor_driver" deviceset="PINHD-1X2" device="" value="Out"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="BATT" library="motor_driver" deviceset="PINHD-1X2" device="" value="In"/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VCC" device=""/>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="J1" library="power_supply" deviceset="XH-CONNECTOR" device="A" value="JST-XH"/>
<part name="U4" library="power_supply" deviceset="LMC6044" device="IM"/>
<part name="SUPPLY12" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R1" library="power_supply" deviceset="RES" device="C" value="220 KΩ"/>
<part name="R2" library="power_supply" deviceset="RES" device="C" value="110 KΩ"/>
<part name="R3" library="power_supply" deviceset="RES" device="C" value="220 KΩ"/>
<part name="R4" library="power_supply" deviceset="RES" device="C" value="110 KΩ"/>
<part name="R5" library="power_supply" deviceset="RES" device="C" value="220 KΩ"/>
<part name="R6" library="power_supply" deviceset="RES" device="C" value="110 KΩ"/>
<part name="SUPPLY15" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY16" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY17" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R7" library="power_supply" deviceset="RES" device="C" value="220 KΩ"/>
<part name="R8" library="power_supply" deviceset="RES" device="C" value="220 KΩ"/>
<part name="R9" library="power_supply" deviceset="RES" device="C" value="220 KΩ"/>
<part name="R10" library="power_supply" deviceset="RES" device="C" value="100 KΩ"/>
<part name="R11" library="power_supply" deviceset="RES" device="C" value="100 KΩ"/>
<part name="R12" library="power_supply" deviceset="RES" device="C" value="110 KΩ"/>
<part name="R13" library="power_supply" deviceset="RES" device="C" value="110 KΩ"/>
<part name="R14" library="power_supply" deviceset="RES" device="C" value="110 KΩ"/>
<part name="A1" library="EAGLE_BoosterPack_Library" deviceset="HEADER4X10-BOOSTERPACK" device="" value="TI"/>
<part name="SUPPLY20" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY21" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="JP1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X2" device="" package3d_urn="urn:adsk.eagle:package:22435/2" value="3V3E"/>
<part name="SUPPLY18" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U1" library="power_supply" deviceset="TPS61088RHLR" device="A"/>
<part name="U2" library="power_supply" deviceset="TPS62142RGTR" device="A"/>
<part name="CBOOT" library="power_supply" deviceset="CAP" device="E" value="100nF"/>
<part name="RT" library="power_supply" deviceset="RES" device="D" value="267K"/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="RLIM" library="power_supply" deviceset="RES" device="D" value="102K"/>
<part name="CSS" library="power_supply" deviceset="CAP" device="B" value="8.2nF"/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VCC" device=""/>
<part name="CIN" library="power_supply" deviceset="CAP" device="D" value="22uF"/>
<part name="CIN." library="power_supply" deviceset="CAP" device="D" value="22uF"/>
<part name="CVIN" library="power_supply" deviceset="CAP" device="B" value="100nF"/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="CVCC" library="power_supply" deviceset="CAP" device="B" value="2.2uF"/>
<part name="CCOMP2" library="power_supply" deviceset="CAP" device="B" value="27pF"/>
<part name="CCOMP" library="power_supply" deviceset="CAP" device="B" value="560pF"/>
<part name="RCOMP" library="power_supply" deviceset="RES" device="D" value="41.2K"/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY9" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY10" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="RFBT" library="power_supply" deviceset="RES" device="C" value="825K"/>
<part name="RFBB1" library="power_supply" deviceset="RES" device="C" value="25.5K"/>
<part name="RFBB2" library="power_supply" deviceset="RES" device="C" value="66.5K"/>
<part name="CSS2" library="power_supply" deviceset="CAP" device="B" value="3.3nF"/>
<part name="CIN2" library="power_supply" deviceset="CAP" device="E" value="10uF"/>
<part name="COUT2" library="power_supply" deviceset="CAP" device="E" value="22uF"/>
<part name="RPG" library="power_supply" deviceset="RES" device="D" value="100K"/>
<part name="L2" library="power_supply" deviceset="IND" device="B" value="2.2uH"/>
<part name="SUPPLY14" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY19" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY22" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY23" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY24" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY25" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VCC" device=""/>
<part name="RLP1" library="power_supply" deviceset="RES" device="D" value="10K"/>
<part name="RLP2" library="power_supply" deviceset="RES" device="D" value="10K"/>
<part name="RLP3" library="power_supply" deviceset="RES" device="D" value="10K"/>
<part name="CLP1" library="power_supply" deviceset="CAP" device="B" value="2.2uF"/>
<part name="CLP2" library="power_supply" deviceset="CAP" device="B" value="2.2uF"/>
<part name="CLP3" library="power_supply" deviceset="CAP" device="B" value="2.2uF"/>
<part name="SUPPLY26" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY27" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY28" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="COUT" library="power_supply" deviceset="CAP" device="D" value="22uF"/>
<part name="COUT." library="power_supply" deviceset="CAP" device="D" value="22uF"/>
<part name="COUT," library="power_supply" deviceset="CAP" device="D" value="22uF"/>
<part name="COUT'" library="power_supply" deviceset="CAP" device="D" value="22uF"/>
<part name="L1" library="power_supply" deviceset="IND" device="C" value="2.2uH"/>
<part name="CHF" library="power_supply" deviceset="CAP" device="B" value="1uF"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="2.54" y1="259.08" x2="2.54" y2="200.66" width="0.1524" layer="97"/>
<wire x1="2.54" y1="200.66" x2="129.54" y2="200.66" width="0.1524" layer="97"/>
<wire x1="129.54" y1="200.66" x2="178.435" y2="200.66" width="0.1524" layer="97"/>
<wire x1="178.435" y1="200.66" x2="178.435" y2="259.08" width="0.1524" layer="97"/>
<wire x1="178.435" y1="259.08" x2="129.54" y2="259.08" width="0.1524" layer="97"/>
<wire x1="129.54" y1="259.08" x2="2.54" y2="259.08" width="0.1524" layer="97"/>
<wire x1="2.54" y1="198.12" x2="2.54" y2="130.81" width="0.1524" layer="97"/>
<wire x1="2.54" y1="130.81" x2="178.435" y2="130.81" width="0.1524" layer="97"/>
<wire x1="178.435" y1="130.81" x2="178.435" y2="198.12" width="0.1524" layer="97"/>
<wire x1="178.435" y1="198.12" x2="62.23" y2="198.12" width="0.1524" layer="97"/>
<wire x1="62.23" y1="198.12" x2="2.54" y2="198.12" width="0.1524" layer="97"/>
<wire x1="2.54" y1="128.27" x2="2.54" y2="72.39" width="0.1524" layer="97"/>
<wire x1="2.54" y1="72.39" x2="77.47" y2="72.39" width="0.1524" layer="97"/>
<wire x1="77.47" y1="72.39" x2="178.435" y2="72.39" width="0.1524" layer="97"/>
<wire x1="178.435" y1="72.39" x2="178.435" y2="128.27" width="0.1524" layer="97"/>
<wire x1="178.435" y1="128.27" x2="77.47" y2="128.27" width="0.1524" layer="97"/>
<text x="162.56" y="201.93" size="1.778" layer="97">Power Stage</text>
<text x="159.385" y="132.08" size="1.778" layer="97">Controller Boost</text>
<text x="168.91" y="73.66" size="1.778" layer="97">Outputs</text>
<wire x1="77.47" y1="128.27" x2="2.54" y2="128.27" width="0.1524" layer="97"/>
<wire x1="2.54" y1="68.58" x2="178.435" y2="68.58" width="0.1524" layer="97"/>
<wire x1="178.435" y1="68.58" x2="178.435" y2="15.24" width="0.1524" layer="97"/>
<wire x1="178.435" y1="15.24" x2="2.54" y2="15.24" width="0.1524" layer="97"/>
<wire x1="2.54" y1="15.24" x2="2.54" y2="68.58" width="0.1524" layer="97"/>
<text x="153.035" y="16.51" size="1.778" layer="97">Battery Cell Monitoring</text>
<wire x1="129.54" y1="259.08" x2="129.54" y2="200.66" width="0.1524" layer="97"/>
<text x="154.94" y="256.54" size="1.778" layer="97">3V3 Buck Converter</text>
<text x="105.41" y="256.54" size="1.778" layer="97">12V Boost Converter</text>
<wire x1="77.47" y1="128.27" x2="77.47" y2="72.39" width="0.1524" layer="97"/>
<text x="56.515" y="73.66" size="1.778" layer="97">Controller Buck</text>
<wire x1="62.23" y1="198.12" x2="62.23" y2="171.45" width="0.1524" layer="97"/>
<wire x1="62.23" y1="171.45" x2="2.54" y2="171.45" width="0.1524" layer="97"/>
</plain>
<instances>
<instance part="FRAME1" gate="/1" x="0" y="0"/>
<instance part="SUPPLY13" gate="G$1" x="10.16" y="252.73"/>
<instance part="12V" gate="G$1" x="135.89" y="82.55"/>
<instance part="SUPPLY1" gate="GND" x="127" y="77.47"/>
<instance part="BATT" gate="G$1" x="106.68" y="85.09" rot="R180"/>
<instance part="SUPPLY8" gate="G$1" x="115.57" y="90.17"/>
<instance part="SUPPLY11" gate="GND" x="115.57" y="77.47"/>
<instance part="J1" gate="G$1" x="163.83" y="57.15"/>
<instance part="U4" gate="G$1" x="44.45" y="43.18"/>
<instance part="SUPPLY12" gate="GND" x="91.44" y="22.86"/>
<instance part="R1" gate="G$1" x="128.27" y="46.99" smashed="yes" rot="MR90">
<attribute name="NAME" x="126.746" y="47.2186" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="126.746" y="44.45" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R2" gate="G$1" x="128.27" y="33.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="126.492" y="33.2486" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="126.492" y="30.48" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R3" gate="G$1" x="140.97" y="46.99" smashed="yes" rot="MR90">
<attribute name="NAME" x="139.192" y="47.9806" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="139.192" y="44.704" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R4" gate="G$1" x="140.97" y="34.29" smashed="yes" rot="MR90">
<attribute name="NAME" x="139.192" y="35.0266" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="139.192" y="32.004" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R5" gate="G$1" x="154.94" y="46.99" smashed="yes" rot="MR90">
<attribute name="NAME" x="152.908" y="47.9806" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="152.908" y="44.958" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R6" gate="G$1" x="154.94" y="34.29" smashed="yes" rot="MR90">
<attribute name="NAME" x="152.908" y="35.2806" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="152.654" y="32.004" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY15" gate="GND" x="128.27" y="23.114" rot="MR0"/>
<instance part="SUPPLY16" gate="GND" x="140.97" y="23.114" rot="MR0"/>
<instance part="SUPPLY17" gate="GND" x="154.94" y="23.114" rot="MR0"/>
<instance part="R7" gate="G$1" x="74.93" y="35.56" smashed="yes">
<attribute name="NAME" x="77.724" y="32.7406" size="1.778" layer="95"/>
<attribute name="VALUE" x="69.85" y="37.084" size="1.778" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="13.97" y="35.56" smashed="yes">
<attribute name="NAME" x="8.636" y="32.4866" size="1.778" layer="95"/>
<attribute name="VALUE" x="8.89" y="37.084" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="13.97" y="50.8" smashed="yes">
<attribute name="NAME" x="11.43" y="52.2986" size="1.778" layer="95"/>
<attribute name="VALUE" x="11.43" y="47.498" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="91.44" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="93.472" y="56.8706" size="1.778" layer="95"/>
<attribute name="VALUE" x="93.218" y="53.848" size="1.778" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="91.44" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="93.472" y="44.1706" size="1.778" layer="95"/>
<attribute name="VALUE" x="93.218" y="41.148" size="1.778" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="68.58" y="29.21" smashed="yes" rot="R90">
<attribute name="NAME" x="70.612" y="29.1846" size="1.778" layer="95"/>
<attribute name="VALUE" x="70.358" y="26.162" size="1.778" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="20.32" y="29.21" smashed="yes" rot="R90">
<attribute name="NAME" x="13.716" y="29.4386" size="1.778" layer="95"/>
<attribute name="VALUE" x="10.668" y="26.67" size="1.778" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="20.32" y="57.15" smashed="yes" rot="R90">
<attribute name="NAME" x="14.224" y="58.3946" size="1.778" layer="95"/>
<attribute name="VALUE" x="10.668" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="A1" gate="G$1" x="165.1" y="113.03" rot="R180"/>
<instance part="SUPPLY20" gate="GND" x="137.16" y="95.25"/>
<instance part="SUPPLY21" gate="GND" x="101.6" y="95.25"/>
<instance part="JP1" gate="G$1" x="166.37" y="82.55" smashed="yes">
<attribute name="NAME" x="160.02" y="88.265" size="1.778" layer="95"/>
<attribute name="VALUE" x="160.02" y="77.47" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY18" gate="GND" x="116.84" y="23.114" rot="MR0"/>
<instance part="U1" gate="G$1" x="123.19" y="170.18" smashed="yes">
<attribute name="NAME" x="105.41" y="187.96" size="1.27" layer="95"/>
<attribute name="VALUE" x="128.27" y="152.4" size="1.27" layer="95"/>
</instance>
<instance part="U2" gate="G$1" x="35.56" y="110.49"/>
<instance part="CBOOT" gate="G$1" x="90.17" y="171.45" smashed="yes">
<attribute name="NAME" x="81.026" y="171.831" size="1.778" layer="95"/>
<attribute name="VALUE" x="82.804" y="166.751" size="1.778" layer="96"/>
</instance>
<instance part="RT" gate="G$1" x="88.9" y="177.8" smashed="yes">
<attribute name="NAME" x="85.09" y="179.2986" size="1.778" layer="95"/>
<attribute name="VALUE" x="91.44" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="132.08" y="189.23"/>
<instance part="SUPPLY3" gate="GND" x="120.65" y="134.62"/>
<instance part="RLIM" gate="G$1" x="158.75" y="173.99" smashed="yes" rot="R90">
<attribute name="NAME" x="160.02" y="174.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="160.02" y="170.688" size="1.778" layer="96"/>
</instance>
<instance part="CSS" gate="G$1" x="115.57" y="144.78" smashed="yes">
<attribute name="NAME" x="109.474" y="145.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="108.204" y="140.081" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="74.93" y="167.64"/>
<instance part="CIN" gate="G$1" x="10.16" y="233.68"/>
<instance part="CIN." gate="G$1" x="22.86" y="233.68"/>
<instance part="CVIN" gate="G$1" x="74.93" y="152.4"/>
<instance part="SUPPLY5" gate="GND" x="74.93" y="134.62"/>
<instance part="SUPPLY6" gate="GND" x="10.16" y="204.47"/>
<instance part="CVCC" gate="G$1" x="19.05" y="151.13"/>
<instance part="CCOMP2" gate="G$1" x="33.02" y="151.13"/>
<instance part="CCOMP" gate="G$1" x="48.26" y="144.78"/>
<instance part="RCOMP" gate="G$1" x="48.26" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="50.038" y="156.1846" size="1.778" layer="95"/>
<attribute name="VALUE" x="50.038" y="152.908" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="48.26" y="134.62"/>
<instance part="SUPPLY9" gate="GND" x="33.02" y="134.62"/>
<instance part="SUPPLY10" gate="GND" x="19.05" y="134.62"/>
<instance part="RFBT" gate="G$1" x="57.15" y="241.3" rot="R90"/>
<instance part="RFBB1" gate="G$1" x="57.15" y="226.06" rot="R90"/>
<instance part="RFBB2" gate="G$1" x="57.15" y="214.63" rot="R90"/>
<instance part="CSS2" gate="G$1" x="55.88" y="95.25"/>
<instance part="CIN2" gate="G$1" x="68.58" y="95.25"/>
<instance part="COUT2" gate="G$1" x="152.4" y="229.87"/>
<instance part="RPG" gate="G$1" x="144.78" y="247.65"/>
<instance part="L2" gate="G$1" x="144.78" y="240.03" smashed="yes" rot="R90">
<attribute name="NAME" x="140.97" y="241.5286" size="1.778" layer="95"/>
<attribute name="VALUE" x="140.97" y="236.728" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY14" gate="GND" x="152.4" y="204.47"/>
<instance part="SUPPLY19" gate="GND" x="15.24" y="93.98"/>
<instance part="SUPPLY22" gate="GND" x="55.88" y="85.09"/>
<instance part="SUPPLY23" gate="GND" x="68.58" y="85.09"/>
<instance part="SUPPLY24" gate="GND" x="55.88" y="123.19" rot="R180"/>
<instance part="SUPPLY25" gate="G$1" x="68.58" y="107.95"/>
<instance part="RLP1" gate="G$1" x="13.97" y="193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="11.43" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="11.43" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="RLP2" gate="G$1" x="41.91" y="193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="39.37" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.37" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="RLP3" gate="G$1" x="86.36" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="87.63" y="114.5286" size="1.778" layer="95"/>
<attribute name="VALUE" x="87.63" y="110.998" size="1.778" layer="96"/>
</instance>
<instance part="CLP1" gate="G$1" x="21.59" y="187.96"/>
<instance part="CLP2" gate="G$1" x="49.53" y="187.96"/>
<instance part="CLP3" gate="G$1" x="86.36" y="104.14"/>
<instance part="SUPPLY26" gate="GND" x="21.59" y="177.8"/>
<instance part="SUPPLY27" gate="GND" x="49.53" y="177.8"/>
<instance part="SUPPLY28" gate="GND" x="86.36" y="95.25"/>
<instance part="COUT" gate="G$1" x="78.74" y="234.95"/>
<instance part="COUT." gate="G$1" x="91.44" y="234.95"/>
<instance part="COUT," gate="G$1" x="104.14" y="234.95"/>
<instance part="COUT'" gate="G$1" x="116.84" y="234.95"/>
<instance part="L1" gate="G$1" x="35.56" y="247.65" smashed="yes" rot="R90">
<attribute name="NAME" x="31.75" y="249.1486" size="1.778" layer="95"/>
<attribute name="VALUE" x="31.75" y="244.348" size="1.778" layer="96"/>
</instance>
<instance part="CHF" gate="G$1" x="68.58" y="234.95"/>
</instances>
<busses>
</busses>
<nets>
<net name="12V" class="0">
<segment>
<pinref part="12V" gate="G$1" pin="1"/>
<wire x1="133.35" y1="85.09" x2="127" y2="85.09" width="0.1524" layer="91"/>
<label x="127" y="85.09" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VOUT@16"/>
<wire x1="146.05" y1="172.72" x2="148.59" y2="172.72" width="0.1524" layer="91"/>
<wire x1="148.59" y1="172.72" x2="148.59" y2="170.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VOUT@15"/>
<wire x1="148.59" y1="170.18" x2="146.05" y2="170.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VOUT@14"/>
<wire x1="146.05" y1="167.64" x2="148.59" y2="167.64" width="0.1524" layer="91"/>
<wire x1="148.59" y1="167.64" x2="148.59" y2="170.18" width="0.1524" layer="91"/>
<junction x="148.59" y="170.18"/>
<junction x="148.59" y="167.64"/>
<wire x1="148.59" y1="167.64" x2="151.13" y2="167.64" width="0.1524" layer="91"/>
<label x="151.13" y="167.64" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RFBT" gate="G$1" pin="2"/>
<wire x1="57.15" y1="246.38" x2="57.15" y2="247.65" width="0.1524" layer="91"/>
<wire x1="57.15" y1="247.65" x2="54.61" y2="247.65" width="0.1524" layer="91"/>
<label x="54.61" y="247.65" size="0.762" layer="95" rot="R180" xref="yes"/>
<wire x1="78.74" y1="237.49" x2="78.74" y2="247.65" width="0.1524" layer="91"/>
<wire x1="78.74" y1="247.65" x2="69.85" y2="247.65" width="0.1524" layer="91"/>
<wire x1="68.58" y1="247.65" x2="57.15" y2="247.65" width="0.1524" layer="91"/>
<wire x1="91.44" y1="237.49" x2="91.44" y2="247.65" width="0.1524" layer="91"/>
<wire x1="91.44" y1="247.65" x2="78.74" y2="247.65" width="0.1524" layer="91"/>
<wire x1="104.14" y1="237.49" x2="104.14" y2="247.65" width="0.1524" layer="91"/>
<wire x1="104.14" y1="247.65" x2="91.44" y2="247.65" width="0.1524" layer="91"/>
<junction x="91.44" y="247.65"/>
<junction x="78.74" y="247.65"/>
<junction x="57.15" y="247.65"/>
<pinref part="COUT" gate="G$1" pin="1"/>
<pinref part="COUT." gate="G$1" pin="1"/>
<pinref part="COUT," gate="G$1" pin="1"/>
<pinref part="COUT'" gate="G$1" pin="1"/>
<wire x1="116.84" y1="237.49" x2="116.84" y2="247.65" width="0.1524" layer="91"/>
<wire x1="116.84" y1="247.65" x2="104.14" y2="247.65" width="0.1524" layer="91"/>
<junction x="104.14" y="247.65"/>
<pinref part="CHF" gate="G$1" pin="1"/>
<wire x1="68.58" y1="237.49" x2="68.58" y2="247.65" width="0.1524" layer="91"/>
<wire x1="68.58" y1="247.65" x2="69.85" y2="247.65" width="0.1524" layer="91"/>
<junction x="68.58" y="247.65"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="12V" gate="G$1" pin="2"/>
<wire x1="133.35" y1="82.55" x2="127" y2="82.55" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="127" y1="80.01" x2="127" y2="82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="BATT" gate="G$1" pin="1"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="109.22" y1="82.55" x2="115.57" y2="82.55" width="0.1524" layer="91"/>
<wire x1="115.57" y1="82.55" x2="115.57" y2="80.01" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="91.44" y1="25.4" x2="91.44" y2="36.83" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="V-"/>
<wire x1="91.44" y1="36.83" x2="91.44" y2="38.1" width="0.1524" layer="91"/>
<wire x1="63.5" y1="43.18" x2="86.36" y2="43.18" width="0.1524" layer="91"/>
<wire x1="86.36" y1="43.18" x2="86.36" y2="36.83" width="0.1524" layer="91"/>
<wire x1="86.36" y1="36.83" x2="91.44" y2="36.83" width="0.1524" layer="91"/>
<junction x="91.44" y="36.83"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="128.27" y1="27.94" x2="128.27" y2="25.654" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="140.97" y1="29.21" x2="140.97" y2="25.654" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="154.94" y1="29.21" x2="154.94" y2="25.654" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="137.16" y1="97.79" x2="137.16" y2="102.87" width="0.1524" layer="91"/>
<pinref part="A1" gate="G$1" pin="P$22"/>
<wire x1="137.16" y1="102.87" x2="144.78" y2="102.87" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$20"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<wire x1="104.14" y1="100.33" x2="101.6" y2="100.33" width="0.1524" layer="91"/>
<wire x1="101.6" y1="100.33" x2="101.6" y2="97.79" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="114.3" y="62.23" size="0.762" layer="95" rot="R180" xref="yes"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="116.84" y1="62.23" x2="114.3" y2="62.23" width="0.1524" layer="91"/>
<wire x1="116.84" y1="25.654" x2="116.84" y2="62.23" width="0.1524" layer="91"/>
<junction x="116.84" y="62.23"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="116.84" y1="62.23" x2="161.29" y2="62.23" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="80.01" y1="35.56" x2="81.28" y2="35.56" width="0.1524" layer="91"/>
<label x="81.28" y="35.56" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="AGND"/>
<wire x1="125.73" y1="190.5" x2="125.73" y2="193.04" width="0.1524" layer="91"/>
<wire x1="125.73" y1="193.04" x2="132.08" y2="193.04" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="132.08" y1="193.04" x2="132.08" y2="191.77" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PGND"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="120.65" y1="149.86" x2="120.65" y2="148.59" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="NC@11"/>
<wire x1="120.65" y1="148.59" x2="120.65" y2="138.43" width="0.1524" layer="91"/>
<wire x1="120.65" y1="138.43" x2="120.65" y2="137.16" width="0.1524" layer="91"/>
<wire x1="125.73" y1="149.86" x2="125.73" y2="148.59" width="0.1524" layer="91"/>
<wire x1="125.73" y1="148.59" x2="120.65" y2="148.59" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="NC@12"/>
<wire x1="146.05" y1="162.56" x2="147.32" y2="162.56" width="0.1524" layer="91"/>
<wire x1="147.32" y1="162.56" x2="147.32" y2="148.59" width="0.1524" layer="91"/>
<wire x1="147.32" y1="148.59" x2="125.73" y2="148.59" width="0.1524" layer="91"/>
<junction x="125.73" y="148.59"/>
<junction x="120.65" y="148.59"/>
<pinref part="RLIM" gate="G$1" pin="1"/>
<wire x1="158.75" y1="168.91" x2="158.75" y2="148.59" width="0.1524" layer="91"/>
<wire x1="158.75" y1="148.59" x2="147.32" y2="148.59" width="0.1524" layer="91"/>
<junction x="147.32" y="148.59"/>
<pinref part="CSS" gate="G$1" pin="2"/>
<wire x1="115.57" y1="139.7" x2="115.57" y2="138.43" width="0.1524" layer="91"/>
<wire x1="115.57" y1="138.43" x2="120.65" y2="138.43" width="0.1524" layer="91"/>
<junction x="120.65" y="138.43"/>
</segment>
<segment>
<pinref part="CVIN" gate="G$1" pin="2"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="74.93" y1="147.32" x2="74.93" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CIN" gate="G$1" pin="2"/>
<wire x1="10.16" y1="228.6" x2="10.16" y2="208.28" width="0.1524" layer="91"/>
<wire x1="10.16" y1="208.28" x2="22.86" y2="208.28" width="0.1524" layer="91"/>
<pinref part="CIN." gate="G$1" pin="2"/>
<wire x1="22.86" y1="208.28" x2="22.86" y2="228.6" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="10.16" y1="208.28" x2="10.16" y2="207.01" width="0.1524" layer="91"/>
<junction x="10.16" y="208.28"/>
<pinref part="RFBB2" gate="G$1" pin="1"/>
<wire x1="57.15" y1="209.55" x2="57.15" y2="208.28" width="0.1524" layer="91"/>
<wire x1="57.15" y1="208.28" x2="22.86" y2="208.28" width="0.1524" layer="91"/>
<junction x="22.86" y="208.28"/>
<wire x1="57.15" y1="208.28" x2="68.58" y2="208.28" width="0.1524" layer="91"/>
<wire x1="68.58" y1="208.28" x2="78.74" y2="208.28" width="0.1524" layer="91"/>
<wire x1="78.74" y1="208.28" x2="78.74" y2="229.87" width="0.1524" layer="91"/>
<wire x1="91.44" y1="229.87" x2="91.44" y2="208.28" width="0.1524" layer="91"/>
<wire x1="91.44" y1="208.28" x2="78.74" y2="208.28" width="0.1524" layer="91"/>
<wire x1="104.14" y1="229.87" x2="104.14" y2="208.28" width="0.1524" layer="91"/>
<wire x1="104.14" y1="208.28" x2="91.44" y2="208.28" width="0.1524" layer="91"/>
<junction x="57.15" y="208.28"/>
<junction x="78.74" y="208.28"/>
<junction x="91.44" y="208.28"/>
<pinref part="COUT" gate="G$1" pin="2"/>
<pinref part="COUT." gate="G$1" pin="2"/>
<pinref part="COUT," gate="G$1" pin="2"/>
<pinref part="COUT'" gate="G$1" pin="2"/>
<wire x1="116.84" y1="229.87" x2="116.84" y2="208.28" width="0.1524" layer="91"/>
<wire x1="116.84" y1="208.28" x2="104.14" y2="208.28" width="0.1524" layer="91"/>
<junction x="104.14" y="208.28"/>
<pinref part="CHF" gate="G$1" pin="2"/>
<wire x1="68.58" y1="229.87" x2="68.58" y2="208.28" width="0.1524" layer="91"/>
<junction x="68.58" y="208.28"/>
</segment>
<segment>
<pinref part="CCOMP" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="48.26" y1="139.7" x2="48.26" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CCOMP2" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="33.02" y1="146.05" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CVCC" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="19.05" y1="146.05" x2="19.05" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="COUT2" gate="G$1" pin="2"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="152.4" y1="224.79" x2="152.4" y2="207.01" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CSS2" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<wire x1="55.88" y1="90.17" x2="55.88" y2="87.63" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CIN2" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<wire x1="68.58" y1="90.17" x2="68.58" y2="87.63" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="DEF@8"/>
<wire x1="17.78" y1="100.33" x2="15.24" y2="100.33" width="0.1524" layer="91"/>
<wire x1="15.24" y1="100.33" x2="15.24" y2="96.52" width="0.1524" layer="91"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<pinref part="U2" gate="G$1" pin="AGND@6"/>
<wire x1="17.78" y1="105.41" x2="8.89" y2="105.41" width="0.1524" layer="91"/>
<wire x1="8.89" y1="105.41" x2="8.89" y2="100.33" width="0.1524" layer="91"/>
<wire x1="8.89" y1="100.33" x2="15.24" y2="100.33" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="FB@5"/>
<wire x1="17.78" y1="107.95" x2="8.89" y2="107.95" width="0.1524" layer="91"/>
<wire x1="8.89" y1="107.95" x2="8.89" y2="105.41" width="0.1524" layer="91"/>
<junction x="8.89" y="105.41"/>
<junction x="15.24" y="100.33"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PGND@16"/>
<wire x1="53.34" y1="118.11" x2="55.88" y2="118.11" width="0.1524" layer="91"/>
<wire x1="55.88" y1="118.11" x2="55.88" y2="115.57" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="PGND@15"/>
<wire x1="55.88" y1="115.57" x2="53.34" y2="115.57" width="0.1524" layer="91"/>
<junction x="55.88" y="118.11"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
<wire x1="55.88" y1="118.11" x2="55.88" y2="120.65" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CLP1" gate="G$1" pin="2"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<wire x1="21.59" y1="182.88" x2="21.59" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CLP2" gate="G$1" pin="2"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="49.53" y1="182.88" x2="49.53" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CLP3" gate="G$1" pin="2"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="86.36" y1="99.06" x2="86.36" y2="97.79" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="BATT" gate="G$1" pin="2"/>
<pinref part="SUPPLY8" gate="G$1" pin="VCC"/>
<wire x1="109.22" y1="85.09" x2="115.57" y2="85.09" width="0.1524" layer="91"/>
<wire x1="115.57" y1="85.09" x2="115.57" y2="87.63" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VIN"/>
<pinref part="SUPPLY4" gate="G$1" pin="VCC"/>
<wire x1="100.33" y1="162.56" x2="74.93" y2="162.56" width="0.1524" layer="91"/>
<wire x1="74.93" y1="162.56" x2="74.93" y2="165.1" width="0.1524" layer="91"/>
<pinref part="CVIN" gate="G$1" pin="1"/>
<wire x1="74.93" y1="154.94" x2="74.93" y2="162.56" width="0.1524" layer="91"/>
<junction x="74.93" y="162.56"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="G$1" pin="VCC"/>
<wire x1="10.16" y1="250.19" x2="10.16" y2="247.65" width="0.1524" layer="91"/>
<pinref part="CIN" gate="G$1" pin="1"/>
<wire x1="10.16" y1="247.65" x2="22.86" y2="247.65" width="0.1524" layer="91"/>
<wire x1="22.86" y1="247.65" x2="30.48" y2="247.65" width="0.1524" layer="91"/>
<wire x1="10.16" y1="236.22" x2="10.16" y2="247.65" width="0.1524" layer="91"/>
<pinref part="CIN." gate="G$1" pin="1"/>
<wire x1="22.86" y1="236.22" x2="22.86" y2="247.65" width="0.1524" layer="91"/>
<junction x="10.16" y="247.65"/>
<junction x="22.86" y="247.65"/>
<pinref part="L1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="EN@13"/>
<wire x1="53.34" y1="110.49" x2="55.88" y2="110.49" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="PVIN@12"/>
<wire x1="55.88" y1="110.49" x2="55.88" y2="107.95" width="0.1524" layer="91"/>
<wire x1="55.88" y1="107.95" x2="53.34" y2="107.95" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="PVIN@11"/>
<wire x1="53.34" y1="105.41" x2="55.88" y2="105.41" width="0.1524" layer="91"/>
<wire x1="55.88" y1="105.41" x2="55.88" y2="107.95" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="AVIN@10"/>
<wire x1="53.34" y1="102.87" x2="55.88" y2="102.87" width="0.1524" layer="91"/>
<wire x1="55.88" y1="102.87" x2="55.88" y2="105.41" width="0.1524" layer="91"/>
<pinref part="CIN2" gate="G$1" pin="1"/>
<wire x1="55.88" y1="102.87" x2="68.58" y2="102.87" width="0.1524" layer="91"/>
<wire x1="68.58" y1="102.87" x2="68.58" y2="97.79" width="0.1524" layer="91"/>
<junction x="55.88" y="107.95"/>
<junction x="55.88" y="105.41"/>
<junction x="55.88" y="102.87"/>
<pinref part="SUPPLY25" gate="G$1" pin="VCC"/>
<wire x1="68.58" y1="105.41" x2="68.58" y2="102.87" width="0.1524" layer="91"/>
<junction x="68.58" y="102.87"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="V+"/>
<wire x1="25.4" y1="43.18" x2="7.62" y2="43.18" width="0.1524" layer="91"/>
<label x="7.62" y="43.18" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="91.44" y1="60.96" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<wire x1="91.44" y1="63.5" x2="92.71" y2="63.5" width="0.1524" layer="91"/>
<label x="92.71" y="63.5" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$21"/>
<wire x1="144.78" y1="100.33" x2="143.51" y2="100.33" width="0.1524" layer="91"/>
<label x="143.51" y="100.33" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="163.83" y1="85.09" x2="158.75" y2="85.09" width="0.1524" layer="91"/>
<label x="158.75" y="85.09" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="FSW@7"/>
<wire x1="17.78" y1="102.87" x2="13.97" y2="102.87" width="0.1524" layer="91"/>
<label x="13.97" y="102.87" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VOS@14"/>
<wire x1="53.34" y1="113.03" x2="55.88" y2="113.03" width="0.1524" layer="91"/>
<label x="55.88" y="113.03" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="COUT2" gate="G$1" pin="1"/>
<wire x1="149.86" y1="240.03" x2="152.4" y2="240.03" width="0.1524" layer="91"/>
<wire x1="152.4" y1="240.03" x2="152.4" y2="232.41" width="0.1524" layer="91"/>
<pinref part="RPG" gate="G$1" pin="2"/>
<wire x1="149.86" y1="247.65" x2="152.4" y2="247.65" width="0.1524" layer="91"/>
<wire x1="152.4" y1="247.65" x2="152.4" y2="240.03" width="0.1524" layer="91"/>
<wire x1="152.4" y1="240.03" x2="154.94" y2="240.03" width="0.1524" layer="91"/>
<label x="154.94" y="240.03" size="0.762" layer="95" xref="yes"/>
<junction x="152.4" y="240.03"/>
</segment>
</net>
<net name="B-" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="8.89" y1="35.56" x2="7.62" y2="35.56" width="0.1524" layer="91"/>
<label x="7.62" y="35.56" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="128.27" y1="59.69" x2="128.27" y2="52.07" width="0.1524" layer="91"/>
<label x="128.27" y="59.69" size="0.762" layer="95" rot="R180" xref="yes"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="128.27" y1="59.69" x2="161.29" y2="59.69" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A-" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="8.89" y1="50.8" x2="7.62" y2="50.8" width="0.1524" layer="91"/>
<label x="7.62" y="50.8" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="140.97" y1="57.15" x2="140.97" y2="52.07" width="0.1524" layer="91"/>
<label x="140.97" y="57.15" size="0.762" layer="95" rot="R180" xref="yes"/>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="140.97" y1="57.15" x2="161.29" y2="57.15" width="0.1524" layer="91"/>
</segment>
</net>
<net name="C+" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="128.27" y1="41.91" x2="128.27" y2="40.64" width="0.1524" layer="91"/>
<wire x1="128.27" y1="40.64" x2="128.27" y2="38.1" width="0.1524" layer="91"/>
<wire x1="128.27" y1="40.64" x2="125.73" y2="40.64" width="0.1524" layer="91"/>
<label x="125.73" y="40.64" size="0.762" layer="95" rot="MR0" xref="yes"/>
<junction x="128.27" y="40.64"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="C+"/>
<wire x1="63.5" y1="39.37" x2="81.28" y2="39.37" width="0.1524" layer="91"/>
<label x="81.28" y="39.37" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="B+" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="140.97" y1="41.91" x2="140.97" y2="40.64" width="0.1524" layer="91"/>
<wire x1="140.97" y1="40.64" x2="140.97" y2="39.37" width="0.1524" layer="91"/>
<wire x1="140.97" y1="40.64" x2="138.43" y2="40.64" width="0.1524" layer="91"/>
<label x="138.43" y="40.64" size="0.762" layer="95" rot="MR0" xref="yes"/>
<junction x="140.97" y="40.64"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="B+"/>
<wire x1="25.4" y1="39.37" x2="7.62" y2="39.37" width="0.1524" layer="91"/>
<label x="7.62" y="39.37" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A+" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="154.94" y1="41.91" x2="154.94" y2="40.64" width="0.1524" layer="91"/>
<wire x1="154.94" y1="40.64" x2="154.94" y2="39.37" width="0.1524" layer="91"/>
<wire x1="154.94" y1="40.64" x2="152.4" y2="40.64" width="0.1524" layer="91"/>
<label x="152.4" y="40.64" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="154.94" y="40.64"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="A+"/>
<wire x1="25.4" y1="46.99" x2="7.62" y2="46.99" width="0.1524" layer="91"/>
<label x="7.62" y="46.99" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="D-"/>
<wire x1="63.5" y1="50.8" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="50.8" x2="66.04" y2="54.61" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="0D"/>
<wire x1="66.04" y1="54.61" x2="63.5" y2="54.61" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="D+"/>
<wire x1="63.5" y1="46.99" x2="86.36" y2="46.99" width="0.1524" layer="91"/>
<wire x1="86.36" y1="46.99" x2="86.36" y2="49.53" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="91.44" y1="50.8" x2="91.44" y2="49.53" width="0.1524" layer="91"/>
<wire x1="91.44" y1="49.53" x2="91.44" y2="48.26" width="0.1524" layer="91"/>
<wire x1="86.36" y1="49.53" x2="91.44" y2="49.53" width="0.1524" layer="91"/>
<junction x="91.44" y="49.53"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="C-"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="63.5" y1="35.56" x2="68.58" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="68.58" y1="35.56" x2="69.85" y2="35.56" width="0.1524" layer="91"/>
<wire x1="68.58" y1="34.29" x2="68.58" y2="35.56" width="0.1524" layer="91"/>
<junction x="68.58" y="35.56"/>
</segment>
</net>
<net name="OC" class="0">
<segment>
<wire x1="64.77" y1="22.86" x2="64.77" y2="31.75" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="OC"/>
<wire x1="64.77" y1="31.75" x2="63.5" y2="31.75" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="68.58" y1="24.13" x2="68.58" y2="22.86" width="0.1524" layer="91"/>
<wire x1="68.58" y1="22.86" x2="64.77" y2="22.86" width="0.1524" layer="91"/>
<wire x1="68.58" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
<label x="81.28" y="22.86" size="0.762" layer="95" xref="yes"/>
<junction x="68.58" y="22.86"/>
</segment>
<segment>
<pinref part="RLP3" gate="G$1" pin="2"/>
<wire x1="86.36" y1="119.38" x2="86.36" y2="120.65" width="0.1524" layer="91"/>
<label x="86.36" y="120.65" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="B-"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="25.4" y1="35.56" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<wire x1="20.32" y1="35.56" x2="20.32" y2="34.29" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="19.05" y1="35.56" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<junction x="20.32" y="35.56"/>
</segment>
</net>
<net name="OB" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="20.32" y1="24.13" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<wire x1="20.32" y1="22.86" x2="24.13" y2="22.86" width="0.1524" layer="91"/>
<wire x1="24.13" y1="22.86" x2="24.13" y2="31.75" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="OB"/>
<wire x1="24.13" y1="31.75" x2="25.4" y2="31.75" width="0.1524" layer="91"/>
<wire x1="20.32" y1="22.86" x2="7.62" y2="22.86" width="0.1524" layer="91"/>
<label x="7.62" y="22.86" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="20.32" y="22.86"/>
</segment>
<segment>
<pinref part="RLP2" gate="G$1" pin="2"/>
<wire x1="36.83" y1="193.04" x2="35.56" y2="193.04" width="0.1524" layer="91"/>
<label x="35.56" y="193.04" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A-"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="25.4" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="20.32" y1="50.8" x2="19.05" y2="50.8" width="0.1524" layer="91"/>
<wire x1="20.32" y1="52.07" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<junction x="20.32" y="50.8"/>
</segment>
</net>
<net name="OA" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OA"/>
<wire x1="25.4" y1="54.61" x2="24.13" y2="54.61" width="0.1524" layer="91"/>
<wire x1="24.13" y1="54.61" x2="24.13" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="24.13" y1="63.5" x2="20.32" y2="63.5" width="0.1524" layer="91"/>
<wire x1="20.32" y1="63.5" x2="20.32" y2="62.23" width="0.1524" layer="91"/>
<wire x1="20.32" y1="63.5" x2="7.62" y2="63.5" width="0.1524" layer="91"/>
<label x="7.62" y="63.5" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="20.32" y="63.5"/>
</segment>
<segment>
<pinref part="RLP1" gate="G$1" pin="2"/>
<wire x1="8.89" y1="193.04" x2="7.62" y2="193.04" width="0.1524" layer="91"/>
<label x="7.62" y="193.04" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="154.94" y1="54.61" x2="154.94" y2="52.07" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="154.94" y1="54.61" x2="161.29" y2="54.61" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3V3O" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="163.83" y1="82.55" x2="158.75" y2="82.55" width="0.1524" layer="91"/>
<label x="158.75" y="82.55" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$1"/>
<wire x1="160.02" y1="100.33" x2="158.75" y2="100.33" width="0.1524" layer="91"/>
<label x="158.75" y="100.33" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SW" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SW@4"/>
<wire x1="100.33" y1="175.26" x2="97.79" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SW@5"/>
<wire x1="97.79" y1="175.26" x2="97.79" y2="172.72" width="0.1524" layer="91"/>
<wire x1="97.79" y1="172.72" x2="100.33" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SW@6"/>
<wire x1="100.33" y1="170.18" x2="97.79" y2="170.18" width="0.1524" layer="91"/>
<wire x1="97.79" y1="170.18" x2="97.79" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SW@7"/>
<wire x1="100.33" y1="167.64" x2="97.79" y2="167.64" width="0.1524" layer="91"/>
<wire x1="97.79" y1="167.64" x2="97.79" y2="170.18" width="0.1524" layer="91"/>
<pinref part="CBOOT" gate="G$1" pin="1"/>
<wire x1="97.79" y1="175.26" x2="90.17" y2="175.26" width="0.1524" layer="91"/>
<wire x1="90.17" y1="175.26" x2="90.17" y2="173.99" width="0.1524" layer="91"/>
<wire x1="90.17" y1="175.26" x2="82.55" y2="175.26" width="0.1524" layer="91"/>
<label x="82.55" y="175.26" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="90.17" y="175.26"/>
<junction x="97.79" y="175.26"/>
<junction x="97.79" y="172.72"/>
<junction x="97.79" y="170.18"/>
</segment>
<segment>
<pinref part="RT" gate="G$1" pin="1"/>
<wire x1="83.82" y1="177.8" x2="82.55" y2="177.8" width="0.1524" layer="91"/>
<label x="82.55" y="177.8" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="40.64" y1="247.65" x2="41.91" y2="247.65" width="0.1524" layer="91"/>
<label x="41.91" y="247.65" size="0.762" layer="95" xref="yes"/>
<pinref part="L1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="BOOT"/>
<wire x1="100.33" y1="165.1" x2="90.17" y2="165.1" width="0.1524" layer="91"/>
<pinref part="CBOOT" gate="G$1" pin="2"/>
<wire x1="90.17" y1="165.1" x2="90.17" y2="166.37" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="FSW"/>
<pinref part="RT" gate="G$1" pin="2"/>
<wire x1="100.33" y1="177.8" x2="93.98" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VC" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="115.57" y1="190.5" x2="115.57" y2="193.04" width="0.1524" layer="91"/>
<wire x1="115.57" y1="193.04" x2="99.06" y2="193.04" width="0.1524" layer="91"/>
<wire x1="99.06" y1="193.04" x2="99.06" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="EN"/>
<wire x1="99.06" y1="180.34" x2="100.33" y2="180.34" width="0.1524" layer="91"/>
<wire x1="99.06" y1="193.04" x2="97.79" y2="193.04" width="0.1524" layer="91"/>
<label x="97.79" y="193.04" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="99.06" y="193.04"/>
</segment>
<segment>
<pinref part="CVCC" gate="G$1" pin="1"/>
<wire x1="19.05" y1="153.67" x2="19.05" y2="162.56" width="0.1524" layer="91"/>
<wire x1="19.05" y1="162.56" x2="20.32" y2="162.56" width="0.1524" layer="91"/>
<label x="20.32" y="162.56" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="FB" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="FB"/>
<wire x1="146.05" y1="175.26" x2="148.59" y2="175.26" width="0.1524" layer="91"/>
<label x="148.59" y="175.26" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RFBB1" gate="G$1" pin="2"/>
<pinref part="RFBT" gate="G$1" pin="1"/>
<wire x1="57.15" y1="231.14" x2="57.15" y2="233.68" width="0.1524" layer="91"/>
<wire x1="57.15" y1="233.68" x2="57.15" y2="236.22" width="0.1524" layer="91"/>
<wire x1="57.15" y1="233.68" x2="54.61" y2="233.68" width="0.1524" layer="91"/>
<label x="54.61" y="233.68" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="57.15" y="233.68"/>
</segment>
</net>
<net name="COMP" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="COMP"/>
<wire x1="146.05" y1="177.8" x2="148.59" y2="177.8" width="0.1524" layer="91"/>
<label x="148.59" y="177.8" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CCOMP2" gate="G$1" pin="1"/>
<wire x1="33.02" y1="153.67" x2="33.02" y2="162.56" width="0.1524" layer="91"/>
<pinref part="RCOMP" gate="G$1" pin="2"/>
<wire x1="33.02" y1="162.56" x2="48.26" y2="162.56" width="0.1524" layer="91"/>
<wire x1="48.26" y1="162.56" x2="48.26" y2="160.02" width="0.1524" layer="91"/>
<wire x1="48.26" y1="162.56" x2="49.53" y2="162.56" width="0.1524" layer="91"/>
<label x="49.53" y="162.56" size="0.762" layer="95" xref="yes"/>
<junction x="48.26" y="162.56"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="ILIM"/>
<pinref part="RLIM" gate="G$1" pin="2"/>
<wire x1="146.05" y1="180.34" x2="158.75" y2="180.34" width="0.1524" layer="91"/>
<wire x1="158.75" y1="180.34" x2="158.75" y2="179.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SS"/>
<pinref part="CSS" gate="G$1" pin="1"/>
<wire x1="115.57" y1="149.86" x2="115.57" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="RCOMP" gate="G$1" pin="1"/>
<pinref part="CCOMP" gate="G$1" pin="1"/>
<wire x1="48.26" y1="149.86" x2="48.26" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="RFBB1" gate="G$1" pin="1"/>
<pinref part="RFBB2" gate="G$1" pin="2"/>
<wire x1="57.15" y1="220.98" x2="57.15" y2="219.71" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SW2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SW@1"/>
<wire x1="17.78" y1="118.11" x2="15.24" y2="118.11" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SW@2"/>
<wire x1="15.24" y1="118.11" x2="15.24" y2="115.57" width="0.1524" layer="91"/>
<wire x1="15.24" y1="115.57" x2="17.78" y2="115.57" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SW@3"/>
<wire x1="17.78" y1="113.03" x2="15.24" y2="113.03" width="0.1524" layer="91"/>
<wire x1="15.24" y1="113.03" x2="15.24" y2="115.57" width="0.1524" layer="91"/>
<wire x1="15.24" y1="113.03" x2="13.97" y2="113.03" width="0.1524" layer="91"/>
<label x="13.97" y="113.03" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="15.24" y="115.57"/>
<junction x="15.24" y="113.03"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="139.7" y1="240.03" x2="137.16" y2="240.03" width="0.1524" layer="91"/>
<label x="137.16" y="240.03" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SS/TR@9"/>
<pinref part="CSS2" gate="G$1" pin="1"/>
<wire x1="53.34" y1="100.33" x2="55.88" y2="100.33" width="0.1524" layer="91"/>
<wire x1="55.88" y1="100.33" x2="55.88" y2="97.79" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PG" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PG@4"/>
<wire x1="17.78" y1="110.49" x2="13.97" y2="110.49" width="0.1524" layer="91"/>
<label x="13.97" y="110.49" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RPG" gate="G$1" pin="1"/>
<wire x1="139.7" y1="247.65" x2="137.16" y2="247.65" width="0.1524" layer="91"/>
<label x="137.16" y="247.65" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="OAA" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="P$27"/>
<wire x1="144.78" y1="115.57" x2="143.51" y2="115.57" width="0.1524" layer="91"/>
<label x="143.51" y="115.57" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RLP1" gate="G$1" pin="1"/>
<pinref part="CLP1" gate="G$1" pin="1"/>
<wire x1="19.05" y1="193.04" x2="21.59" y2="193.04" width="0.1524" layer="91"/>
<wire x1="21.59" y1="193.04" x2="21.59" y2="190.5" width="0.1524" layer="91"/>
<wire x1="21.59" y1="193.04" x2="22.86" y2="193.04" width="0.1524" layer="91"/>
<label x="22.86" y="193.04" size="0.762" layer="95" xref="yes"/>
<junction x="21.59" y="193.04"/>
</segment>
</net>
<net name="OBB" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="P$28"/>
<wire x1="144.78" y1="118.11" x2="143.51" y2="118.11" width="0.1524" layer="91"/>
<label x="143.51" y="118.11" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RLP2" gate="G$1" pin="1"/>
<pinref part="CLP2" gate="G$1" pin="1"/>
<wire x1="46.99" y1="193.04" x2="49.53" y2="193.04" width="0.1524" layer="91"/>
<wire x1="49.53" y1="193.04" x2="49.53" y2="190.5" width="0.1524" layer="91"/>
<wire x1="49.53" y1="193.04" x2="50.8" y2="193.04" width="0.1524" layer="91"/>
<label x="50.8" y="193.04" size="0.762" layer="95" xref="yes"/>
<junction x="49.53" y="193.04"/>
</segment>
</net>
<net name="OCC" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="P$29"/>
<wire x1="144.78" y1="120.65" x2="143.51" y2="120.65" width="0.1524" layer="91"/>
<label x="143.51" y="120.65" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RLP3" gate="G$1" pin="1"/>
<pinref part="CLP3" gate="G$1" pin="1"/>
<wire x1="86.36" y1="109.22" x2="86.36" y2="107.95" width="0.1524" layer="91"/>
<wire x1="86.36" y1="107.95" x2="86.36" y2="106.68" width="0.1524" layer="91"/>
<wire x1="86.36" y1="107.95" x2="87.63" y2="107.95" width="0.1524" layer="91"/>
<label x="87.63" y="107.95" size="0.762" layer="95" xref="yes"/>
<junction x="86.36" y="107.95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,25.4,43.18,U4,V+,5V,,,"/>
<approved hash="104,1,63.5,43.18,U4,V-,GND,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
