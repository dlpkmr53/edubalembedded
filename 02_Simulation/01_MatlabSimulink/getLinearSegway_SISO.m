% Linearize symbolically and then insert values   

% input variables
    syms phiw_dot_ref
    
% state variables
    syms phiw phiw_dot theta theta_dot
    
% constants
    syms m R L I g Tau K m_wheel b_wheel I_wheel
    
% put model here
    [y1,y2,d_phiw,d_phiw_dot,d_theta,d_theta_dot] = nonlinearSegwayODE(...
        phiw_dot_ref,...                 % input
        phiw,phiw_dot,theta,theta_dot,... % states
        m, L, R, I, Tau, K, g, m_wheel, b_wheel, I_wheel);         % parameters
    
% symbolic state space matrices
    symbROM.A = jacobian([d_phiw_dot,d_theta,d_theta_dot],[phiw_dot,theta,theta_dot]);
    symbROM.B = jacobian([d_phiw_dot,d_theta,d_theta_dot],[phiw_dot_ref]);
    symbROM.C = jacobian([y2],[phiw_dot,theta,theta_dot]);
    symbROM.D = jacobian([y2],[phiw_dot_ref]);
    
% Set wheel parameters (weight, intertia, wheel damping) to zero
    b_wheel = 0;
    I_wheel = 0;
    m_wheel = 0;
    
% Define linearization point
        phiw        = 0;
        phiw_dot    = 0;
        theta       = 0;
        theta_dot   = 0;

% Evaluate as a function of still undefined parameters 
	As = eval(subs(symbROM.A)); 
    Bs = eval(subs(symbROM.B)); 
    Cs = eval(subs(symbROM.C)); 
    Ds = eval(subs(symbROM.D)); 

% Parameters from Aachen
    m       = par.m;
    R       = par.R;
    L       = par.L;
    I       = par.I;
    Bm      = par.Bm;
    g       = par.g;
    Tau     = par.Tau;
    K       = par.K;
    Ts      = par.Ts;
    K_lqr   = par.K_lqr;

% Evaluate numerically    
    A = eval(subs(symbROM.A)); 
    B = eval(subs(symbROM.B)); 
    C = eval(subs(symbROM.C)); 
    D = eval(subs(symbROM.D)); 

    sys = tf(ss(A,B,C,D));






