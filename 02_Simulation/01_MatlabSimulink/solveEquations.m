%% Solve system of ODE for theta_dd
% Modeling follows "Modeling of Two-Wheeled Self-Balancing Robot Driven by
% DC Gearmotors" by Frankovsky et al.
%
% --> look into function nonlinearSegwayODE
% --> the dynamics for phi_wheel are given, only an equation for the second
% derivative of theta is required
% 
% How-to get equations
% Use equations (17) and (18) 
% eqn 18: E18 = T_tot/r
% eqn 19: E19 = -T_tot
% 
% --> set E19 = -1/r * E18
% 
% and solve for dd_theta

syms c1 c2 c3 c4 c6 c7 c8 
syms x_dd x_d theta_dd theta_d theta
    
syms m m_wheel I_wheel R b_wheel L T I g 
eqn1 = c1*x_dd        + c2*x_d    + c3 * cos(theta)*theta_dd    + c4*sin(theta)*theta_d^2 == ...
    -1/R*(c6*cos(theta)*x_dd            + c7*theta_dd             + c8*sin(theta));


eqn2 = subs(eqn1,{c1,c2,c3,c4,c6,c7,c8},...
    [m+2*m_wheel+2*I_wheel/R^2,...
    2*b_wheel/R^2,...
    m*L,...
    -m*L,...
    m*L,...
    m*L^2+I,...
    -m*g*L]);

% Replace x with phi_w state
syms phi phi_d phi_dd phiw phiw_d phiw_dd
% phi is the wheel angle (relative to the up-pointing z-axis)
% phiw is the motor shaft angle (relative to chassis)
lhs_phi         = phiw + theta;
lhs_phi_d       = phiw_d + theta_d;
lhs_phi_dd      = phiw_dd + theta_dd;
lhs_x           = phi * R;
lhs_x_d         = phi_d * R;
lhs_x_dd        = phi_dd * R;

lhs_x           = subs(lhs_x, {phi}, [lhs_phi]);
lhs_x_d         = subs(lhs_x_d, {phi_d}, [lhs_phi_d]);
lhs_x_dd        = subs(lhs_x_dd, {phi_dd}, [lhs_phi_dd]);

eqn3 = eqn2;
% eqn3 = subs(eqn2, {x_d, x_dd}, [lhs_x_d, lhs_x_dd]);
% Eliminate body inertia with steiner term
eqn3 = subs(eqn3, {I}, {m*L^2});
eqn3 = subs(eqn3, {m_wheel, I_wheel, b_wheel}, {0,0,0})
lhs_theta_dd = solve([eqn3],[theta_dd])
lin_eqn3 = subs(eqn3, {cos(theta), sin(theta), theta_d^2}, {1, theta, 0})
% copy the symbolic solution of theta_dd into the file "nonlinearSegwayODE"