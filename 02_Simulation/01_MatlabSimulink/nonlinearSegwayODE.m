function [y1,y2,lhs_phiw_d,lhs_phiw_dd,lhs_theta_d,lhs_theta_dd,...
    x_d,phi,phi_d] = ...
    nonlinearSegwayODE(phiw_d_ref,...                 % input
    phiw,phiw_d,theta,theta_d,...                   % states
    m, L, R, I, Tau, K, g, m_wheel, b_wheel, I_wheel)   % Parameters

% State space
% State update
% Wheel angle phi_wheel
% From speed controller, we know (approximate)
lhs_phiw_d      = phiw_d;
lhs_phiw_dd     = -1/Tau * phiw_d + K/Tau * phiw_d_ref;
phiw_dd = lhs_phiw_dd;

% Body angle theta
lhs_theta_d     = theta_d;
lhs_theta_dd    = ...
            ((L*g*m*sin(theta) - L*R*m*phiw_dd*cos(theta))/R - R*phiw_dd*(m + 2*m_wheel + (2*I_wheel)/R^2) - (2*b_wheel*(phiw_d + theta_d))/R + L*m*theta_d^2*sin(theta))/((2*m*L^2 + R*m*cos(theta)*L)/R + R*(m + 2*m_wheel + (2*I_wheel)/R^2) + L*m*cos(theta));
% theta_dd is derived in the file solveEquations.m

% Other quantities (for plotting)
phi         = phiw + theta;
phi_d       = phiw_d + theta_d;
% phi_dd      = lhs_phiw_dd + lhs_theta_dd;
x           = phi * R;
x_d         = phi_d * R;
% x_dd        = phi_dd * R;

% Outputs
% y1 = x;
% y2 = theta;
y1 = phiw;
y2 = theta;