% load minisegway_versuch1
% load minisegway_versuch2
% load minisegway_versuch3
% load minisegway_versuch5
% figure;
% ax1=subplot(211);
% plot((1:size(Ident_Data,1))*0.01,Ident_Data(:,1));
% ax2=subplot(212);
% plot((1:size(Ident_Data,1))*0.01,rad2deg(Ident_Data(:,2))-10.5);
% linkaxes([ax1,ax2],'x')


load minisegway_versuch3
load minisegway_simulation3
start_ind = 3374;
end_ind = 3674;
Ident_Data = Ident_Data(start_ind:end_ind,:);
fig=figure;
ax1=subplot(211);
hold on
grid on
h3=plot([0,3],[0,0],'k--');
h1=plot(data.t, rad2deg(data.phiw));
h2=plot((1:size(Ident_Data,1))*0.01,rad2deg(Ident_Data(:,1)));
legend([h1,h2],'\textrm{Simulation}', '\textrm{Experiment}', 'Location', 'southeast');
ylabel('Wheel angle $\varphi$ [deg]');
ax2=subplot(212);
hold on
grid on
plot([0,3],[0,0],'k--');
plot(data.t, rad2deg(data.theta));
plot((1:size(Ident_Data,1))*0.01,rad2deg(Ident_Data(:,2))-10.5);
ylabel('Body angle $\theta$ [deg]');
xlabel('\textrm{Time }[s]');
linkaxes([ax1,ax2],'x')
xlim([0,3]);

plotform(fig, 'plotsize', [8.853,8]);