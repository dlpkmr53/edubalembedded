t = (1:size(Ident_Data,1))*0.01;
ysim = lsim(P1, Ident_Data(:,1), t);
figure
ax(1) = subplot(211);
hold on
xlabel('\textrm{Time} [s]');
ylabel('\textrm{Voltage} [V]');
plot(t, Ident_Data(:,1))
legend('\textrm{Input voltage}');
ax(2) = subplot(212);
hold on
xlabel('\textrm{Time} [s]');
ylabel('\textrm{Shaft speed} [rad/s]');
plot(t, Ident_Data(:,2));
plot(t, ysim);
legend('\textrm{Measurement}', 'PT_1\textrm{ Model}');
linkaxes(ax, 'x');