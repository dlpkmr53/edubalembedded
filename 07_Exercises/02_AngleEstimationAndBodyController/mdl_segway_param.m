%% Initialize parameters
par.m   = 0.933;        % body part mass [kg] 
par.R   = 0.04;         % radius of wheel [m]
par.L   = 0.085749;     % position of COM [m]
par.I   = 0.00686014;   % inertia of body part [kg*m^2]       
par.Bm  = 0;            % bearing damping ratio [N*m/(rad/s)]
par.g   = 9.81;         % gravity constant [m/s^2]
par.Tau = 0.0994;       % speed loop time constant (P=0.194775 I=3.387779)
par.K   = 1;            % speed loop gain
par.Ts  = 0.01;         % controller sampling time
