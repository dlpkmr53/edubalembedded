%% Initialize parameters
mdl_segway_param;
par.t_sim = 3;
par.simopt.Solver = 'ode4';
par.simopt.SrcWorkspace = 'current';
par.modelname   = 'mdl_segway_simulation';

% Initial conditions
phi0            = 0;            % Wheel angle [rad]
phid0           = 0;            % Wheel angular velocity [rad/s]
theta0          = 0;            % Body angle [rad]
thetad0         = 0;            % Body angular velocity [rad/s]

%% Open-loop system
K_lqr           = [0 0 0 0];

% Run simulation
simout          = sim(par.modelname,par.simopt);
data            = aux_segway_plot(simout,par);

%% Closed-loop system
% Design LQR controller
% Q               = ?           % weighting matrix Q - penalize states
% R               = ?           % weighting matrix R - penalize actuation
% K_lqr           = ?           % LQR gain matrix
% Kd_lqr          = ?           % time-discrete LQR gain matrix

% Run simulation
% simout          = sim(par.modelname,par.simopt);
% data            = aux_segway_plot(simout,par);